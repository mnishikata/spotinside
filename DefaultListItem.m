//
//  ListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "DefaultListItem.h"

@implementation DefaultListItem



- (id) init {
	self = [super init];
	if (self != nil) {
		
		
		name = [@"Computer" retain];
		canDelete = NO;
		canMove = NO;
		canDrop = NO;
		canSelect = YES;
		
		icon = nil;
	}
	return self;
}


- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		name = [@"Computer" retain];

		canDelete = NO;
		canMove = NO;
		canDrop = NO;
		canSelect = YES;
		
		
	}
	return self;
	
	
}




-(NSString*)kind {return @"default";}


-(NSString* )setQuery:(id)value
{
}

-(NSString*)query {
	
	
	return @"";
}



-(void)setName:(NSString*)aName
{

}

-(NSImage*)icon
{
	if( icon == nil )
		icon = [[NSImage imageNamed:@"computer.tiff"] retain];
	
	return [super icon];
	
}
/*
-(void)search
{
//do nothing
}
*/
-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{
	
	[aTarget performSelector:aSelector];
	//do nothing	
}

-(NSArray*)scope
{
	
	return [NSArray arrayWithObject: kMDQueryScopeComputer];
}
/*
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{
	
	requestedArrayController = anArrayController;
	endTarget = [aTarget retain];
	endSelectr = aSelector;

	
	isSearching = YES;
	[searchAgent search:@"" 
				 format:aQuery
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishForAdditionalQuery:)];
}*/

-(void)finishForAdditionalQuery:(id)result
{
	isSearching = NO;
	
	if( result == nil ) result = [NSArray array];
	
	[requestedArrayController addObjects: result];
	
	[endTarget performSelector:endSelector withObject:self];
	[endTarget release];
	endTarget = nil;
	endSelector = nil;
	
	
	
	//[[NSNotificationCenter defaultCenter] postNotificationName:@"ListItemDidFinishSearchWithAdditionalQuery" object:self];
	
	[self postNotification];
	
}

-(NSString*)queryTemplate {
	
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	NSString* _format = [ud objectForKey:@"defaultQueryTemplate"];
	
	////NSLog(@"_format%@",_format);
	if( _format == nil || [_format isEqualToString:@""] )
	{
		_format = @"(kMDItemTextContent == \"%@*\"cd) && (kMDItemContentType != com.apple.mail.emlx) && (kMDItemContentType != public.vcard)";
		
		[ud setObject:_format forKey:@"defaultQueryTemplate"];
	}

	

	
	
	return _format;
}





@end
