//
//  MNIconizedCell.m
//  WordProcessor
//
//  Created by Masatoshi Nishikata on 16/12/05.
//  Copyright 2005 __MyCompanyName__. All rights reserved.
//


#import "TagAttachmentCell.h"
#import "DrawLibrary.h"

#define DEFAULT_ATTRIBUTES  [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:12.0], NSFontAttributeName, [NSColor colorWithCalibratedRed:0.6 green:0.6 blue:0.6 alpha:1.0], NSForegroundColorAttributeName, NULL]

#define DEFAULT_ATTRIBUTES_HIGHLIGHTED  [NSDictionary dictionaryWithObjectsAndKeys:[NSFont systemFontOfSize:12.0], NSFontAttributeName, [NSColor whiteColor], NSForegroundColorAttributeName, NULL]

#define DEFAULT_FRAME_COLOR  [NSColor colorWithCalibratedRed:0.9 green:0.9 blue:0.9 alpha:1.0]
#define DEFAULT_FRAME_COLOR_FRAME  [NSColor colorWithCalibratedRed:0.6 green:0.6 blue:0.6 alpha:1.0]

#define DEFAULT_FRAME_COLOR_HIGHLIGHTED  [NSColor colorWithCalibratedRed:0.35 green:0.55 blue:0.92 alpha:1.0]



@implementation TagAttachmentCell

+(NSString*)escape:(NSString*)str
{
	if( str == nil ) str = @"";
	
	NSMutableString* mstr = [[[NSMutableString alloc] initWithString:str] autorelease];
	
	[mstr replaceOccurrencesOfString:@"\"" withString:@"\\\""
		   options:1
			 range:NSMakeRange(0,[mstr length])];

	[mstr replaceOccurrencesOfString:@"*" withString:@"\\*"
							 options:1
							   range:NSMakeRange(0,[mstr length])];

	return (NSString*)mstr;
}

+(NSAttributedString*)newTagWithString:(NSString*)string selected:(BOOL) selectNewTag
{
	NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	
	NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:wrapper] autorelease];
	TagAttachmentCell* aCell = [[[TagAttachmentCell alloc] initWithAttachment:anAttachment] autorelease];
	[aCell setAttachment: anAttachment];

	[aCell loadDefaults];
	

	[aCell setTitle:[TagAttachmentCell escape:string]];
	[aCell setState: (selectNewTag? NSOnState: NSOffState) ];

	[aCell setImage:[aCell __image]];
	
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	//[wrapper setIcon:[aCell image  ]];
	
	return [NSAttributedString attributedStringWithAttachment: anAttachment];
}	



 - (void)__drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
 
{
	int state = [self state];
	
	NSRect frameRect = cellFrame;
	frameRect.size.width -= 4;
	frameRect.size.height -= 4;
frameRect.origin.x +=2;
frameRect.origin.y +=2;

	if( state == NSOnState ){
		drawRoundedBox( DEFAULT_FRAME_COLOR_HIGHLIGHTED ,
						frameRect , 8, DEFAULT_FRAME_COLOR_HIGHLIGHTED , 0, 1);
	}
	else{
		drawRoundedBox( DEFAULT_FRAME_COLOR_FRAME ,
						frameRect , 8, DEFAULT_FRAME_COLOR , 0, 1);
		
	}
	
	///
	
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: 0
					 yBy: cellFrame.origin.y + cellFrame.size.height ]; 
	
	// rotate axis
	//[affine rotateByDegrees:180];
	[affine scaleXBy:1.0 yBy:-1.0];
	[affine concat];
	
	
	
	
	cellFrame.origin.y = 0;

	
	

	
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:[self description] attributes: ([self state] == NSOnState? DEFAULT_ATTRIBUTES_HIGHLIGHTED : DEFAULT_ATTRIBUTES)] autorelease];


	[attr drawAtPoint: NSMakePoint(cellFrame.origin.x +10, cellFrame.origin.y+1)];

	[context restoreGraphicsState];

}
-(NSSize)cellSize
{
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:[self description] attributes:DEFAULT_ATTRIBUTES] autorelease];

	NSSize size = NSZeroSize;
	
	size.width = (int)[attr size].width + 20 ;
	size.height = [attr size].height ;

	if( size.height < 20 ) size.height = 20;
	
	return size;	
}


-(NSImage*)__image
{
	
	
	NSRect frame = NSZeroRect;
	frame.size = [self cellSize];
	//NSRect imageRect = [self imageRectForBounds:frame ];
	


	NSImage* img = [[[NSImage alloc] initWithSize:frame.size] autorelease];
	
	[img setFlipped:YES];
	[img lockFocus];
	[self __drawWithFrame:frame inView:nil];
	[img unlockFocus];
	
	return img;
	
}



- (id)initWithAttachment:(NSTextAttachment*)anAttachment
{

	
    self = [super initWithAttachment:anAttachment];
    if (self) {



		
		buttonSize = 16;
		buttonPosition = 0;
		showButtonTitle = NO;
		showBezel = NO;
		
		[self loadDefaults];

		

    }
    return self;
}




- (id)copyWithZone:(NSZone *)zone
{
	//AlarmAttachmentCell* cell = [[AlarmAttachmentCell alloc] initWithAttachment:[self attachment]]
	
	TagAttachmentCell* cell = [super copyWithZone:(NSZone *)zone];
	
	[cell setTitle:[self tite]];
	[cell setImage:[cell __image]];

		
	
	return cell;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	[super encodeWithCoder: encoder];
	
	[encoder encodeObject: [self title] forKey: @"TagTitle"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		if( [coder containsValueForKey:@"TagTitle"] )
			[self setTitle:   [coder decodeObjectForKey:@"TagTitle"] ];


		[self setBezeled: NO]; 
		[self setImage:[self __image]];

	}
	return self;
	
	
}


- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)untilMouseUp

{
	if( [aTextView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
		textView =  aTextView ;
	
	
	
	
	
	
	
	BOOL result = NO;
	//NSLog(@"trackMouse:inRect:ofView:untilMouseUp:");
	NSDate *endDate;
	NSPoint currentPoint = [theEvent locationInWindow];
	BOOL done = NO;
	BOOL clickOnTitle = NO;
	
	
	
	
	if( !clickOnTitle )
	{
		
		[self setHighlighted: YES];
		[aTextView display];
		
	}
				
	
	
	
	BOOL trackContinously = [self startTrackingAt:currentPoint inView:aTextView];
	// catch next mouse-dragged or mouse-up event until timeout
	BOOL mouseIsUp = NO;
	BOOL mouseIsDragged = NO;
	
	NSEvent *event;
	while (!done) { // loop ...
		
		
		
		
		NSPoint lastPoint = currentPoint;
		endDate = [NSDate distantFuture];
		
		event = [NSApp nextEventMatchingMask:(NSLeftMouseUpMask|NSLeftMouseDraggedMask) untilDate:endDate inMode:NSEventTrackingRunLoopMode dequeue:YES];
		
		if (event) { // mouse event
			
			currentPoint = [event locationInWindow];
			if (trackContinously) { // send continueTracking.../stopTracking...
				if (![self continueTracking:lastPoint at:currentPoint inView:aTextView]) {
					done = YES;
					[self stopTracking:lastPoint at:currentPoint inView:aTextView mouseIsUp:mouseIsUp];
				}
				
			}
			
			
			mouseIsUp = ([event type] == NSLeftMouseUp);
			mouseIsDragged = ([event type] == NSLeftMouseDragged );
			
			done = done || mouseIsUp || mouseIsDragged;
			if (untilMouseUp) {
				result = mouseIsUp;
			} else {
				
			}
			
			// check, if the mouse left our cell rect
			result = NSPointInRect([aTextView convertPoint:currentPoint fromView:nil], cellFrame);
			if (!result) {
				done = YES;
				//NSLog(@"mouse is up outside rect");
			}
			
			
			if (done && result  && !clickOnTitle ) {
				[NSApp sendAction:[self action] to:[self target] from:self];
			}
		} 
		
	} // ... while (!done)
	
	if( mouseIsDragged )
	{
		
		[self setHighlighted: NO];
		
		if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
		{
			[textView buttonAttachmentCellDragged:self atCharacterIndex:charIndex];
		}
		
		
		
		[aTextView display];
		return result;
	}
	
	[self setHighlighted: NO];
	
	[aTextView display];
	
	return result;
}


-(void)action:(id)sender
{
	//NSLog(@"action"); 
	/*
	if( [textView conformsToProtocol:@protocol(AlarmAttachmentCellOwnerTextView)] )
	{
		
		[[AlarmInspector sharedAlarmInspector]  modifyAlarm:self];


	}*/
	
	[super action:sender];
	
}



-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	//[[AlarmInspector sharedAlarmInspector]  modifyAlarm:self];
}


- (void) dealloc {
	

	[super dealloc];
}

-(BOOL)textView:(NSTextView* )aTextView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString
{

	
	return [super textView:aTextView shouldChangeTextInRange:affectedCharRange  replacementString:replacementString];
}



-(void)setState:(int)state
{
	[super setState:(int)state];
	[self setImage:[self __image]];
}

-(void)setButtonSize:(float)size
{
	[self willChangeValueForKey:@"buttonSize"];
	buttonSize = size;
	[self didChangeValueForKey:@"buttonSize"];

	//[[AlarmInspector sharedAlarmInspector] updateInspector];
	
}

-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	/*
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"AlarmAttachmentCell_buttonSize"];
	[ud setInteger:[self state] forKey:@"AlarmAttachmentCell_state"];

	[ud setObject:[NSArchiver archivedDataWithRootObject: color] forKey:@"AlarmAttachmentCell_color"];

	
	[ud synchronize];
	 */
}


-(void)loadDefaults
{
	
	
	showBezel = NO;
	showButtonTitle = NO;
	
	
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	
	[self setTitle: @"Untitled"];

	[self setBezeled: NO]; 
	lock = NO;
	
}

-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TagAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
	
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"TagAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		/*
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"TagAttachmentCellItem"];
		[aContextMenu addItem:customMenuItem ];
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Edit...",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"TagAttachmentCellItem"];
		[customMenuItem setTarget: self];//[ButtonInspector sharedButtonInspector]];
			[customMenuItem setAction: @selector(editTag:)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem autorelease];	
			*/			
}

-(void)editTag:(id)sender
{
	
}
+(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView <TagAttachmentCellOwnerTextView> *)aTextView
{
	//NSLog(@"customizeContextualMenu");
	int piyo;
	NSMenuItem* customMenuItem;
	
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"TagAttachmentCellItemClassMethodMenu"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		/*
		
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Insert Alarm...",@"")
												action:nil keyEquivalent:@"A"];
	[customMenuItem setKeyEquivalentModifierMask:1048576];
	[customMenuItem setRepresentedObject:@"AlarmAttachmentCellItemClassMethodMenu"];

	[customMenuItem setTarget: nil];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(insertAlarm:)];
	
	[aContextMenu addItem:customMenuItem ];
	[customMenuItem autorelease];	
		 */
			
}

-(NSString*)description
{
	return [self title];
}


#pragma mark -
#pragma mark @protocol AttachmentCellConverting 
-(int)stringRepresentation:(NSDictionary*)context
{
	return [self attributedStringWithoutImageRepresentation:context];
}
-(int)attributedStringWithoutImageRepresentation:(NSDictionary*)context //without image 
{
	NSMutableAttributedString* mattr = [context objectForKey:@"MutableAttributedString"];
	unsigned index = [[context objectForKey:@"CellIndex"] intValue];

	
	
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:[self description] attributes:DEFAULT_ATTRIBUTES] autorelease];
	
	[mattr replaceCharactersInRange:NSMakeRange(index,1) withAttributedString:attr];
	
	
	
	
	
	return 0;
	
}
-(int)attributedStringRepresentation:(NSDictionary*)context
{
	return [self attributedStringWithoutImageRepresentation:context];

	/*
	if( UD_CHECKBOX_CONVERT_TO_TEXT ) 
		return [self attributedStringWithoutImageRepresentation:context];
	
	
	NSFileWrapper* wrapper = [[self attachment] fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
	[wrapper setIcon:[self image  ]];
		
	
	return 0;//
	 */
	
}

-(NSDictionary*)pasteboardRepresentation:(id)context
{
	return nil;
}


@end
