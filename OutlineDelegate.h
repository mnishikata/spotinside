/* OutlineDelegate */

#import <Cocoa/Cocoa.h>
@class TooltipWindow;

@interface OutlineDelegate : NSObject
{
	IBOutlet id outlineView;
	IBOutlet id myDocument;
	IBOutlet id findResult;
	IBOutlet id table;
	IBOutlet id flowView;

	IBOutlet id windowUIObjectController;

	NSMutableArray* dataArray;
	
	
	TooltipWindow* tooltip;
	
}
- (IBAction)addFolder:(id)sender;
@end
