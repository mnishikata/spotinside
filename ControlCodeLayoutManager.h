// Smultron version 1.2.5, 2005-05-08
// Written by Peter Borg, pgw3@mac.com
// Copyright (C) 2004-2005 Peter Borg
// For the latest version of the code go to http://smultron.sourceforge.net
// Released under GNU General Public License, http://www.gnu.org/copyleft/gpl.html

// draw invisible characters like return , tab , space

#import <Cocoa/Cocoa.h>

@interface ControlCodeLayoutManager : NSLayoutManager {
	

	NSColor* highlightColor;
}

-(void)setShowsInvisibleCharacters:(BOOL)flag;
-(BOOL)showsInvisibleCharacters;
-(void)drawNumberAt:(NSPoint)point charIndex:(unsigned)charIndex;

- (id) temporaryAttributeForKey:(NSString*)key 
			   atCharacterIndex:(unsigned)charIndex longestEffectiveRange:(NSRangePointer)effectiveCharRange expandTo:(int)expandMode;
@end
