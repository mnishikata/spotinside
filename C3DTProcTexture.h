//
//  C3DTProcTexture.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Mon Jun 16 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTProcTexture.h,v 1.3 2003/07/08 11:21:11 pmanna Exp $
//
// $Log: C3DTProcTexture.h,v $
// Revision 1.3  2003/07/08 11:21:11  pmanna
// Modified to use a more generic generateTexture
// Moved texture generation to public
//
// Revision 1.2  2003/06/22 09:39:36  pmanna
// Refactoring of texture objects to consolidate common tasks
//
// Revision 1.1  2003/06/17 07:06:48  pmanna
// Added code for Perlin noise & modiified Tutorial 2
//
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/glu.h>
#import "C3DTTexture.h"
#import	"C3DTMath.h"

typedef enum {
    PERLIN_TYPE = 0,
    WAVE_TYPE,
    GRAIN_TYPE,
    CHECKER_TYPE
} procTextureType;

@interface C3DTProcTexture : C3DTTexture {
    procTextureType	_textureType;
    _C3DTVector		_startColor;		// The start color to interpolate from
    _C3DTVector		_endColor;			// The end color
    float			_density;
    float			_persistence;
    int				_octaves;
}

+ (C3DTProcTexture * )perlinTextureWithSize: (NSSize)aSize
                                  fromColor: (_C3DTVector)startColor
                                    toColor: (_C3DTVector)endColor
                                    density: (float)aDensity
                                persistence: (float)aPersistence
                                    octaves: (int)octaves;

+ (C3DTProcTexture * )waveTextureWithSize: (NSSize)aSize
                                fromColor: (_C3DTVector)startColor
                                  toColor: (_C3DTVector)endColor
                                  density: (float)aDensity
                              persistence: (float)aPersistence
                                  octaves: (int)octaves;

+ (C3DTProcTexture * )grainTextureWithSize: (NSSize)aSize
                                 fromColor: (_C3DTVector)startColor
                                   toColor: (_C3DTVector)endColor
                                   density: (float)aDensity
                               persistence: (float)aPersistence
                                   octaves: (int)octaves;

+ (C3DTProcTexture * )checkerTextureWithSize: (NSSize)aSize
                                   fromColor: (_C3DTVector)startColor
                                     toColor: (_C3DTVector)endColor
                                    cellSize: (int)octaves;

- (id)initTextureWithType: (procTextureType)aType size: (NSSize)aSize
                fromColor: (_C3DTVector)startColor toColor: (_C3DTVector)endColor
                  density: (float)aDensity persistence: (float)aPersistence octaves: (int)octaves;

- (BOOL)allocateTextureBytesForSize: (_C3DTVector)aSize;

- (BOOL)createPerlinTextureWithSize: (_C3DTVector)aSize
                          fromColor: (_C3DTVector)startColor
                            toColor: (_C3DTVector)endColor
                             density: (float)aDensity
                        persistence: (float)aPersistence
                            octaves: (int)octaves;

- (BOOL)createWaveTextureWithSize: (_C3DTVector)aSize
                        fromColor: (_C3DTVector)startColor
                          toColor: (_C3DTVector)endColor
                           density: (float)aDensity
                      persistence: (float)aPersistence
                          octaves: (int)octaves;

- (BOOL)createGrainTextureWithSize: (_C3DTVector)aSize
                         fromColor: (_C3DTVector)startColor
                           toColor: (_C3DTVector)endColor
                            density: (float)aDensity
                       persistence: (float)aPersistence
                           octaves: (int)octaves;

@end
