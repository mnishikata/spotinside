#import "AppController.h"
#import "DefaultListItem.h"
#import "HistoryListItem.h"

#import "SpotlightTextContentRetriever.h"
#import "AvoidNullStringValueTransformer.h"
#import "KeyedUnarchiveFromDataTransformer.h"
@implementation AppController

+ (void)initialize {
    if (self == [AppController class]) {   
		
		
		NSValueTransformer *mbTrans = [[AvoidNullStringValueTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"AvoidNullStringValueTransformer"];
		[mbTrans release];
		
		mbTrans = [[KeyedUnarchiveFromDataTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"KeyedUnarchiveFromDataTransformer"];
		[mbTrans release];
		
		
    }
}


-(void)awakeFromNib
{
	metaListArray = nil;
	allKeys = nil;
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	if( [ud objectForKey:@"SUCheckAtStartup"] == nil )
	{
		[ud setBool:YES forKey:@"SUCheckAtStartup"];
	}
	
	
	if( [ud objectForKey:@"googleFormat"] == nil )
	{
		[ud setObject:@"http://www.google.com/search?q=%@" forKey:@"googleFormat"];
	}
	
	
	
	if( [ud objectForKey:@"useCache"] == nil )
	{
		[ud setBool:YES forKey:@"useCache"];
	}
	//counterMode
	
	if( [ud objectForKey:@"counterMode"] == nil )
	{
		[ud setInteger:1 forKey:@"counterMode"];
	}
	
	//findOption
	
	if( [ud objectForKey:@"findOption"] == nil )
	{
		[ud setBool:YES forKey:@"findOption"];
	}
	
	if( [ud objectForKey:@"commandG"] == nil )
	{
		[ud setBool:YES forKey:@"commandG"];
	}
	
	
	if( [ud objectForKey:@"tagPrefix"] == nil )
	{
		[ud setValue:@"&" forKey:@"tagPrefix" ];
		[ud setValue:@"" forKey:@"tagSuffix" ];
		[ud setValue:@" " forKey:@"tagSeparator" ];
		[ud setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[ud setValue:@"" forKey:@"tagStartCode" ];
		[ud setValue:@"" forKey:@"tagEndCode" ];
		
		[ud setValue:@"&.*" forKey:@"tagGroupExtract" ];
		[ud setValue:@"(?<=&).[^ \\n]*" forKey:@"tagExtract" ];	
	}
	
	
	[ud addObserver:self forKeyPath:@"tagPrefix" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagSuffix" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagSeparator" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagCommentPrefix" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagStartCode" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagEndCode" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagGroupExtract" options:NSKeyValueObservingOptionNew context:nil];
	[ud addObserver:self forKeyPath:@"tagExtract" options:NSKeyValueObservingOptionNew context:nil];


	[self observeValueForKeyPath:nil ofObject:nil change:nil context:nil];
	
	
	if( [ud objectForKey:@"tagPaneVisible"] == nil )
	{
		[ud setBool:NO forKey:@"tagPaneVisible"];
		
	}
	///
	
	tagManager = [[TagManager alloc] init];
	
	///
	organiserArray = [[NSMutableArray array] retain];	
	[self loadOrganiser];	
	
}
-(TagManager*)tagManager
{
	return tagManager;
}


-(NSArray*)metadataList
{
	if( metaListArray == nil )
	{
		metaListArray = [[NSMutableArray alloc] init]; 
		//[metaList setContent:metaListArray];
		
		NSArray* allAttr = MDSchemaCopyAllAttributes();
		NSArray* currentMetaList = [NSArray arrayWithArray:allAttr];
		
		int hoge;
		for( hoge = 0; hoge < [currentMetaList count]; hoge++ )
		{
			NSMutableDictionary* dictionary = [NSMutableDictionary dictionary];
			
			
			NSString* str = [currentMetaList objectAtIndex:hoge];
			
			NSString* mdtitle = MDSchemaCopyDisplayNameForAttribute(str);
			
			NSString* title;
			if( mdtitle == nil ) title = str;
			else{
				title = [NSString stringWithString:mdtitle];
				CFRelease(mdtitle);
			}
			
			NSString* desc;
			
			CFStringRef mddesc = MDSchemaCopyDisplayDescriptionForAttribute (str);
			
			if( mddesc == nil ) desc = @"";
			else{
				desc = [NSString stringWithString:mddesc];
				CFRelease(mddesc);
			}
			[dictionary setObject:title forKey:@"attributeDisplayName"];
			[dictionary setObject:str forKey:@"attribute"];
			[dictionary setObject:[NSNumber numberWithBool:NO ] forKey:@"enable"];
			[dictionary setObject:desc forKey:@"attributeDescription"];
			
			[metaListArray  addObject:dictionary];
			
		}
		
		
		// add optional
		
		NSMutableDictionary* dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:kMDQueryResultContentRelevance, @"attribute",NSLocalizedString(@"Score",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
		
		[metaListArray  addObject:dic];
		
		dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"IconColumn.SpotInsideInternalAttribute", @"attribute",NSLocalizedString(@"Icon",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable",nil];
		
		[metaListArray  addObject:dic];
		
		
		dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:@"TagBotTagColumn.SpotInsideInternalAttribute", @"attribute",NSLocalizedString(@"TagBot Tag",@""),@"attributeDisplayName",[NSNumber numberWithBool:NO ], @"enable", @"Tags added by TagBot", @"attributeDescription", nil];
		
		[metaListArray  addObject:dic];
		
		
		
		
		if( allAttr != nil ) CFRelease(allAttr);
		
		
	}
	
	return metaListArray;
	
}


-(NSMutableArray*)allKeys
{
	if( allKeys == nil )
	{
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		NSArray* tcArray = [ud objectForKey:@"defaultTableColumns"];
		if( tcArray == nil )
		{
			tcArray =  [NSMutableArray arrayWithObjects: @"IconColumn.SpotInsideInternalAttribute", kMDItemDisplayName, kMDQueryResultContentRelevance, kMDItemPath,kMDItemFSCreationDate,kMDItemFSContentChangeDate, kMDItemFSSize, @"TagBotTagColumn.SpotInsideInternalAttribute",nil];
			
		}
		
		allKeys =  [NSMutableArray arrayWithArray: tcArray];
		[allKeys retain];
	}
	
	return allKeys;
	
}

-(NSMutableArray*)organiserArray
{
	return organiserArray;
}

-(void)saveOrganiser
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSData* data = [NSKeyedArchiver archivedDataWithRootObject:organiserArray];
	[ud setObject:data  forKey:@"Organiser_2"];
	[ud synchronize];
}



-(void)loadOrganiser
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSData* data = [ud objectForKey: @"Organiser_2"];
	
	if( data != nil )
	{
		[organiserArray removeAllObjects];

		id array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
		if( array != nil )
			[organiserArray addObjectsFromArray: array];

	
		
	//	//NSLog(@"org %@",[array description]);
	
	}else
	{
	
	
	/*
	NSString* organiserPath = [[NSBundle bundleForClass:[self class]] pathForResource:@"memolist" ofType:@"plist"];
	NSString* organiserStr = [[NSString alloc] initWithContentsOfFile:organiserPath];
	
	NSArray* array = [organiserStr propertyList];
	
	[organiserArray removeAllObjects];
	[organiserArray addObjectsFromArray: array];
	*/
	



	
	}
	
	BOOL hasComputer = NO;
	BOOL hasHistory = NO;
	
	
	int hoge;
	for(hoge = 0; hoge < [organiserArray count] ; hoge++ )
	{

		id item = [organiserArray objectAtIndex:hoge];
		if( [item isKindOfClass:[DefaultListItem class]] ) hasComputer = YES;
		if( [item isKindOfClass:[HistoryListItem class]] ) hasHistory = YES;

	}
	
	
	if( !hasComputer )
	{
	DefaultListItem  *computer = [[[DefaultListItem alloc] init] autorelease];
	[organiserArray addObject:computer];
	}
	if( !hasHistory )
	{
	HistoryListItem  *history = [HistoryListItem sharedHistoryListItem];
	[organiserArray addObject:history];
	}
	
	//[[NSNotificationCenter defaultCenter] postNotificationName:@"OutlineChangedNotification" object:nil];
	
	/*
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	NSData* data = [ud objectForKey: @"Organiser"];
	
	if( data != nil )
	{
	id array = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	
		//NSLog(@"org %@",[array description]);
		
	[organiserArray removeAllObjects];
	[organiserArray addObjectsFromArray: array];
	
		[[NSNotificationCenter defaultCenter] postNotificationName:@"OutlineChangedNotification" object:nil];
	}
	 */
	
	
}
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	[ud addObserver:self forKeyPath:@"defaultQueryTemplate" options:NSKeyValueObservingOptionNew
			context:nil];
	
	
	//modifiy menu
	NSLog(@"[self OSVersion] %d",[self OSVersion]);
	if( [self OSVersion] < 105 )
	{
		NSMenuItem *menuItem = [[NSApp mainMenu] itemWithTag:100];
		{
			NSMenuItem *qlMenuItem = [[menuItem submenu] itemWithTag:101];
			[[menuItem submenu] removeItem: qlMenuItem];
		}
		
	}
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	NSString* tagPrefix = [ud valueForKey:@"tagPrefix"];
	NSString* tagSuffix = [ud valueForKey:@"tagSuffix"];
	NSString* tagSeparator = [ud valueForKey:@"tagSeparator"];
	NSString* tagCommentPrefix = [ud valueForKey:@"tagCommentPrefix"];
	NSString* tagStartCode = [ud valueForKey:@"tagStartCode"];
	NSString* tagEndCode = [ud valueForKey:@"tagEndCode"];
	
	
	
	NSString* sample = [NSString stringWithFormat:@"%@%@Sample Tag1%@%@%@Tag2%@%@%@Tag3%@%@%@Additional Finder comments here",tagStartCode, tagPrefix,tagSuffix,tagSeparator,tagPrefix,tagSuffix,tagSeparator,tagPrefix,tagSuffix, tagEndCode, tagCommentPrefix];
	
	
	[tagSample setStringValue:sample ];
	
	
[[NSNotificationCenter defaultCenter] postNotificationName:@"SourceReselectRequestNotification"
													object:nil];	
}



- (void)applicationWillTerminate:(NSNotification *)aNotification
{
	[self saveOrganiser];
	
	/// Show Donation Alert
	
/*
	int result = NSRunAlertPanel(@"Donation info",@"Donation msg",@"Donate...",@"Not Now",@"I paid/No thanks");
	
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	if( [ud objectForKey:@"Donated"]  == nil)
	{
		
		
		
		if( result == NSAlertDefaultReturn ) // donate
		{
			NSString* str = @"https://www.paypal.com/us/cgi-bin/webscr?cmd=_flow&SESSION=mQQ3Q9upCx8Sr-1Sh-G46he7Qby1iAt2gopFV8PYBV-cXcCnNiTipS5-M74&dispatch=5885d80a13c0db1f80512b0980fcab74abc3e59231243d18fb86b96d6baa4d65";
			
			[[NSWorkspace sharedWorkspace] openURL: [NSURL URLWithString: str]];
		}
		
		if( result == NSAlertOtherReturn ) // donate
		{
	[ud setObject:[NSNumber numberWithBool:YES] 
											  forKey:@"Donated"];
		}
		
	}*/
	
}

-(int)OSVersion
{
	long SystemVersionInHexDigits;
	long MajorVersion, MinorVersion, MinorMinorVersion;
	
	Gestalt(gestaltSystemVersion, &SystemVersionInHexDigits);
	
	
	MinorMinorVersion = SystemVersionInHexDigits & 0xF;
	
	MinorVersion = (SystemVersionInHexDigits & 0xF0)/0xF;
	
	MajorVersion = ((SystemVersionInHexDigits & 0xF000)/0xF00) * 10 +
		(SystemVersionInHexDigits & 0xF00)/0xF0;
	
	 
	////NSLog(@"ver %ld", SystemVersionInHexDigits);
	////NSLog(@"%ld.%ld.%ld", MajorVersion, MinorVersion, MinorMinorVersion);	
	
	
	return (int)MajorVersion*10 + MinorVersion;
}


#pragma mark -


-(IBAction)website:(id)sender
{

	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	[ws openURL:[NSURL URLWithString:@"http://www.oneriver.jp/"] ];
}

-(NSString*)versionString
{
	NSBundle* bundle = [NSBundle bundleForClass:[self class]];
	NSString* myVersion = [bundle objectForInfoDictionaryKey:@"CFBundleVersion"];
	
	return myVersion;
}
-(IBAction)preset:(id)sender
{
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	int tag = [sender tag];
	
	if( tag == 0 )
	{
		//defaultQueryTemplate
		[ud setObject:@"(kMDItemTextContent == \"%@*\"cd) && (kMDItemContentType != com.apple.mail.emlx) && (kMDItemContentType != public.vcard)" forKey:@"defaultQueryTemplate"];
		
		
	}else if( tag == 1 )
	{
		[ud setObject:@"(* = \"%@*\"wcd || kMDItemTextContent = \"%@*\"cd) && (kMDItemContentType != com.apple.mail.emlx) && (kMDItemContentType != public.vcard)" forKey:@"defaultQueryTemplate"];

		
				
	}else if( tag == 2 )
	{
		[ud setObject:@"(* = \"%@*\"wcd || kMDItemTextContent = \"%@*\"cd)" forKey:@"defaultQueryTemplate"];
		
		
		
	}
	
	
	
	/// TAGGING
	
	if( tag == 100 )//natural
	{
		[ud setValue:@"" forKey:@"tagPrefix" ];
		[ud setValue:@"" forKey:@"tagSuffix" ];
		[ud setValue:@", " forKey:@"tagSeparator" ];
		[ud setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[ud setValue:@"" forKey:@"tagStartCode" ];
		[ud setValue:@"" forKey:@"tagEndCode" ];
		
		[ud setValue:@".*" forKey:@"tagGroupExtract" ];
		[ud setValue:@"[^,\\n]+" forKey:@"tagExtract" ];
		
	}
	else if( tag == 101 )//tagBot
	{
		[ud setValue:@"&" forKey:@"tagPrefix" ];
		[ud setValue:@"" forKey:@"tagSuffix" ];
		[ud setValue:@" " forKey:@"tagSeparator" ];
		[ud setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[ud setValue:@"" forKey:@"tagStartCode" ];
		[ud setValue:@"" forKey:@"tagEndCode" ];
		
		[ud setValue:@"&.*" forKey:@"tagGroupExtract" ];
		[ud setValue:@"(?<=&).[^ \\n]*" forKey:@"tagExtract" ];			
	}
	else if( tag == 102 )//punakea
	{
		[ud setValue:@"@" forKey:@"tagPrefix" ];
		[ud setValue:@";" forKey:@"tagSuffix" ];
		[ud setValue:@"" forKey:@"tagSeparator" ];
		[ud setValue:@"\n" forKey:@"tagCommentPrefix" ];
		[ud setValue:@"###begin_tags###" forKey:@"tagStartCode" ];
		[ud setValue:@"###end_tags###"  forKey:@"tagEndCode" ];
		
		
		[ud setValue:@"###begin_tags###.*###end_tags###" forKey:@"tagGroupExtract" ];
		[ud setValue:@"(?<=@)[^;]*(?=;)" forKey:@"tagExtract" ];	
		
	}

}
#pragma mark -

-(IBAction)test:(id)sender
{
/*
 IBOutlet id testDocField;
	IBOutlet id testPluginField;
	IBOutlet id testPreviewBySpotInsideField;
	IBOutlet id testPreviewByUserField;
	IBOutlet id testOtherInfo;
 */
	NSFileManager* fm = [NSFileManager defaultManager];
	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	
	NSString* documentPath = [testDocField title];
	NSString* pluginPath = [testPluginField title];


	if( ![pluginPath hasSuffix:@"mdimporter"] )
	{
		NSString* spotlightFolder = [pluginPath stringByAppendingPathComponent:@"Contents/Library/Spotlight/"];
		if( [fm fileExistsAtPath: spotlightFolder] )
		{
			NSArray* contents = [fm directoryContentsAtPath: spotlightFolder];
			
			if( contents!=nil && [contents count] > 0 )
			{
				int hoge;
				for( hoge=0; hoge<[contents count];hoge++)
				{
					NSString* aPath = [contents objectAtIndex: hoge];
					if( [aPath hasSuffix:@"mdimporter"] )
					{
						pluginPath = [spotlightFolder stringByAppendingPathComponent:aPath ];
						
					}
				}
				
			}
		}
	}
	
	
	if( [pluginPath hasSuffix:@"mdimporter"] )
	{
		
		NSString* targetFilePath_converted = [documentPath stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
		NSURL* anUrl = [NSURL URLWithString: targetFilePath_converted];
		FSRef ref;
		CFURLGetFSRef(anUrl,&ref);
		CFTypeRef outValue;
		LSCopyItemAttribute (
							 &ref,
							 kLSRolesAll,
							 kLSItemContentType,
							 &outValue
							 );
		
		if( outValue == nil ) return nil;
		
		NSString* uti = [NSString stringWithString:outValue];
		CFRelease(outValue);

		
		
		
		NSDictionary* dict = [SISpotlightTextContentRetriever executeMDImporterAtPath:pluginPath
														 forPath:targetFilePath_converted
															 uti:uti];
		
		[testPreviewByUserField setString:[dict description]];
	}else
	{
		[testPreviewByUserField setString:@"Reading failed"];

	}
	
	
	NSDictionary* dict =  [SISpotlightTextContentRetriever metaDataOfFileAtPath: documentPath];
	[testPreviewBySpotInsideField setString:[dict description]];

	
	
	//////////////
	
	//testOtherInfo
	
	
	
	
	NSString* str = [NSString stringWithFormat:@"[LOADED PLUGINS] \n\n %@ \n\n\n\n[UNLOADED PLUGINS] \n\n %@ \n\n\n\n [UTI HANDLER TABLE] \n\n %@ ", [SISpotlightTextContentRetriever loadedPlugIns], 
		[SISpotlightTextContentRetriever unloadedPlugIns],
		[SISpotlightTextContentRetriever contentTypesForMDImporter]];
		
		[testOtherInfo setString:str];

	
}



@end
