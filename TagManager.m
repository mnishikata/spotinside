#import "TagManager.h"
#import "NSTextView (coordinate extension).h"
#import "NSFileManager+NNExtensions.h"
//#import "EdgiesLibrary.h"
#import "AGRegex.h"
#import "TagAttachmentTextView.h"
//#import "TaggingAttachmentCell.h"
#import <WebKit/WebKit.h>
@implementation TagManager

#define UD_PASTE_AS_WEB_ARCHIVE YES
id preferenceValueForKey(NSString* key);

id preferenceValueForKey(NSString* key)
{
	return [[NSUserDefaults standardUserDefaults] valueForKey:key];	
}

+(TagManager*)sharedTagManager
{
	return [[NSApp delegate] tagManager];
}
- (id) init {
	self = [super init];
	if (self != nil) {
		
		[NSBundle loadNibNamed:@"TagManager" owner:self];
		
		activeCell = nil;
	}
	return self;
}

-(NSArray*)tagArrayInComment:(NSString*)rawString
{
	NSString* tagGroupExtract;
	NSString* tagExtract;
	
	
	tagGroupExtract = preferenceValueForKey(@"tagGroupExtract");
	tagExtract = preferenceValueForKey(@"tagExtract");
	
	if( tagGroupExtract == nil || tagExtract == nil ) return [NSArray array];

	AGRegex* regex = [AGRegex regexWithPattern:tagGroupExtract];
	AGRegexMatch* firstMatch = [regex findInString: rawString];
	
	if( firstMatch == nil ) return [NSArray array];
	
	NSRange matchedRange = [firstMatch range];
	NSString* tagGroupString = [rawString substringWithRange:matchedRange];
	
	regex = [AGRegex regexWithPattern:tagExtract];
	NSArray* array = [regex  findAllInString:tagGroupString ];

	NSMutableArray* tagStringArray = [NSMutableArray array];
	int hoge;
	for( hoge = 0; hoge <[array count]; hoge++ )
	{
		AGRegexMatch* match = [array objectAtIndex:hoge];
		matchedRange = [match range];
		NSString* tagString = [tagGroupString substringWithRange:matchedRange];
		tagString = [tagString omitSpacesAtBothEnds];
		[tagStringArray addObject: tagString];
	}
	
	
	//NSLog(@"Exisiting tags %@",[tagStringArray description]);
	return tagStringArray;
}

-(NSString*)additionalCommentInComment:(NSString*)rawString
{
	NSString* tagGroupExtract;
	tagGroupExtract = preferenceValueForKey(@"tagGroupExtract");
	if( tagGroupExtract == nil  ) return rawString;
	
	AGRegex* regex = [AGRegex regexWithPattern:tagGroupExtract];
	NSString* string = [regex  replaceWithString:@"" inString:rawString limit:1];

	NSString* commentPrefix = preferenceValueForKey(@"tagCommentPrefix");

	if( [string hasPrefix:commentPrefix ] )
	{
		string = [string substringFromIndex: [commentPrefix length]];
		
	}
	
	
	return string;

}

-(NSString*)commentOfFileAt:(NSString*)path
{
	NSString* comment = nil;
	
	/*
	 slow....
	 comment = [[NSFileManager defaultManager]  commentForURL:[NSURL fileURLWithPath:path]];
	 */
	
	MDItemRef mditem =  MDItemCreate (nil,path);
	
	if( mditem != nil ) 
	{
		comment = MDItemCopyAttribute(mditem, kMDItemFinderComment );
		CFRelease(mditem);
		
		if( comment != nil ) [comment autorelease];
	}
	
	
	if( comment == nil ) comment = @"";

	return comment;
	
}


-(void)showSheetForWindow:(NSWindow*)aWindow forFiles:(NSArray*)fullPaths sender:(id)aCell
{
	if( [window isVisible] )
	{
		//NSBeep();
		return;
	}
	

	[NSApp activateIgnoringOtherApps:YES];
	
	targetPathArray = [NSMutableArray arrayWithArray: fullPaths];
	[targetPathArray retain];
	
	windowForSheet = [aWindow retain];
	
	
	if( [targetPathArray count] > 1 )
	{
		[doAll setHidden:NO];
	}else
	{
		[doAll setHidden:YES];
	}
	
	/*
	if( [aCell isKindOfClass:[TaggingAttachmentCell class] ] )
	{
		[activeCell release];
		activeCell = [aCell retain];
	}else*/
	{
		[activeCell release];
		activeCell = nil;
	}
	
	
	[self startSheet];
	
}

-(void)startSheet
{
	NSString* path = [targetPathArray objectAtIndex:0];
	
	[comment setStringValue:@""];
	[filename setStringValue:[path lastPathComponent]];
	[image setImage: [[NSWorkspace sharedWorkspace] iconForFile: path] ];

	
	//create attributed strings
	
	id obj = preferenceValueForKey(@"tagList");
	NSAttributedString* as=nil;
	if( obj ) as  = [NSKeyedUnarchiver unarchiveObjectWithData:obj];
	
	if( as )
	{
		[[textView textStorage] setAttributedString: as];
	}
	/////
	
	
	//tagging cell の場合、さらにデフォのタグを選択
	
	if( activeCell )
	{
		//[textView selectTags:[activeCell tagTitles] addWhenNotInList:YES];
	}
	
	////
	
	
	NSString* rawComment = [self commentOfFileAt: path];
	
	if( rawComment != nil && ![rawComment isEqualToString:@""] )
	{
		NSArray* exsitingTags = [self tagArrayInComment: rawComment];
		NSString* additionalComment = [self additionalCommentInComment:rawComment];
		
		[textView selectTags:exsitingTags addWhenNotInList:YES];
		[comment setStringValue:  additionalComment];
	}
	
	
	
	[NSApp beginSheet:window modalForWindow:windowForSheet
		modalDelegate:nil didEndSelector:nil contextInfo:nil];

}

- (IBAction)buttonClicked:(id)sender
{
	
	[textView didChangeText];
	//NSLog(@"buttonClicked");
	
	int tag = [sender tag];
	BOOL doAllFlag = ( ![doAll isHidden] & [doAll state] == NSOnState );
	NSString* path;

	
	path = [targetPathArray objectAtIndex:0];	
	
	if( tag == 0 )
	{
		NSString* commentStr = [comment stringValue];
		NSArray* tags = [textView tagAttachmentCellArrayInRange:[textView fullRange]];
		
		NSMutableArray* tagStrings = [NSMutableArray array];
		int hoge;
		for( hoge = 0; hoge < [tags count]; hoge++ )
		{
			id cell = [tags objectAtIndex:hoge];
			if( [cell state] == NSOnState )
			{
				[tagStrings addObject:[cell title] ];
				
			}
		}
		
		
		
		//renaming		
		NSString* newName = [filename stringValue];
		
		if( ![newName isEqualToString:[path lastPathComponent] ] )
		{
			newName = [[newName safeFilename] uniqueFilenameForFolder: [path stringByDeletingLastPathComponent]];
			if( [[newName pathExtension] isEqualToString: @""] && ![[path pathExtension] isEqualToString: @""])
			{
				newName = [newName stringByAppendingPathExtension:[path pathExtension] ];
			}
			//NSLog(@"newName %@",newName);
			
			NSString* newPath = [[path stringByDeletingLastPathComponent] stringByAppendingPathComponent:newName ];
			[[NSFileManager defaultManager] movePath:path toPath:newPath handler:nil];
			
			[targetPathArray replaceObjectAtIndex:0 withObject:newPath];
		}
		
		
		//create tags pre
		
		NSString* tagPrefix;
		NSString* tagSuffix;
		NSString* tagSeparator;
		NSString* tagCommentPrefix;
		NSString* tagStartCode;
		NSString* tagEndCode;

		
		tagPrefix = preferenceValueForKey(@"tagPrefix");
		tagSuffix = preferenceValueForKey(@"tagSuffix");
		tagCommentPrefix = preferenceValueForKey(@"tagCommentPrefix");
		tagSeparator = preferenceValueForKey(@"tagSeparator");
		tagStartCode = preferenceValueForKey(@"tagStartCode");
		tagEndCode = preferenceValueForKey(@"tagEndCode");
		
		if( !tagPrefix ) tagPrefix = @"";
		if( !tagSuffix ) tagSuffix = @"";
		if( !tagSeparator ) tagSeparator = @"";
		if( !tagCommentPrefix ) tagCommentPrefix = @"";
		if( !tagStartCode ) tagStartCode = @"";
		if( !tagEndCode ) tagEndCode = @"";

		
		/// create tags
		

		do
		{
			
			path = [targetPathArray objectAtIndex:0];	
		
			NSMutableString* mstr = [[[NSMutableString alloc] init] autorelease];

			if( [tagStrings count] > 0 )
			{
				[mstr appendString:tagStartCode];
				int hoge;
				for( hoge = 0; hoge <[ tagStrings count]; hoge++ )
				{
					id obj = [tagStrings objectAtIndex:hoge];
					if( hoge > 0 )
					{
						[mstr appendString: 	tagSeparator];
					}
					[mstr appendFormat:@"%@%@%@",tagPrefix,obj,tagSuffix];
					
				}
				[mstr appendString: tagEndCode];

				
			}
			[mstr appendString: tagCommentPrefix];
			[mstr appendString: commentStr];

			
			//NSLog(@"Tag %@",mstr);
			BOOL flag = [[NSFileManager defaultManager] setComment:mstr forURL:[NSURL fileURLWithPath:path]];

			if( !flag )
			{
			
				[NSAlert alertWithMessageText:@"Tagging Failed" defaultButton:nil alternateButton:nil otherButton:nil informativeTextWithFormat:@"Unknown error occured" , nil];
			}
			
			

			[targetPathArray removeObjectAtIndex:0];

			
		}while( doAllFlag && [targetPathArray count]>0  );


		
	}else
	{
		if( doAllFlag )
		{
			[targetPathArray removeAllObjects];
			
		}
		
		else
		{
			[targetPathArray removeObjectAtIndex:0];

		}
	}
	
	//update tag text view default
	
	
	
	
	[window close];
	[NSApp endSheet:window];
	
	if( [targetPathArray count] > 0 )
	{
		[self startSheet];
	}else
	{
		[windowForSheet release];
		[targetPathArray release];
	}
	
}


-(void)dropClipboardTo:(NSString*)dropLocation
{
	return;}

@end
