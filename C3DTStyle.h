//
//  C3DTStyle.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTStyle.h,v 1.2 2003/06/19 20:30:49 pmanna Exp $
//
// $Log: C3DTStyle.h,v $
// Revision 1.2  2003/06/19 20:30:49  pmanna
// Moved OpenGL headers to superclass
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTObject.h"
#import <OpenGL/glu.h>
#import <OpenGL/glext.h>


@interface C3DTStyle : C3DTObject {
    BOOL	_isReference;
}

- (void)apply;
- (void)restore;

- (BOOL)isReference;
- (void)setIsReference:(BOOL)newIsReference;

@end
