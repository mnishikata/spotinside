//
//  FolderListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "TagListItem.h"
#import "NSFileManager+NNExtensions.h"
#import "AliasListItem.h"

@implementation TagListItem



- (id) init {
	self = [super init];
	if (self != nil) {


		name = [ NSLocalizedString(@"Untitled Tag",@"") retain];
		canDelete = YES;
		canMove = YES;
		canDrop = YES;
		canSelect = YES;
		
		icon = nil;
		

	}
	return self;
}


- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		canDelete = YES;
		canMove = YES;
		canDrop = YES;
		canSelect = YES;
		

	}
	return self;
	
	
}

-(NSString*)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{
	NSString* string = [NSString stringWithFormat:@"(%@)&&(%@)",[self query],aQuery];
	
	[super searchWithAdditionalQuery:string forArrayController:anArrayController 
						   endTarget:aTarget andSelector:aSelector];
	
	return string;
}


-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{
	//NSLog(@"search");
	
	if( ![name isEqualToString: NSLocalizedString(@"Untitled Tag",@"")] )
		[super searchForArrayController:arrayController withEndTarget:aTarget andSelector:aSelector];
}

/*
-(void)search
{
	//NSLog(@"search");
	
	if( ![name isEqualToString:@"Untitled Tag"] )
		[super search];
}
*/


-(NSString*)kind {return @"tagger";}


-(void )setQuery:(id)value
{
}

-(NSString*)query  {
	
	if( name == nil || [name isEqualToString:@""] ) return @"";
		
		
	NSString* format = @"((kMDItemFinderComment = '&%@ *')   || (kMDItemFinderComment = '* &%@ *') || (kMDItemFinderComment = '* &%@')   || (kMDItemFinderComment = '&%@') || (kMDItemKeywords = %@ && kMDItemContentType == com.apple.mail.emlx))";

 format = [NSString stringWithFormat:   format,
	 name,name,name,name,name];

	
	return format;
}

-(void)dropItem:(AliasListItem*)object
{
	if( ![object isKindOfClass:[AliasListItem class]] ) {
		NSBeep();
		return;
	}
	
	
	
	NSString* path = [object path];
	if( path == nil ) {
		NSBeep(); return;
	}

	

	
	NSString* comment = nil;
	
	/*
	 slow....
	comment = [[NSFileManager defaultManager]  commentForURL:[NSURL fileURLWithPath:path]];
	*/
	
	MDItemRef mditem =  MDItemCreate (nil,path);
	
	if( mditem != nil ) 
	{
		comment = MDItemCopyAttribute(mditem, kMDItemFinderComment );
		CFRelease(mditem);
		
		if( comment != nil ) [comment autorelease];
	}
	
	
	if( comment == nil ) comment = @"";
	NSString* tagString = [@"&" stringByAppendingString:[self name]];

	int modKey = GetCurrentKeyModifiers( );

	if(  (modKey | 1024) == (2048 | 1024) ) // option
	{
		NSMutableString* mstr = [[[NSMutableString alloc] initWithString: comment ] autorelease];
		
		[mstr replaceOccurrencesOfString:tagString withString:@""
								 options:0 range:NSMakeRange(0,[mstr length])];
		
		
		comment = mstr;
		
	}else
	{
			
		
		if( [comment rangeOfString:tagString].length != 0 ) //alread exist
		{
			// do nothing
		}else
		{
			comment = [comment stringByAppendingString: @" "];
			comment = [comment stringByAppendingString: tagString];

		}
		
	}
	
	[[NSFileManager defaultManager] setComment:comment forURL:[NSURL fileURLWithPath:path]];

}


-(void)setName:(NSString*)aName
{
	
	isRenaming = NO;

	if( ![aName isEqualToString:name] )
	{
		[UNDO_MANAGER registerUndoWithTarget:self 
									selector:@selector(undoRename:) object:[self name]];
		[UNDO_MANAGER setActionName: NSLocalizedString(@"Rename",@"")];
	}
	
	if( aName == nil ) 
		aName = @"";

	
	NSMutableString* string = [[NSMutableString alloc] initWithString:aName];
	[string replaceOccurrencesOfString:@"&" withString:@"+" options:0 range:NSMakeRange(0,[string length])];
		[string replaceOccurrencesOfString:@" " withString:@"_" options:0 range:NSMakeRange(0,[string length])];
	
	[name release];
	name = [string retain];
	
	if( contents == nil ) 
	{
		[[NSNotificationCenter defaultCenter] postNotificationName:@"SourceReselectRequestNotification"
															object:nil];	

		//NSBeep();
		//[self search];
	}
}

-(NSImage*)icon
{
	if( icon == nil )
	icon = [[NSImage imageNamed:@"tagbot.png"] retain];
	
	return [super icon];

}


@end
