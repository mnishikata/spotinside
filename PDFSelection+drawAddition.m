//
//  PDFSelection+drawAddition.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/03/25.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "PDFSelection+drawAddition.h"
#import "ColorCheckbox.h"


@implementation PDFSelection (drawAddition)
- (void) drawForPage: (PDFPage *) page withBox: (PDFDisplayBox) box active: (BOOL) active
{
	////NSLog(@"drawForPage");
	NSRect bound = [self boundsForPage:page];

	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	NSColor* highlightColor;
	
	id obj = [ud objectForKey:@"highlightColor"];
	
	if( obj != nil )
		highlightColor = [[NSUnarchiver unarchiveObjectWithData:obj] retain];
	
	else
	{
		highlightColor = [[NSColor colorWithCalibratedRed:0.2
													green:0.2
													 blue:1.0 alpha:1.0] retain];
		
		[ud setObject:[NSArchiver archivedDataWithRootObject:highlightColor]
			   forKey:@"highlightColor"];
	}
	
	int openEnd = 0;

	[ColorCheckbox roundedBoxFrame:highlightColor
							 frame:bound
							 curve:3.0
						   openEnd:openEnd];
	
}
/*
- (void) drawForPage: (PDFPage *) page  active: (BOOL) active
{
	NSRect bound = [self boundsForPage:page];
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	NSColor* highlightColor;
	
	id obj = [ud objectForKey:@"highlightColor"];
	
	if( obj != nil )
		highlightColor = [[NSUnarchiver unarchiveObjectWithData:obj] retain];
	
	else
	{
		highlightColor = [[NSColor colorWithCalibratedRed:0.2
													green:0.2
													 blue:1.0 alpha:1.0] retain];
		
		[ud setObject:[NSArchiver archivedDataWithRootObject:highlightColor]
			   forKey:@"highlightColor"];
	}
	
	int openEnd = 0;
	
	[ColorCheckbox roundedBoxFrame:highlightColor
							 frame:bound
							 curve:3.0
						   openEnd:openEnd];
	
}
*/

@end
