//
//  NSString+Word.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/14.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "NSString+Words.h"
#import <OgreKit/OgreKit.h>

@implementation NSString (Words)

-(NSArray*)words
{
	OGRegularExpression *regex = [OGRegularExpression regularExpressionWithString:@"\\w+" options:OgreIgnoreCaseOption];
	
	NSMutableArray* strArray = [NSMutableArray array];
	[strArray addObject:self];
	
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:self];
	OGRegularExpressionMatch    *match;	// マッチ結果
	while ((match = [enumerator nextObject]) != nil) {	// 順番にマッチ結果を得る
		
		
		[strArray addObject:  [match matchedString]];
	}
	
	return strArray;
}

-(unsigned)reverseLocation:(unsigned)locationFromLast
{
	
	return [self length] - locationFromLast -1 ; 
}

-(NSString*)omitSpacesAtBothEnds
{
	NSMutableString* newStr = [[NSMutableString alloc] initWithString:self];
	[newStr autorelease];
	
	while( [newStr length] > 0 )
	{
		NSString* moji = [newStr substringToIndex:1];
		
		if( [ moji isEqualToString:@" " ] || [ moji isEqualToString:[NSString stringWithCString:"　"] ] )
			[newStr deleteCharactersInRange:NSMakeRange(0,1)];
		
		else
			break;
		
		
		
	}
	
	
	
	if( [newStr length] != 0 )
	{
		while( [newStr length] > 0 )
		{
			NSString* moji = [newStr substringFromIndex:[newStr reverseLocation:0]];
			
			if( [ moji isEqualToString:@" " ] || [ moji isEqualToString:[NSString stringWithCString:"　"] ] )
				[newStr deleteCharactersInRange:NSMakeRange([newStr reverseLocation:0],1)];
			
			else
				break;
			
		}
	}
	
	
	return newStr;
}

-(NSString*)uniqueFilenameForFolder:(NSString*)folderPath
{
	NSString* fullpath = [folderPath stringByAppendingPathComponent:self];
	fullpath = [fullpath uniquePathForFolder];
	
	return [fullpath lastPathComponent];
}

-(NSString*)uniquePathForFolder // change path to unique path
{
	NSFileManager* fm = [NSFileManager defaultManager];
	NSString* returnValue = [NSString stringWithString: self ];
	NSString* ext = [self pathExtension];
	unsigned hoge =1;
	
	while( [fm fileExistsAtPath:returnValue] )
	{
		
		returnValue = [NSString stringWithFormat:@"%@-%d",[self stringByDeletingPathExtension],hoge];
		
		if( ![ext isEqualToString:@""] )
			returnValue = [returnValue stringByAppendingPathExtension: ext];
		
		
		hoge++;
		
	}
	return returnValue;
	
}

@end
