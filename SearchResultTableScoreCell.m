//
//  SearchResultTableScoreCell.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/29.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

/*
 
 indicator セルだと、数字が書き換わる！？
 
 */


#import "SearchResultTableScoreCell.h"


@implementation SearchResultTableScoreCell

- (id) init {
	self = [super init];
	if (self != nil) {
		
		
		fillPatternImage = [[NSImage alloc] initByReferencingFile:
			[[NSBundle bundleForClass:[self class]] pathForResource:@"fillPattern" ofType:@"tiff"]];

		
		
		[self setType:NSNullCellType  ];
	}
	return self;
}

- (void)drawInteriorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	float score = [[self title] floatValue];
	
	
	
	//trim 
	cellFrame.origin.x += 1;
	cellFrame.origin.y += 0;
	cellFrame.size.width -= 2;
	cellFrame.size.height -= 1;
	
	
	cellFrame.size.width = cellFrame.size.width * score;
	
	[[NSColor colorWithPatternImage:fillPatternImage] set];
	[NSBezierPath fillRect: cellFrame];
	


	
}


@end
