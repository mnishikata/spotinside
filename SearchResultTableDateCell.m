//
//  SearchResultTableDateCell.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/29.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SearchResultTableDateCell.h"


@implementation SearchResultTableDateCell

- (id) init {
	self = [super init];
	if (self != nil) {

	
		NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
		
		[formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
		[formatter setDateStyle:kCFDateFormatterLongStyle];
		[formatter setTimeStyle:kCFDateFormatterShortStyle];
	
		[self setFormatter:formatter];

	}
	return self;
}

- (NSRect)drawingRectForBounds:(NSRect)theRect

{
	NSRect rect =  [super drawingRectForBounds:(NSRect)theRect];
	////NSLog(@"drawingRectForBounds %f" , rect.size.width);

	NSDateFormatter* formatter = [self formatter];
	if( rect.size.width < 80 && [formatter dateStyle] != kCFDateFormatterShortStyle)
	{
		[formatter setDateStyle:kCFDateFormatterShortStyle];
		[formatter setTimeStyle:kCFDateFormatterNoStyle];
				
	}else if( rect.size.width > 80 && [formatter dateStyle] != kCFDateFormatterLongStyle)
	{
		[formatter setDateStyle:kCFDateFormatterLongStyle];
		[formatter setTimeStyle:kCFDateFormatterShortStyle];

	}
	
	
	return rect;
}
@end
