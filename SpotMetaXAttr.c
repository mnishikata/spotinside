/*
 *  SpotMetaXAttr.c
 *  SpotMetaReadWrite
 *
 *  Created by Ben Summers on 25/09/2005.
 *  Copyright 2005 Ben Summers. All rights reserved.
 *
 */

#include <CoreFoundation/CoreFoundation.h>
#include <sys/xattr.h>
#include <errno.h>

#include "SpotMetaXAttr.h"

// doesn't need to be terribly high, as the user names can't be that long anyway
#define MAX_STRING_SIZE_FOR_CONVERSION	256

// ===============================================================================================
// 		Read attributes
// ===============================================================================================

#define READ_RETURN_ERROR	{error = true; /* printf("read fail on line %d\n", __LINE__);*/ goto finish;}

static CFTypeRef SpotMetaXAttrRead_DecodeValue(char Type, CFStringRef String);

CFDictionaryRef SpotMetaXAttrRead(const char *filename, bool *pHasSpotMetaMarkerOut, int Flags)
{
	bool error = false;
	bool hasSpotMetaMarker = false;
	char *xattrNamesBuffer = 0;
	int xattrDataBufferSize = 0;
	char *xattrDataBuffer = 0;
	CFStringRef string = 0;
	CFTypeRef value = 0;

	// Create a dictionary object
	CFMutableDictionaryRef metaData = CFDictionaryCreateMutable(NULL, 0,
		&kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
	if(metaData == NULL)
	{
		return NULL;
	}

	// How much data is required for the attributes?
	ssize_t xattrNamesBufferSize = listxattr(filename, NULL, 0, 0);
	if(xattrNamesBufferSize < 0)
	{
		READ_RETURN_ERROR
	}
	else if(xattrNamesBufferSize > 0)
	{
		// There is some data there to look at
		xattrNamesBuffer = (char*)malloc(xattrNamesBufferSize + 4);
		if(xattrNamesBuffer == NULL) READ_RETURN_ERROR;
		
		ssize_t ns = listxattr(filename, xattrNamesBuffer, xattrNamesBufferSize, 0);
		if(ns < 0)
		{
			READ_RETURN_ERROR
		}
		else if(ns > 0)
		{
			// Read all the attribute values
			const char *xattrName = xattrNamesBuffer;
			while(xattrName < (xattrNamesBuffer + ns))
			{
				// Store size of name
				int xattrNameSize = strlen(xattrName);
			
				// Is it one of ours?
				if(strncmp(xattrName, SPOTMETA_XATTR_PREFIX, sizeof(SPOTMETA_XATTR_PREFIX)-1) == 0)
				{
					// Is it the marker attribute?
					if(xattrName[sizeof(SPOTMETA_XATTR_PREFIX)-1] == SPOTMETA_XATTR_MARKER_SINGLE_CHAR
						&& xattrName[sizeof(SPOTMETA_XATTR_PREFIX)] == '\0')
					{
						hasSpotMetaMarker = true;
					}
					else
					{
						// Parse the name to get the type.
						// Carefully. Don't go over the end of the terminated string.
						bool ok = true;
						
						// Multivalued flag
						bool multiValued = false;
						switch(xattrName[sizeof(SPOTMETA_XATTR_PREFIX)-1])
						{
						case 'm': multiValued = true; break;
						case 's': multiValued = false; break;
						default: ok = false; break;
						}
						
						// Type -- do checking now before the _ is checked, careful not to read too far
						char type = 0;
						if(ok)
						{
							type = xattrName[sizeof(SPOTMETA_XATTR_PREFIX)];
							switch(type)
							{
							case 'n': case 't': case 'c': case 'd': case 'b': ok = true; break;
							default: ok = false; break;
							}
						}
						
						// Check that there's a _ after the type and there's something after that
						if(ok)
						{
							// OK re terminator because of order of execution
							if(xattrName[sizeof(SPOTMETA_XATTR_PREFIX) + 1] != '_'
								|| xattrName[sizeof(SPOTMETA_XATTR_PREFIX) + 2] == 0)
							{
								ok = false;
							}
						}
						
						// Make sure there's enough space for the attribute data
						if(ok)
						{
							ssize_t dataSize = getxattr(filename, xattrName, NULL, 0, 0, 0);
							if(dataSize < 0)
							{
								if(errno == ENOATTR)
								{
									// Deleted from under us
									ok = false;
								}
								else
								{
									READ_RETURN_ERROR
								}
							}
							else if(dataSize == 0)
							{
								// something must have removed all the data from under us
								ok = false;
							}
							else
							{
								// Make sure there's enough space in the buffer to get the attribute
								if(xattrDataBuffer == 0)
								{
									xattrDataBuffer = (char*)malloc(dataSize + 4);
									xattrDataBufferSize = dataSize + 4;
								}
								else if(xattrDataBufferSize < (dataSize + 4))
								{
									char *resized = (char*)realloc(xattrDataBuffer, dataSize + 4);
									if(resized == NULL) READ_RETURN_ERROR;
									xattrDataBuffer = resized;
									xattrDataBufferSize = dataSize + 4;
								}
							}
						}
						
						// Read the data!
						ssize_t dataSize = 0;
						if(ok)
						{
							dataSize = getxattr(filename, xattrName, xattrDataBuffer,
								xattrDataBufferSize - 1, // for terminator
								0, 0);
							if(dataSize < 0)
							{
								if(errno == ENOATTR)
								{
									// Deleted from under us
									ok = false;
								}
								else
								{
									READ_RETURN_ERROR
								}
							}
							else if(dataSize == 0)
							{
								// something must have deleted this from under us
								ok = false;
							}
							else
							{
								// Terminate the data
								xattrDataBuffer[dataSize] = '\0';
							}
							// Got the data in the buffer
						}
						
						// Decode the data
						value = NULL;
						if(ok)
						{
							// Make a string from the data
							CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault,
								(const UInt8 *)xattrDataBuffer, dataSize, kCFAllocatorNull);
							if(data == NULL) READ_RETURN_ERROR;
							string = CFStringCreateFromExternalRepresentation(kCFAllocatorDefault,
								data, kCFStringEncodingUTF8);
							if(string == NULL) READ_RETURN_ERROR;
							CFRelease(data);
						
							if(multiValued)
							{
								// Multi-valued -- split the string up into an array, then parse
								// each bit in turn.
								CFArrayRef values = CFStringCreateArrayBySeparatingStrings(kCFAllocatorDefault,
									string, CFSTR(SPOTMETA_MULTIVALUE_SEPARATOR_STR));
								if(values == NULL) READ_RETURN_ERROR;
								
								// Create an array for the value
								value = CFArrayCreateMutable(kCFAllocatorDefault, 0, &kCFTypeArrayCallBacks);
								if(value == NULL) {CFRelease(values); READ_RETURN_ERROR;}
								
								// Run each string in turn...
								CFIndex numValues = CFArrayGetCount(values);
								CFIndex i = 0;
								for(i = 0; i < numValues; ++i)
								{
									CFStringRef s = CFArrayGetValueAtIndex(values, i);
									if(s != NULL)
									{
										CFTypeRef d = SpotMetaXAttrRead_DecodeValue(type, s);
										if(d != NULL)
										{
											// Save the value (if it decoded without problem)
											CFArrayAppendValue((CFMutableArrayRef)value, d);
											
											// Release the data returned
											CFRelease(d);
										}
									}
								}
							}
							else
							{
								// Single valued
								value = SpotMetaXAttrRead_DecodeValue(type, string);
								if(value == NULL)
								{
									// If parsing fails, just ignore it
									ok = false;
								}
							}
							
							CFRelease(string);
							string = NULL;
						}
						
						// Store the value in the return array
						if(ok && value != NULL)
						{
							// Turn the key into a string
							int nameSizeAdjust = ((Flags & SPOTMETA_XATTR_READFLAG_RETAIN_PREFIX) == 0)
								?(sizeof(SPOTMETA_XATTR_PREFIX)-1):0;
							// Safe to just adjust the start, because we know exactly what characters they are,
							// and none of them are multi-byte
							CFDataRef data = CFDataCreateWithBytesNoCopy(kCFAllocatorDefault,
								(const UInt8 *)(xattrName + nameSizeAdjust),
								xattrNameSize - nameSizeAdjust, kCFAllocatorNull);
							if(data == NULL) READ_RETURN_ERROR;
							CFStringRef key = CFStringCreateFromExternalRepresentation(kCFAllocatorDefault,
								data, kCFStringEncodingUTF8);
							if(key == NULL) READ_RETURN_ERROR;
							CFRelease(data);

							// Decode the key name
							CFStringRef decoded = NULL;
							if(Flags & SPOTMETA_XATTR_READFLAG_ENCODED_KEY_NAMES)
							{
								// Don't decode the name
								decoded = key;
								CFRetain(decoded);
							}
							else
							{
								// Decode the name for the caller
								// Note that we mustn't treat the first 1 or 3 _'s as the beginnings of an escape sequence
								decoded = SpotMetaDecodeStringFromXAttrName(key,
									((Flags & SPOTMETA_XATTR_READFLAG_RETAIN_PREFIX) == 0)?1:3);
							}
							if(decoded == NULL) READ_RETURN_ERROR;

							// Add to the dictionary
							CFDictionarySetValue(metaData, decoded, value);

							// Release the data
							CFRelease(decoded);
							decoded = NULL;
							CFRelease(key);
							key = NULL;
							CFRelease(value);
							value = NULL;
						}
					}
				}
			
				// Next attribute
				xattrName += xattrNameSize + 1;
			}
		}
	}

finish:
	if(error)
	{
		// error, clean up dictionary
		if(metaData != NULL)
		{
			CFRelease(metaData);
			metaData = NULL;
		}
	}
	else
	{
		// not error, optionally return information about the marker
		if(pHasSpotMetaMarkerOut != NULL)
		{
			*pHasSpotMetaMarkerOut = hasSpotMetaMarker;
		}
	}
	if(xattrNamesBuffer != 0) free(xattrNamesBuffer);
	if(xattrDataBuffer != 0) free(xattrDataBuffer);
	if(string != NULL) CFRelease(string);
	if(value != NULL) CFRelease(value);
	return metaData;
}

static CFTypeRef SpotMetaXAttrRead_DecodeValue(char Type, CFStringRef String)
{
	switch(Type)
	{
	case 't':
	case 'c':
		// For strings and choices, just return the string, after retaining it
		CFRetain(String);
		return String;
		break;
		
	case 'n':
		{
			// See if there's a '.' in the string so we can get an integer type where appropraite
			CFRange r = CFStringFind(String, CFSTR("."), 0);
			if(r.location == kCFNotFound)
			{
				// Looks like an integer
				SInt32 value = CFStringGetIntValue(String);
				return CFNumberCreate(kCFAllocatorDefault, kCFNumberSInt32Type, &value);
			}
			else
			{
				// Otherwise, treat it as a double
				double value = CFStringGetDoubleValue(String);
				return CFNumberCreate(kCFAllocatorDefault, kCFNumberDoubleType, &value);
			}
		}
		break;

	case 'b':
		// Check the string...
		if(CFEqual(String, CFSTR("0")))
		{
			return kCFBooleanFalse;
		}
		else if(CFEqual(String, CFSTR("1")))
		{
			return kCFBooleanTrue;
		}
		else
		{
			return NULL;
		}
		break;

	case 'd':
		{
			// Try to convert it to a date
			if(CFStringGetLength(String) == 14)		// YYYYMMDDHHMMSS
			{
				// Convert to normal C string
				char date[32];
				if(CFStringGetCString(String, date, sizeof(date), kCFStringEncodingASCII))
				{
					int year, month, day, hour, minute, second;	// types in CFGregorianDate are wrong size to do this directly
					if(sscanf(date, "%04d%02d%02d%02d%02d%02d", &year, &month, &day, &hour, &minute, &second) == 6)
					{
						CFGregorianDate greg;
						greg.year = year;
						greg.month = month;
						greg.day = day;
						greg.hour = hour;
						greg.minute = minute;
						greg.second = second;
						
						return CFDateCreate(kCFAllocatorDefault,
							CFGregorianDateGetAbsoluteTime(greg, NULL /* GMT */));
					}
				}
			}
		}
		break;
		
	default:
		return NULL;
		break;
	}
	
	return NULL;
}


// ===============================================================================================
// 		Write attributes
// ===============================================================================================

typedef struct
{
	const char *filename;
	int numXAttrWritten;
	int numErrors;
	int flags;
} SpotMetaXAttrWrite_Context;

static void SpotMetaXAttrWrite_Applier(const void *key, const void *value, void *context);
static CFStringRef SpotMetaXAttrWrite_GetStringRepForValue(const void *value, char expectedType);


bool SpotMetaXAttrWrite(const char *filename, CFDictionaryRef MetaData, int Flags)
{
	// Set up a nice context
	SpotMetaXAttrWrite_Context context;
	context.filename = filename;
	context.numXAttrWritten = 0;
	context.numErrors = 0;
	context.flags = Flags;
	
	// Iterate over the data, writing the attributes
	CFDictionaryApplyFunction(MetaData, SpotMetaXAttrWrite_Applier, &context);

	// If nothing was written, we need to write a marker to tell the importer that
	// nothing is supposed to be there.
	if(context.numXAttrWritten == 0)
	{
		if(setxattr(filename, SPOTMETA_XATTR_MARKER, "*", 1, 0, 0) != 0)
		{
			++context.numErrors;
		}
	}
	else
	{
		// But if something was written, remove it. Ignore errors, it's not important
		// if the removal fails.
		removexattr(filename, SPOTMETA_XATTR_MARKER, 0);
	}

	// Looked OK?
	return (context.numErrors == 0);
}

#define APPLIER_RETURN_ERROR {pcontext->numErrors++; /*printf("applier fail on line %d\n", __LINE__);*/ goto cleanup;}

static void SpotMetaXAttrWrite_Applier(const void *key, const void *value, void *context)
{
	// Objects which may need to be cleaned up.
	CFStringRef convertedKey = NULL;
	CFStringRef xattrName = NULL;
	CFDataRef xattrNameUTF8 = NULL;
	CFMutableStringRef xattrDataBuild = NULL;	// because of typing
	CFStringRef xattrData = NULL;
	CFDataRef xattrDataUTF8 = NULL;

	// Context
	SpotMetaXAttrWrite_Context *pcontext = (SpotMetaXAttrWrite_Context*)context;

	// Where do things start in this key?
	int typeLocationInKey = 0;
	if(pcontext->flags & SPOTMETA_XATTR_WRITEFLAG_PREFIX_INCLUDED)
	{
		typeLocationInKey = sizeof(SPOTMETA_XATTR_PREFIX)-1;
	}

	// Write this value... first of all, check the key.
	CFStringRef keyString = key;
	if(CFStringGetLength(keyString) <= 3)	// for <multi-valued><basic type>_, plus needs at least one char after
	{
		APPLIER_RETURN_ERROR
	}
	UniChar keyTypeInfo[4];
	CFRange keyTypeInfoRange = {typeLocationInKey+0,3};
	CFStringGetCharacters(keyString, keyTypeInfoRange, keyTypeInfo);
	if(keyTypeInfo[2] != '_')
	{
		// supposed to have a terminator there!
		APPLIER_RETURN_ERROR
	}
	
	// Then prepare the value for writing out to disc
	bool isNull = false;
	if(CFGetTypeID(value) == CFNullGetTypeID())
	{
		isNull = true;
	}
	else
	{
		// Encode the data
		if(keyTypeInfo[0] == 's')
		{
			// Single value. Get the string representation
			xattrData = SpotMetaXAttrWrite_GetStringRepForValue(value, keyTypeInfo[1]);
			if(xattrData == NULL)
			{
				APPLIER_RETURN_ERROR
			}
		}
		else if(keyTypeInfo[0] == 'm')
		{
			// Make sure the value is an array
			if(CFGetTypeID(value) != CFArrayGetTypeID())
			{
				APPLIER_RETURN_ERROR
			}
			
			// Create a mutable string to get us going
			xattrDataBuild = CFStringCreateMutable(kCFAllocatorDefault, 0);
			if(xattrDataBuild == NULL) APPLIER_RETURN_ERROR;
			
			// And then go through the array, appending values
			CFIndex numValues = CFArrayGetCount(value);
			if(numValues == 0) isNull = true;		// treat an empty array as a null value
			CFIndex i = 0;
			for(i = 0; i < numValues; ++i)
			{
				// Get the string representation of this data
				CFStringRef s = SpotMetaXAttrWrite_GetStringRepForValue(CFArrayGetValueAtIndex(value, i), keyTypeInfo[1]);
				if(s == NULL) APPLIER_RETURN_ERROR;
			
				// Add separator?
				if(i != 0)
				{
					UniChar c = SPOTMETA_MULTIVALUE_SEPARATOR;
					CFStringAppendCharacters(xattrDataBuild, &c, 1);
				}
				
				// Add string
				CFStringAppend(xattrDataBuild, s);
			
				// Release the temporary string
				CFRelease(s);
			}
			
			xattrData = xattrDataBuild;
			xattrDataBuild = NULL;
		}
		else
		{
			// Bad data type
			APPLIER_RETURN_ERROR
		}

		// Final check on whether something was created
		if(xattrData == NULL)
		{
			APPLIER_RETURN_ERROR
		}
	}

	// Convert the string and add the prefix
	if(pcontext->flags & SPOTMETA_XATTR_WRITEFLAG_PREFIX_INCLUDED)
	{
		if(pcontext->flags & SPOTMETA_XATTR_WRITEFLAG_ENCODED_KEY_NAMES)
		{
			// All looks good as it is!
			xattrName = keyString;
			CFRetain(xattrName);
		}
		else
		{
			// It'll need encoding, but the prefix is already there.
			xattrName = SpotMetaEncodeStringForXAttrName(keyString, 3 /* don't encode the initial _s */);
		}
	}
	else
	{
		CFStringRef convertedKey = NULL;
		if(pcontext->flags & SPOTMETA_XATTR_WRITEFLAG_ENCODED_KEY_NAMES)
		{
			// Just use the supplied key, and retain it
			convertedKey = keyString;
			CFRetain(convertedKey);
		}
		else
		{
			convertedKey = SpotMetaEncodeStringForXAttrName(keyString, 1 /* don't encode the _ after the typename */);
		}
		CFMutableStringRef xattrNameMut = NULL;
		if(convertedKey == NULL
			|| (xattrNameMut = CFStringCreateMutableCopy(
					kCFAllocatorDefault,
					sizeof(SPOTMETA_XATTR_PREFIX) + CFStringGetLength(convertedKey),
					CFSTR(SPOTMETA_XATTR_PREFIX))) == NULL)
		{
			APPLIER_RETURN_ERROR
		}
		CFStringAppend(xattrNameMut, convertedKey);	// won't fail as size is specified above
		xattrName = xattrNameMut;
	}

	// Make a UTF-8 encoded version of the name
	xattrNameUTF8 = CFStringCreateExternalRepresentation(kCFAllocatorDefault,
			xattrName, kCFStringEncodingUTF8, ' ');
	if(xattrNameUTF8 == NULL) APPLIER_RETURN_ERROR;
	
	// And now we need to zero terminate it
	char xattrNameUTF8ZeroTerminated[SPOTMETA_MAX_XATTR_NAME + 4];
	int xattrNameUTF8Length = CFDataGetLength(xattrNameUTF8);
	if(xattrNameUTF8Length > SPOTMETA_MAX_XATTR_NAME)
	{
		APPLIER_RETURN_ERROR
	}
	CFDataGetBytes(xattrNameUTF8, CFRangeMake(0,xattrNameUTF8Length), (UInt8*)xattrNameUTF8ZeroTerminated);
	xattrNameUTF8ZeroTerminated[xattrNameUTF8Length] = 0;

	// Write the data, or just delete the key?
	if(isNull)
	{
		// Remove the attribute from the file
		if(removexattr(pcontext->filename, xattrNameUTF8ZeroTerminated, 0) != 0
			&& errno != ENOATTR)
		{
			// Failed, but not because the attribute didn't exist
			APPLIER_RETURN_ERROR
		}
	}
	else
	{
		// Make UTF-8 encoded version of the data
		xattrDataUTF8 = CFStringCreateExternalRepresentation(kCFAllocatorDefault,
				xattrData, kCFStringEncodingUTF8, ' ');
		if(xattrDataUTF8 == NULL) APPLIER_RETURN_ERROR;
		
		// And write the data
		if(setxattr(pcontext->filename, xattrNameUTF8ZeroTerminated,
			(const char*)CFDataGetBytePtr(xattrDataUTF8), CFDataGetLength(xattrDataUTF8), 0, 0) != 0)
		{
			// Failed to apply
			APPLIER_RETURN_ERROR
		}
		
		// Add count of how many were added
		++(pcontext->numXAttrWritten);
	}
	
	// Clean up
cleanup:
	if(convertedKey != NULL)	CFRelease(convertedKey);
	if(xattrName != NULL)		CFRelease(xattrName);
	if(xattrNameUTF8 != NULL)	CFRelease(xattrNameUTF8);
	if(xattrDataBuild != NULL)	CFRelease(xattrDataBuild);
	if(xattrData != NULL)		CFRelease(xattrData);
	if(xattrDataUTF8 != NULL)	CFRelease(xattrDataUTF8);
}

CFStringRef SpotMetaXAttrWrite_GetStringRepForValue(const void *value, char expectedType)
{
	if((expectedType == 't' || expectedType == 'c') && CFGetTypeID(value) == CFStringGetTypeID())
	{
		// String / Choice. Just return it, after retaining the value
		CFRetain(value);
		return value;
	}
	if(expectedType == 'n' && CFGetTypeID(value) == CFNumberGetTypeID())
	{
		static CFNumberFormatterRef numberFormatter = NULL;
		if(numberFormatter == NULL)
		{
			numberFormatter = CFNumberFormatterCreate(kCFAllocatorDefault, CFLocaleCopyCurrent(), kCFNumberFormatterDecimalStyle);
			CFNumberFormatterSetFormat(numberFormatter, CFSTR(""));
		}
		if(numberFormatter == NULL) return NULL;	// deal with lack of formatter
		
		// Do conversion
		return CFNumberFormatterCreateStringWithNumber(kCFAllocatorDefault, numberFormatter, value);
	}
	if(expectedType == 'b' && CFGetTypeID(value) == CFBooleanGetTypeID())
	{
		return CFBooleanGetValue(value)?CFSTR("1"):CFSTR("0");
	}
	if(expectedType == 'd' && CFGetTypeID(value) == CFDateGetTypeID())
	{
		CFGregorianDate greg =
			CFAbsoluteTimeGetGregorianDate(CFDateGetAbsoluteTime(value), NULL /* GMT */);
	
		return CFStringCreateWithFormat(kCFAllocatorDefault, NULL, CFSTR("%04d%02d%02d%02d%02d%02d"),
			greg.year, greg.month, greg.day, greg.hour, greg.minute, (int)greg.second);
	}
	else
	{
		// Doesn't look good.
		return NULL;
	}
}

// ===============================================================================================
// 		Utility functions
// ===============================================================================================

static const char *base62Encode = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
static const unsigned char base62Decode[] = {255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 255, 255, 255, 255, 255, 255, 255, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 255, 255, 255, 255, 255, 255, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61};
static const char *commonCharsEncode = ".,:'\"!$%&(\n)-\r_=?@+;";	// note \n (10) and \r (13) in their correct positions
static const unsigned char commonCharsDecode[] = {255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 10, 255, 255, 13, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 5, 4, 255, 6, 7, 8, 3, 9, 11, 255, 18, 1, 12, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 2, 19, 255, 15, 255, 16, 17, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 14};
#define DECODE_THROUGH(x, value) ((value < sizeof(x))?x[value]:255)
#define DECODE_BASE62(value) DECODE_THROUGH(base62Decode, value)
#define DECODE_COMMON_CHARS(value) DECODE_THROUGH(commonCharsDecode, value)

CFStringRef SpotMetaEncodeStringForXAttrName(const CFStringRef String, int NumNoEncodeUnderscores)
{
	UniChar buffer[MAX_STRING_SIZE_FOR_CONVERSION];
	UniChar bufferOut[MAX_STRING_SIZE_FOR_CONVERSION];
	
	int stringLen = CFStringGetLength(String);
	if(stringLen > (MAX_STRING_SIZE_FOR_CONVERSION-1)) stringLen = (MAX_STRING_SIZE_FOR_CONVERSION-1);
	CFRange range = {0,stringLen};
	CFStringGetCharacters(String, range, buffer);
	
	// Copy, doing conversion along the way
	CFIndex length = CFStringGetLength(String);
	CFIndex inPos = 0;
	CFIndex outPos = 0;
	bool tooLong = false;
	#define CHECK_LENGTH_AVAILABLE(x) {if((outPos+(x))>=(MAX_STRING_SIZE_FOR_CONVERSION-1)) {tooLong = true; break;}}
	while(buffer[inPos] != '\0' && inPos < length)
	{
		UniChar c = buffer[inPos];
		if(c == '_' && NumNoEncodeUnderscores > 0)
		{
			// Don't encode the first so many of these
			CHECK_LENGTH_AVAILABLE(1)
			bufferOut[outPos++] = '_';
			NumNoEncodeUnderscores--;
		}
		else if((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9'))
		{
			// Normal char
			CHECK_LENGTH_AVAILABLE(1)
			bufferOut[outPos++] = c;
		}
		else if(c == ' ')
		{
			// Space, encode as __
			CHECK_LENGTH_AVAILABLE(2)
			bufferOut[outPos++] = '_';
			bufferOut[outPos++] = '_';
		}
		else
		{
			// Other unicode character, encode it in a mildly complex manner
			CHECK_LENGTH_AVAILABLE(4)

			// Map common punctuation chars to codes < 20
			int n = c;
			int sc = DECODE_COMMON_CHARS(c);
			if(sc != 255) n = sc;

			// Decode to a mix of base 20 and base 62
			int digits[4];
			digits[0] = n % 20;		n /= 20;
			digits[1] = n % 62;		n /= 62;
			digits[2] = n % 62;		n /= 62;
			if(n != 0)
			{
				// BAD THINGS HAPPENED -- with 16 bit unicode this should always be zero.
				return NULL;
			}
			int len = 3;
			if(digits[2] == 0)
			{
				--len;
				if(digits[1] == 0)
				{
					--len;
				}
			}
			digits[0] += (len-1)*20;

			bufferOut[outPos++] = '_';
			int l = 0;
			for(l = 0; l < len; ++l)
			{
				bufferOut[outPos++] = base62Encode[digits[l]];
			}
		}
		++inPos;
	}
	#undef CHECK_LENGTH_AVAILABLE
	
	// Too long?
	if(tooLong)
	{
		return NULL;
	}

	// terminate
	bufferOut[outPos] = '\0';

	// Return escaped string
	return CFStringCreateWithCharacters(kCFAllocatorDefault, bufferOut, outPos);
}

CFStringRef SpotMetaDecodeStringFromXAttrName(const CFStringRef String, int NumNoDecodeUnderscores)
{
	UniChar buffer[MAX_STRING_SIZE_FOR_CONVERSION];	// even if everything is expanded, it'll be fine, because it can only get smaller
	UniChar bufferOut[MAX_STRING_SIZE_FOR_CONVERSION];
	
	CFRange range = {0,(MAX_STRING_SIZE_FOR_CONVERSION-2)};
	CFStringGetCharacters(String, range, buffer);

	// Copy, doing conversion along the way
	CFIndex length = CFStringGetLength(String);
	CFIndex inPos = 0;
	CFIndex outPos = 0;
	while(buffer[inPos] != '\0' && inPos < length)
	{
		UniChar c = buffer[inPos];
		if(c == '_')
		{
			if(NumNoDecodeUnderscores > 0)
			{
				--NumNoDecodeUnderscores;
				bufferOut[outPos++] = '_';
			}
			else if((length - inPos) >= 2)
			{
				if(buffer[inPos+1] == '_')
				{
					// Space
					bufferOut[outPos++] = ' ';
					++inPos;
				}
				else
				{
					// The beginnings of one of our horrid encodings.
					int digits[4];
					++inPos;
					digits[0] = DECODE_BASE62(buffer[inPos]);
					if(digits[0] != 255)
					{
						// Looks valid to start with. How many more characters are there?
						int more = digits[0] / 20;
						digits[0] -= (20*more);
						if(more <= 2)
						{
							int l = 0;
							for(l = 0; l < more; ++l)
							{
								if((length - inPos) >= 1)
								{
									++inPos;
									digits[l+1] = DECODE_BASE62(buffer[inPos]);
								}
								else
								{
									return NULL;
								}
								if(digits[l+1] == 255)
								{
									return NULL;
								}
							}
						}
						
						// And decode
						int c = digits[0];
						if(more >= 1)
						{
							c += digits[1] * 20;
						}
						if(more >= 2)
						{
							c += digits[2] * (20*62);
						}
						// check for small char codes
						if(c < 20)
						{
							c = commonCharsEncode[c];
						}
						
						// Store
						bufferOut[outPos++] = (UniChar)c;
					}
				}
			}
		}
		else
		{
			// Normal char, just copy
			bufferOut[outPos++] = c;
		}
		inPos++;
	}

	// terminate
	bufferOut[outPos] = '\0';

	return CFStringCreateWithCharacters(kCFAllocatorDefault, bufferOut, outPos);
}


