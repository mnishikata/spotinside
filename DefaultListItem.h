//
//  ListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ListItem.h"

@interface DefaultListItem : ListItem {

}

- (id) init ;
- (id)initWithCoder:(NSCoder *)coder;
-(NSString*)kind ;
-(NSString* )setQuery:(id)value;
-(NSString*)query ;
-(void)setName:(NSString*)aName;
-(NSImage*)icon;
-(void)search;
-(NSArray*)scope;
-(void)searchWithAdditionalQuery:(NSString*)query forArrayController:(NSArrayController*)anArrayController;
-(void)finishForAdditionalQuery:(id)result;
-(NSString*)queryTemplate ;


@end
