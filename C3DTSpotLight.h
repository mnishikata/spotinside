//
//  C3DTSpotLight.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTSpotLight.h,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTSpotLight.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//


#import "C3DTLight.h"
#import "C3DTMath.h"

@interface C3DTSpotLight : C3DTLight {
    _C3DTVector	_spotDirection;
    float		_spotExponent;
    float		_spotCutoff;
}

+ (id)spotLight;

- (_C3DTVector)spotDirection;
- (void)setSpotDirection: (_C3DTVector)aDirection;
- (void)setSpotDirectionX: (float)x Y: (float)y Z: (float)z;

- (float)spotExponent;
- (void)setSpotExponent: (float)aValue;

- (float)spotCutoff;
- (void)setSpotCutoff: (float)aValue;


@end
