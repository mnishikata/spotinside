//
//  main.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/06/24.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **) argv);
}
