//
//  NSFileManagerExt.m
//  Jikeiretsu
//
//  Created by Masatoshi Nishikata on 07/03/05.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "NSFileManagerExt.h"


@implementation NSFileManager (extension) 

-(unsigned long long)fileSizeAtPath:(NSString*)path
{
	BOOL isDirectory;
	if( ! [self fileExistsAtPath:path isDirectory:&isDirectory] )
		return 0;
	
	if( ! isDirectory )
	{
		NSDictionary* fileAttributes = [self fileAttributesAtPath:path traverseLink:YES];
		return [[fileAttributes  objectForKey: NSFileSize] unsignedLongLongValue];
	}
	
	//
	unsigned long long fileSize = 0;
	int hoge;
	NSArray* subpaths = [self subpathsAtPath: path];
	for( hoge = 0; hoge < [subpaths count]; hoge++ )
	{
		NSDictionary* fileAttributes = [self fileAttributesAtPath:[path stringByAppendingPathComponent:  [subpaths objectAtIndex:hoge]] traverseLink:YES];
		fileSize += [[fileAttributes  objectForKey: NSFileSize] unsignedLongLongValue];
	}
	
	return fileSize;
}

+(BOOL)createDirectoryIfNotExists:(NSString*)path
{	
	BOOL isDirectory;
	BOOL flag;
	NSFileManager* fm = [NSFileManager defaultManager];
	
	flag = [fm fileExistsAtPath:path isDirectory:&isDirectory];
	
	if( flag == YES &&  isDirectory == NO ) return NO;
	
	flag = [fm createDirectoryAtPath:path attributes:nil];
	
	return flag;
}
+(BOOL)fileExistsAtPath:(NSString*)path
{
	NSFileManager* fm = [NSFileManager defaultManager];
	return [fm fileExistsAtPath:path];
}

@end
