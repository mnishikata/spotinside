//
//  SearchResultTableIconCell.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/29.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SearchResultTableIconCell.h"
#import "ThreadWorker.h"
//#import "ColorCheckbox.h"

@implementation SearchResultTableIconCell


- (id) init {
	self = [super init];
	if (self != nil) {
		
		[self setType:NSImageCellType  ];
		[self setImage:nil];
		
		
		iconCache = [[NSMutableDictionary alloc] init];
	}
	return self;
}
- (void) dealloc {
	

	[super dealloc];
}


-(void)setOwnerTableView:(NSTableView*)view andCacheDictionary:(NSMutableDictionary*)dict
{
	iconCache = dict;
	ownerTableView = view;
	
}


- (void)drawInteriorWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	/*
	NSString* path = [self title];
	
	
	NSImage* icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
	[icon setSize:NSMakeSize(16,16)];
	cellFrame.origin.y +=16;

	[icon compositeToPoint:cellFrame.origin operation:NSCompositeSourceOver];

	*/
	

	
	
	
	
	NSString* path = [self title];
	
	id cachedIcon;
	@synchronized(iconCache){
		cachedIcon  =[iconCache objectForKey:path ];
	}
	
	if( cachedIcon == nil )
	{

		NSDictionary* fetchDic = [NSDictionary dictionaryWithObjectsAndKeys: path,@"path",NSStringFromRect(cellFrame),@"cellFrame",
			[NSView focusView],@"focusView",
			nil];

		/*
		[ThreadWorker 
        workOn:self 
  withSelector:@selector(fetchIcon:) 
    withObject:fetchDic
didEndSelector:@selector(finishFetchIcon:) ];
		*/
		[NSThread detachNewThreadSelector:@selector(fetchIcon:) toTarget:self withObject:fetchDic];
		
		cellFrame.origin.y +=16;
		
		[[NSImage imageNamed:@"blankIcon"] compositeToPoint:cellFrame.origin operation:NSCompositeSourceOver];

		
		
		return;

	}
	
	cellFrame.origin.y +=16;

	[cachedIcon compositeToPoint:cellFrame.origin operation:NSCompositeSourceOver];

}

-(id)fetchIcon:(NSMutableDictionary*)dictionary
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	NSString* path = [dictionary objectForKey:@"path"];

	
	
	/// check if it is edgy file
	
	if( 0/* [[[path pathExtension] lowercaseString] isEqualToString:@"edgy"] */)
	{
		/*
		NSData* readDocData =[[[NSData alloc] initWithContentsOfFile:path] autorelease];
		
		if( readDocData != nil )
		{

			id dic = [NSKeyedUnarchiver unarchiveObjectWithData:readDocData];
			NSColor* tabColor = [dic objectForKey:@"color"];
			
			NSImage* icon = [ColorCheckbox roundedBox:tabColor
											  size:NSMakeSize(12,12)
											 curve:4 
										frameColor:tabColor];
			
			[iconCache setObject:icon forKey:path];

		}
		*/
		
		
	}else
	{
	
	
		// no
		NSImage* icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
		[icon setSize:NSMakeSize(16,16)];
		
		@synchronized(iconCache){
			[iconCache setObject:icon forKey:path];
		}
	}

	////NSLog(@"%d",[iconCache count]);
	[self performSelectorOnMainThread:@selector(finishFetchIcon:) withObject:dictionary waitUntilDone:NO];
	
	[pool release];
	//return dictionary;
}



-(void)finishFetchIcon:(NSMutableDictionary*)dictionary
{
	
	//	NSString* path = [dictionary objectForKey:@"path"];
	NSRect cellFrame  = NSRectFromString( [dictionary objectForKey:@"cellFrame"] );
	//	NSView* controlView = [dictionary objectForKey:@"controlView"];
	NSView* focusView = [dictionary objectForKey:@"focusView"];
	
	////NSLog(@"fetch");
	
	
	if( ownerTableView != nil && iconCache != nil )
	{
		NSScrollView* sv = [[ownerTableView superview] superview];
		NSRect ownerRect = [sv documentVisibleRect];
		if( NSIntersectsRect(ownerRect , cellFrame) )
				[focusView setNeedsDisplayInRect: cellFrame];
	
	}
	
}

@end
