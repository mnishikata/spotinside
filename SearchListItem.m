//
//  ListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SearchListItem.h"

@implementation SearchListItem



- (id) init {
	self = [super init];
	if (self != nil) {
		
		queryTemplate= nil;
		canDelete = YES;
		canMove = YES;
		canDrop = NO;
		canSelect = YES;
		
		icon = nil;
	}
	return self;
}


- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		queryTemplate = nil;
		canDelete = YES;
		canMove = YES;
		canDrop = NO;
		canSelect = YES;
		
		
	}
	return self;
	
	
}




-(NSString*)kind {return @"search";}




-(NSImage*)icon
{
	if( icon == nil )
		icon = [[NSImage imageNamed:@"find.tiff"] retain];
	
	return [super icon];
	
}
/*
-(void)search
{
	if( isRenaming ) return;

	
	[contents release];
	contents = nil;
	
	isSearching = YES;
	
	//NSLog(@"[self query] %@",[self query]);
	
	if( [self query] == nil || [[self query] isEqualToString:@""]) //do nothing
	{
		[self finish:nil];
		return;
	}
	
	[super search];
}*/


/*
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{
	
	//NSLog(@"query %@",query);
	
	requestedArrayController = anArrayController;
	endTarget = [aTarget retain];
	endSelectr = aSelector;

	isSearching = YES;
	[searchAgent search:@"" 
				 format:query
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishForAdditionalQuery:)];
	
}
*/


-(NSString*)queryTemplate {
	
	
	return queryTemplate;
}

-(void)setQueryTemplate:(NSString*)string
{
	[queryTemplate release];
	queryTemplate = [string retain];
}


- (void)contextualMenu:(NSMenu*)menu{
	NSMenuItem* customMenuItem;
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Edit...",@"")
												action:@selector(edit) keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"MN"];
	[customMenuItem setTarget:self];
	[customMenuItem autorelease];	
	
	[menu addItem:customMenuItem];
	
	[super contextualMenu:menu];
	
}

-(void)edit
{
	if( window == nil )
	{
		[NSBundle loadNibNamed:@"SearchListItem" owner:self];
	}

	if( [self scope] != nil )
		[scopeField setObjectValue: [self scope] ];
	
	if( [self query] != nil )
		[queryField setStringValue: [self query] ];
	
	if( [self queryTemplate] != nil )
		[queryTemplateField setStringValue: [self queryTemplate] ];

	[NSApp beginSheet:window modalForWindow:[NSApp mainWindow]
		modalDelegate:self
	   didEndSelector:nil contextInfo:nil];
}

-(IBAction)buttonClicked:(id)sender
{
	if ([window makeFirstResponder:window]) {
		/* All fields are now valid; it’s safe to use fieldEditor:forObject:
        to claim the field editor. */
	}
	else {
		/* Force first responder to resign. */
		[window endEditingFor:nil];
	}
	
	
	
	int tag = [sender tag];
	
	if( tag == 1 ) //add
	{
		NSOpenPanel * op = [NSOpenPanel openPanel];
		
		[op setCanChooseFiles:NO];
		[op setCanChooseDirectories:YES];
		int result = [op runModal];
		if( result == NSOKButton )
		{
			NSMutableArray* newScope  = [[[NSMutableArray alloc] initWithArray:[self scope]] autorelease];
			[newScope addObject:[op filename] ];
			[self setScope:newScope];
			[scopeField setObjectValue: [self scope] ];

		}
		return;
		
	}else if( tag == 0 ) //ok
	{
	
		[self setScope: [scopeField objectValue]];
		
		if( [[queryField stringValue] isEqualToString:@""] )
			[self setQuery:nil];
		else
			[self setQuery: [queryField stringValue]];
		
		if( [[queryTemplateField stringValue] isEqualToString:@""] )
			[self setQueryTemplate:nil];
		else
			[self setQueryTemplate: [queryTemplateField stringValue]];

		
		[[NSNotificationCenter defaultCenter] postNotificationName:@"SourceReselectRequestNotification"
															object:nil];	

	}
	
	[NSApp endSheet:window];
	[window orderOut:self];
	
}

@end
