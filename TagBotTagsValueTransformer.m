//
//  TagBotTagsValueTransformer.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "TagBotTagsValueTransformer.h"
#import <OgreKit/OgreKit.h>


@implementation TagBotTagsValueTransformer



+ (Class)transformedValueClass {
    return [NSString class];
}
+ (BOOL)allowsReverseTransformation { return NO; }


- (id)transformedValue:(id)value {

    if (value == nil || ![value isKindOfClass:[NSString class]]) {
        return @"";
    }
	
	OGRegularExpression *regex = [OGRegularExpression regularExpressionWithString:@"(?<=&)\\w+"];
	OGRegularExpressionMatch *match;
	NSArray *matches = [regex allMatchesInString:value];
	NSMutableArray* array = [NSMutableArray array];

	int hoge;
	for (hoge=0; hoge<[matches count]; hoge++) {
		match = [matches objectAtIndex:hoge];
		[array addObject:  [match matchedString]];
	}

	if( [array count] == 0 ) return @"";
	
	
	
	return [array componentsJoinedByString:@", "];
}

@end
