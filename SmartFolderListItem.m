//
//  FolderListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SmartFolderListItem.h"
#import "MyDocument.h"

@implementation SmartFolderListItem





-(NSString*)kind {return @"savedSearch";}

/*
-(void)search
{
	if( isRenaming ) return;

	[contents release];
	contents = nil;
	
	isSearching = YES;
	[searchAgent search:@"" 
				 format:[self query]
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finish:)];
}*/

-(NSString*)query {
	
	NSString* path = [self path];


	if( path != nil )
	{
		
		NSString* xmlstr = [[[NSString alloc] initWithContentsOfFile:path
															encoding:NSUTF8StringEncoding
															   error:nil] autorelease];
		
		NSDictionary* xmldict = [xmlstr propertyList];
		
		
		
		if( xmldict != nil )
		{
			NSString* query = [xmldict objectForKey: @"RawQuery"];

			if( query != nil ) return query;
		}
		
	}

	return @"";
}


-(NSArray*)scope
{
	NSString* path = [self path];
	
	
	if( path != nil )
	{
		
		NSString* xmlstr = [[[NSString alloc] initWithContentsOfFile:path
															encoding:NSUTF8StringEncoding
															   error:nil] autorelease];
		
		NSDictionary* xmldict = [xmlstr propertyList];
		
		
		
		if( xmldict != nil )
		{
			NSArray* scopeArray = [[xmldict objectForKey: @"SearchCriteria"] objectForKey:@"FXScopeArrayOfPaths"];
			
			if( scopeArray != nil ) return scopeArray;
		}
		
	}
	
	return [NSArray array];
}

-(NSArray*)contents
{
	return contents;	
}


-(void)setName:(NSString*)aName
{
	isRenaming = NO;

	
	if( ![aName isEqualToString:name] )
	{
		[UNDO_MANAGER registerUndoWithTarget:self 
									selector:@selector(undoRename:) object:[self name]];
		[UNDO_MANAGER setActionName: NSLocalizedString(@"Rename",@"")];
	}

	
	[name release];
	name = [[aName stringByDeletingPathExtension] retain];
	
	[self postNotification];
}

-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{
	requestedArrayController = [arrayController retain];
	
	
	if( isRenaming ) {
		
		[self finishWithEndTarget:nil];
		return;
	}
	
	[contents release];
	contents = nil;
	
	endTarget = [aTarget retain];
	endSelector = aSelector;
	
	
	isSearching = YES;
	[searchAgent search:@"" 
				 format:[self query]
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishWithEndTarget:)];
}


-(NSString*)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{
	NSString* string = [NSString stringWithFormat:@"(%@)&&(%@)",[self query],aQuery];

	[super searchWithAdditionalQuery:string forArrayController:anArrayController 
						   endTarget:aTarget andSelector:aSelector];
	
	return string;
}

@end
