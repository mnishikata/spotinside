//
//  SearchResultTableSizeValueTransformer.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/20.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SearchResultTableSizeValueTransformer.h"


@implementation SearchResultTableSizeValueTransformer

+ (Class)transformedValueClass {
    return [NSString class];
}

- (id)transformedValue:(id)value {
    if (value == nil) {
        return NSLocalizedString(@"--", @"File size shown for 0 byte files");
    }
    if (![value respondsToSelector:@selector(intValue)]) {
        [NSException raise:NSInternalInconsistencyException format:@"Value %@ does not respond to intValue", [value class]];
    }
    int fsSize = [value intValue];
    // special case for small files
    if (fsSize == 0) {
        return NSLocalizedString(@"0 KB", @"File size shown for 0 byte files");
    }
    
    const int cutOff = 900;
    
    if (fsSize < cutOff) {
        return [NSString stringWithFormat:NSLocalizedString(@"%d Bytes", @"File size shown formatted as bytes"), fsSize];
    }
    
    double numK = (double)fsSize / 1024;
    if (numK < cutOff) {
        return [NSString stringWithFormat:NSLocalizedString(@"%.2f KB", @"File size shown formatted as kilobytes"), numK];
    }
    
    double numMB = numK / 1024;
    if (numMB < cutOff) {
        return [NSString stringWithFormat:NSLocalizedString(@"%.2f MB", @"File size shown formatted as megabytes"), numMB];
    }
    
    double numGB = numMB / 1024;
    return [NSString stringWithFormat:NSLocalizedString(@"%.2f GB", @"File size shown formatted as gigabytes"), numGB];
}

@end
