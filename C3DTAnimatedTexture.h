//
//  C3DTAnimatedTexture.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Fri May 30 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTAnimatedTexture.h,v 1.6 2003/07/08 11:19:46 pmanna Exp $
//
// $Log: C3DTAnimatedTexture.h,v $
// Revision 1.6  2003/07/08 11:19:46  pmanna
// Modified to use a more generic generateTexture
//
// Revision 1.5  2003/06/22 09:39:36  pmanna
// Refactoring of texture objects to consolidate common tasks
//
// Revision 1.4  2003/06/19 20:33:40  pmanna
// Modified init and class method names
//
// Revision 1.3  2003/06/15 12:46:32  pmanna
// Added alpha channel and color keying
//
// Revision 1.2  2003/06/11 21:29:36  pmanna
// Corrected updateTexture to reverse mapping
//
// Revision 1.1.1.1  2003/06/10 18:09:22  pmanna
// Initial import
//
//

#import <Cocoa/Cocoa.h>
#import <QuickTime/QuickTime.h>
#import <OpenGL/glu.h>
#import "C3DTTexture.h"
#import	"C3DTMath.h"


@interface C3DTAnimatedTexture : C3DTTexture {
    NSTimeInterval	_animateInterval;
    BOOL			_useAlpha;

    GLuint			_alphaMask;
    short			movieRefNum;		// Reference to movie file
    BOOL			moviePaused;		// Is our movie paused?
    Movie			theMovie;			// The QT movie
    Rect			movieBox;			// Size of movie
    GWorldPtr		offscreenGWorld;	// Offscreen buffer for QT playing
    unsigned char	*gWorldMem;			// Memory used for offscreen buffer
    PixMapHandle	gWorldPixMap;		// PixMap for offscreen buffer
    Ptr				gWorldPixBase;		// Actual pointer to buffer
    
    NSTimer			*_animateTimer;
}

+ (C3DTAnimatedTexture *)textureWithMovie: (NSString *)aName interval: (NSTimeInterval)anInterval alpha: (BOOL)alphaFlag;

- (id)initWithMovie: (NSString*)aName interval: (NSTimeInterval)anInterval alpha: (BOOL)alphaFlag;

- (NSTimeInterval)animateInterval;
- (void)setAnimateInterval: (NSTimeInterval)anInterval;

- (BOOL)useAlpha;
- (void)setUseAlpha: (BOOL)newUseAlpha;

- (void)start;
- (void)stop;

- (void)doAnimate: (NSTimer *)timer;

@end
