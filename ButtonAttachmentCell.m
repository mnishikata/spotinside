//
//  ButtonAttachmentCell.m
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 07/05/21.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ButtonAttachmentCell.h"

//MACRO

#define TIMEOUT_WHILE(n) NSDate* __timeOutDate = [NSDate dateWithTimeIntervalSinceNow: n ];	while ( [__timeOutDate compare:[NSDate date]] == NSOrderedDescending )


@implementation ButtonAttachmentCell (Protocol)
- (NSPoint)cellBaselineOffset
{
	return NSMakePoint(0,0);
}
- (void)setAttachment:(NSTextAttachment *)anAttachment
{
	attachment = anAttachment; // is not retained
	[anAttachment setAttachmentCell:self];
	
	
	NSFileWrapper* wrapper = [anAttachment fileWrapper];
	[wrapper setFilename:@"file.tiff"];
	[wrapper setPreferredFilename:[wrapper filename]];//self rename
		
}

- (NSTextAttachment *)attachment
{
	return attachment;
}


- (BOOL)wantsToTrackMouse
{
	return YES;	
}




@end
@implementation ButtonAttachmentCell



- (id)initWithAttachment:(NSTextAttachment*)anAtachment
{
    self = [super init];
    if (self) {
		
		textView = nil;
		toolTipTag = 0;
		
		
		[self loadDefaults];
		
		
		[self setAttachment:anAtachment];
		[self setAction:@selector(action:)];
		[self setTarget:self];
/*
		myButton = [[NSButton alloc] init];
		[myButton setTitle:@""];
		[myButton setBordered:YES];
		[myButton setFrame:NSMakeRect(0,0,BUTTON_SIZE,BUTTON_SIZE)];
*/
		[self setButtonType:NSMomentaryPushButton ];
		[self setBezelStyle:NSRegularSquareBezelStyle];
		[self setBordered:showBezel];

	//	[myButton setCell:self];
 
		

		[self setTitle: @"ICON"];
		[self setImagePosition:NSImageOnly  ];

		
		[self setObserveKeyPaths];
		
		
		//[self setShowsBorderOnlyWhileMouseInside:YES];
	}
    return self;
}

-(void)setObserveKeyPaths
{
	[self addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"font" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"buttonPosition" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"buttonSize" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"showButtonTitle" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"showBezel" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"image" options:NSKeyValueObservingOptionNew context:nil];
	[self addObserver:self forKeyPath:@"lock" options:NSKeyValueObservingOptionNew context:nil];

	
}

-(void)removeObserveKeyPaths
{
	[self removeObserver:self forKeyPath:@"title" ];
	[self removeObserver:self forKeyPath:@"font" ];
	[self removeObserver:self forKeyPath:@"buttonPosition" ];
	[self removeObserver:self forKeyPath:@"buttonSize" ];
	[self removeObserver:self forKeyPath:@"showButtonTitle" ];
	[self removeObserver:self forKeyPath:@"showBezel" ];
	[self removeObserver:self forKeyPath:@"image" ];
	[self removeObserver:self forKeyPath:@"lock" ];
	
	
}

- (void) dealloc {
	
	[self removeObserveKeyPaths];
	
	[super dealloc];
}




-(void)loadDefaults
{
	id value;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	value = [ud valueForKey: @"ButtonAttachmentCell_buttonSize"];
	buttonSize = (value != nil ? [value floatValue] : 48); 
	
	value = [ud valueForKey: @"ButtonAttachmentCell_buttonPosition"];
	buttonPosition = (value != nil ? [value intValue] : 0); 

	value = [ud valueForKey: @"ButtonAttachmentCell_showButtonTitle"];
	showButtonTitle = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"ButtonAttachmentCell_showBezel"];
	showBezel = (value != nil ? [value boolValue] : YES); 
	
	value = [ud valueForKey: @"ButtonAttachmentCell_font"];
	if( value != nil ){
		NSFont* font = [NSUnarchiver unarchiveObjectWithData: value];
		if( font != nil && [font isKindOfClass:[NSFont class]] )
			[self setFont:font];
	}
	
	lock = NO;
}





- (void)encodeWithCoder:(NSCoder *)encoder
{
	//NSLog(@"encodeWithCoder");
	[super encodeWithCoder: encoder];
	
   // [encoder encodeObject: myButton forKey: @"button"];
	[encoder encodeFloat: buttonSize forKey: @"buttonSize"];
	[encoder encodeInt: buttonPosition forKey: @"buttonPosition"];
	[encoder encodeBool: showButtonTitle forKey: @"showButtonTitle"];
	[encoder encodeBool: showBezel forKey: @"showBezel"];

	[encoder encodeObject: [self representedObject] forKey: @"representedObject"];
	[encoder encodeObject:attachment forKey:@"attachment"];
	
	[encoder encodeBool: lock forKey: @"lock"];

    return;
}

- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [super initWithCoder:(NSCoder *)coder];
	if (self != nil) {
		
		
		textView = nil;
		toolTipTag = 0;
		[self loadDefaults];

		if( [coder containsValueForKey:@"buttonSize"] )
			buttonSize	=   [coder decodeFloatForKey: @"buttonSize"];
	
		if( [coder containsValueForKey:@"buttonPosition"] )
			buttonPosition	=   [coder decodeIntForKey: @"buttonPosition"];
			
		if( [coder containsValueForKey:@"showButtonTitle"] )
			showButtonTitle =  [coder decodeBoolForKey: @"showButtonTitle"];
	
		if( [coder containsValueForKey:@"showBezel"] )
			showBezel =  [coder decodeBoolForKey: @"showBezel"];

		
		if( [coder containsValueForKey:@"representedObject"] )
			[self setRepresentedObject: [coder decodeObjectForKey: @"representedObject"]];
			
		if( [coder containsValueForKey:@"attachment"] )
			[self setAttachment: [coder decodeObjectForKey: @"attachment"]];
			
		if( [coder containsValueForKey:@"lock"] )
			lock =  [coder decodeBoolForKey: @"lock"];
		
		
		
		[self setAction:@selector(action:)];
		[self setTarget:self];
		
		[self setButtonType:NSMomentaryPushButton ];
		
		[self setBordered:showBezel];
		[self setBezelStyle:NSRegularSquareBezelStyle];


		[self setImagePosition:NSImageOnly  ];
		
		
		[self setObserveKeyPaths];

	}
	return self;
	
	
}



- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aTextView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)untilMouseUp

{
	if( [aTextView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
		textView =  aTextView ;
	
	
	
	
	
	
	
	BOOL result = NO;
	//NSLog(@"trackMouse:inRect:ofView:untilMouseUp:");
	NSDate *endDate;
	NSPoint currentPoint = [theEvent locationInWindow];
	BOOL done = NO;
	BOOL clickOnTitle = NO;
	
	// Click on title
	if( ! NSPointInRect([aTextView convertPoint:currentPoint fromView:nil], [self buttonFrame:cellFrame]) )
	{
		[textView setSelectedRange:NSMakeRange(charIndex,1)];
		
		clickOnTitle = YES;
		
		if( [theEvent clickCount] >= 2 )
		{
			
			[self activateInspectorForTextView:textView];
			return NO;
		}
		
	}
	
	
	
	if( !clickOnTitle )
	{
		
		[self setHighlighted: YES];
		[aTextView display];
		//[[SoundManager sharedSoundManager] playSoundWithName:@"soundClickButton"];

	}
				
	
	
	
	BOOL trackContinously = [self startTrackingAt:currentPoint inView:aTextView];
	// catch next mouse-dragged or mouse-up event until timeout
	BOOL mouseIsUp = NO;
	BOOL mouseIsDragged = NO;
	
	NSEvent *event;
	while (!done) { // loop ...
		
		
		
		
		NSPoint lastPoint = currentPoint;
		endDate = [NSDate distantFuture];
		
		event = [NSApp nextEventMatchingMask:(NSLeftMouseUpMask|NSLeftMouseDraggedMask) untilDate:endDate inMode:NSEventTrackingRunLoopMode dequeue:YES];
		
		if (event) { // mouse event
			
			currentPoint = [event locationInWindow];
			if (trackContinously) { // send continueTracking.../stopTracking...
				if (![self continueTracking:lastPoint at:currentPoint inView:aTextView]) {
					done = YES;
					[self stopTracking:lastPoint at:currentPoint inView:aTextView mouseIsUp:mouseIsUp];
				}
				
			}
			
			
			mouseIsUp = ([event type] == NSLeftMouseUp);
			mouseIsDragged = ([event type] == NSLeftMouseDragged );
			
			done = done || mouseIsUp || mouseIsDragged;
			if (untilMouseUp) {
				result = mouseIsUp;
			} else {
				
			}
			
			// check, if the mouse left our cell rect
			result = NSPointInRect([aTextView convertPoint:currentPoint fromView:nil], cellFrame);
			if (!result) {
				done = YES;
				//NSLog(@"mouse is up outside rect");
			}
			
			
			if (done && result && !mouseIsDragged && !clickOnTitle ) {
				[NSApp sendAction:[self action] to:[self target] from:self];
			}
		} 

	} // ... while (!done)
	
	if( mouseIsDragged )
	{

		[self setHighlighted: NO];

		if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
		{
			[textView buttonAttachmentCellDragged:self atCharacterIndex:charIndex];
		}

			
		
		[aTextView display];
		return result;
	}
	
	[self setHighlighted: NO];
	//[[SoundManager sharedSoundManager] playSoundWithName:@"soundReleaseButton"];

	[aTextView display];
	
	return result;
}


#pragma mark -

-(void)setButtonSize:(float)size
{
	buttonSize = size;

	NSRect bounds = NSZeroRect;
	bounds.size = [self cellSize];
	
	NSRect imageRect = [self imageRectForBounds:bounds ];
	[[self image] setSize: imageRect.size];
	
}

/*
//-(float)buttonSize
{
	return buttonSize;
}
//-(int)buttonPosition
{
	return buttonPosition;
}

//-(void)setButtonPosition:(int)position
{
	 buttonPosition = position;
}
//-(BOOL)showButtonTitle
{
	return showButtonTitle;
}
//-(void)setShowButtonTitle:(BOOL)flag
{
	showButtonTitle = flag;
}
*/

#pragma mark Implemented in Subclass

-(NSString*)targetPath
{
		return @""; // should be implemented in sublass
}


-(BOOL)textView:(NSTextView  *)aTextView shouldChangeTextInRange:(NSRange)affectedCharRange  replacementString:(NSString *)replacementString
{
	/*
	[[self attachment] setFileWrapper:nil]; ここを有効にすると、undoができなくなってしまう
	[[self attachment] setAttachmentCell:nil];
	 */
	textView = nil;
	return YES;
}

-(BOOL)canAcceptDrop
{
	return NO;
}

-(void)activateInspectorForTextView:(NSTextView  *)aTextView
{
	//override	
}

-(void)useAsDefault
{
	//NSLog(@"useAsDefault"); 
	/*
	 float buttonSize;
	 int buttonPosition;
	 BOOL showButtonTitle;
	 NSFont* [self font];
	 
	 */
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud setFloat:buttonSize forKey:@"ButtonAttachmentCell_buttonSize"];
	[ud setInteger:buttonPosition forKey:@"ButtonAttachmentCell_buttonPosition"];
	[ud setBool:showButtonTitle forKey:@"ButtonAttachmentCell_showButtonTitle"];
	[ud setBool:showBezel forKey:@"ButtonAttachmentCell_showBezel"];
	[ud setObject:[NSArchiver archivedDataWithRootObject: [self font]] forKey:@"ButtonAttachmentCell_font"];
	
	[ud synchronize];
}


#pragma mark  -
#pragma mark  Accessor
-(NSTextView*)textView
{
	return textView;	
}
-(NSImage*)buttonImageWithAlpha:(float)alpha
{
	NSSize cellSize = [self cellSize];
	NSImage* image = [[[NSImage alloc] initWithSize: cellSize] autorelease];
	
	[image setFlipped:YES];
	[image lockFocus];
	[self  drawWithFrame:NSMakeRect(0,0,cellSize.width,cellSize.height)
				  inView:(NSView *)textView
		  characterIndex: 0
		   layoutManager:nil];
	
	[image unlockFocus];
	
	
	
	NSImage* dragImage = [[[NSImage alloc] initWithSize: cellSize] autorelease];
	[dragImage lockFocus];
	[image drawAtPoint:NSZeroPoint fromRect:NSMakeRect(0,0,cellSize.width, cellSize.height)
			 operation:NSCompositeSourceOver fraction:alpha];
	[dragImage unlockFocus];	
	
	return dragImage;
}
-(NSData*)imageData
{
	return [[self image] TIFFRepresentation];
}
- (NSSize)cellSize
{
	if( !showButtonTitle ) return NSMakeSize(buttonSize,buttonSize);
	
	
	NSSize supersize = NSMakeSize(buttonSize,buttonSize);
	NSSize titleSize = [self titleSize];

	if( buttonPosition == 1 )
	{
		supersize.height += titleSize.height;
		if( supersize.width < titleSize.width ) supersize.width = titleSize.width;
		return supersize;
	}
	
	supersize.width += titleSize.width;
	return supersize;
}
-(NSSize)titleSize
{	
	return [[self title] sizeWithAttributes: [self titleAttribute]];	
}

-(NSRect)buttonFrame:(NSRect)cellFrame
{
	return [self iconFrame:cellFrame];
}

-(NSRect)iconFrame:(NSRect)cellFrame
{
	if( !showButtonTitle ) return cellFrame;
	
	
//	NSSize titleSize = [self titleSize];
	
	if( buttonPosition == 1 )//sita
	return NSMakeRect(cellFrame.origin.x + (cellFrame.size.width-buttonSize)/2, cellFrame.origin.y,buttonSize,buttonSize);
	
	return NSMakeRect(cellFrame.origin.x , cellFrame.origin.y ,buttonSize,buttonSize);

	
}
-(NSRect)titleFrame:(NSRect)cellFrame
{
	NSSize titleSize = [self titleSize];
	
	if( buttonPosition == 1 )//sita
	{
		if( titleSize.width > cellFrame.size.width ) 
			return NSMakeRect(cellFrame.origin.x,cellFrame.origin.y+buttonSize,titleSize.width,titleSize.height);
		
		return NSMakeRect(cellFrame.origin.x + (cellFrame.size.width-titleSize.width)/2,cellFrame.origin.y+buttonSize,titleSize.width,titleSize.height);
	}
	
	return NSMakeRect(cellFrame.origin.x + buttonSize, cellFrame.origin.y + (buttonSize -titleSize.height)/2 , titleSize.width, titleSize.height );
}

-(NSDictionary*)titleAttribute
{
	NSFont* font = [self font];
	return [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
}


#pragma makr -
#pragma mark Cell drawing


- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(unsigned)charIndex layoutManager:(NSLayoutManager *)layoutManager
{
	if( [controlView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
		textView = controlView;

	
	if( toolTipTag != 0 )
	[textView removeToolTip:toolTipTag];
	toolTipTag = [textView addToolTipRect:cellFrame owner:[self title] userData:nil];
	
	
	[self drawWithFrame:cellFrame inView:controlView];
	
	if( showButtonTitle )
		[[self title] drawAtPoint:[self titleFrame:cellFrame].origin  withAttributes:[self titleAttribute]];	

	
}



- (NSRect)drawTitle:(NSAttributedString*)title withFrame:(NSRect)frame inView:(NSView*)controlView
{
	// does not draw title 
	
	return NSZeroRect;
}

- (void)drawImage:(NSImage*)image withFrame:(NSRect)frame inView:(NSView*)controlView
{
	[image setFlipped:YES];
	
	[image drawInRect:frame 
			 fromRect:NSMakeRect(0,0,[image size].width,[image size].height) operation:NSCompositeSourceOver fraction:1.0];

}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	/// Draw icon
	[super drawWithFrame:[self iconFrame: cellFrame] inView:(NSView *)controlView];
}

/*
//- (BOOL)showsBorderOnlyWhileMouseInside
{
	return YES;
}

//- (void)mouseEntered:(NSEvent *)event
{
	//NSLog(@"mouseEntered");
}*/

#pragma mark -

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{

	//NSLog(@"keyPath %@",keyPath);
	
	
	
	if( [keyPath isEqualToString:@"buttonSize"] )
	{
		[[self image] setSize:NSMakeSize(buttonSize,buttonSize)];	
	}
	
	
	if( [keyPath isEqualToString:@"showBezel"] )
	{
		[self setBordered:showBezel];
	}
		
	if( textView != nil )
	{
		if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
			[textView buttonAttachmentCellUpdated:self ];

			//redraw here NSTextAttachmentCell
		
		
	}

	
}

#pragma mark -
#pragma mark Actions



-(void)customizeContextualMenu:(NSMenu*)aContextMenu for:(NSTextView  *)aTextView
{
	int piyo;
	NSMenuItem* customMenuItem;
	
	/* remove Button Inspector item */
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"ButtonAttachmentCellItem"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
		//add separator
		customMenuItem = [NSMenuItem separatorItem];
		[customMenuItem setRepresentedObject:@"ButtonAttachmentCellItem"];
		[aContextMenu addItem:customMenuItem ];
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Button Inspector",@"")
													action:nil keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"ButtonAttachmentCellItem"];
		[customMenuItem setTarget: aTextView];//[ButtonInspector sharedButtonInspector]];
		[customMenuItem setAction: @selector(openButtonInspector)];
		
		[aContextMenu addItem:customMenuItem ];
		[customMenuItem autorelease];	
			
}



-(void)action:(id)sender
{
	//NSLog(@"action"); 
	if( [textView conformsToProtocol:@protocol(ButtonAttachmentCellOwnerTextView)] )
		[textView buttonAttachmentCellClicked:self];
	
	
	[self setHighlighted:NO];
	
}



-(BOOL)removeFromTextView
{
	//NSLog(@"removeFromTextView");

	
	if( textView == nil ) return NO;
	if( ! [textView respondsToSelector:@selector(textStorage) ] ) return NO;
				
	//NSLog(@"start removing");
	
	NSTextStorage* textStorage = [[textView textStorage] retain];
	
	unsigned hoge = 0 ;
	TIMEOUT_WHILE(10)
	{
		////NSLog(@"hoge %d , %@",hoge, NSStringFromRange([self fullRange]));
		NSRange range;
		
		NSDictionary* dic = [textStorage attributesAtIndex:hoge effectiveRange:&range ];
		
		id object = [dic objectForKey:NSAttachmentAttributeName ];
		if( [object attachmentCell] == self )
		{
			[textStorage replaceCharactersInRange:NSMakeRange(hoge,1)  withString:@""];
			
			
		}
		else
		{
			hoge = NSMaxRange(range);
		}
		
		
		if( hoge >= [textStorage length] ) 
			break;
	}
	
	[textView buttonAttachmentCellUpdated:self ];
	
	[textStorage release];
	return YES;
}


@end
//<AliasAttachmentCellOwnerTextView>




