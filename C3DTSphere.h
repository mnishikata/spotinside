//
//  C3DTSphere.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTSphere.h,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTSphere.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTGeometry.h"


@interface C3DTSphere : C3DTGeometry {
    float		_radius;
    int			_slices;
    int			_stacks;
}

+ (C3DTSphere *)sphereWithRadius: (float)aRadius slices: (int)aSliceNumber stacks: (int)aStackNumber;

- (id)initWithRadius: (float)aRadius slices: (int)aSliceNumber stacks: (int)aStackNumber;

- (float)radius;
- (void)setRadius: (float)aRadius;

@end
