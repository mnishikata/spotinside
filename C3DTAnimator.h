//
//  C3DTAnimator.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Fri May 30 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTAnimator.h,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTAnimator.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import <Cocoa/Cocoa.h>
#import "C3DTNode.h"

#define	C3DT_REFRESH	@"C3DTRefresh"

@interface C3DTAnimator : C3DTNode {
    NSTimer			*_animateTimer;
    NSTimeInterval	_animateInterval;
}

+ (C3DTAnimator *)animatorWithInterval: (NSTimeInterval)anInterval;

- (id)initWithInterval: (NSTimeInterval)anInterval;

- (NSTimeInterval)animateInterval;
- (void)setAnimateInterval: (NSTimeInterval)anInterval;

- (void)start;
- (void)stop;

- (void)doAnimate: (NSTimer *)timer;

@end
