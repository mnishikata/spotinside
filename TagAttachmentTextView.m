//
//  ButtonAttachmentTextView.m
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 07/05/21.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//



#import "TagAttachmentTextView.h"

#import "NSTextView (coordinate extension).h"
//#import "AttachmentCellConverter.h"
#import "DrawLibrary.h"
#import "NSString (extension).h"
#import "TagManager.h"

#define AttributeSafeTextViewPboardType @"EdgiesTextPboardType"
#define AVAILABLE_TYPES [NSArray arrayWithObjects: @"DropAnythingPboardType", @"EdgiesTextPboardType", NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, NSTIFFPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType", NSFilenamesPboardType,   nil]

@implementation TagAttachmentTextView


/*
- (BOOL)resignFirstResponder
{	
	BOOL flag = [super resignFirstResponder];
	
	// Inputting Kanji
	if( [self hasMarkedText] )
		flag = NO;
	
	
	return flag;
	
}
- (BOOL)becomeFirstResponder
{
	return YES;
}

- (BOOL)acceptsFirstResponder {
	
	return YES;
}
*/

#pragma mark Action

- (void)didChangeText
{
	//NSLog(@"- (void)didChangeText %d",createTag);
	
	BOOL addedNewTag = NO;
	if( createTag )
	{
		NSMutableAttributedString* mattr = [[[NSMutableAttributedString alloc] init] autorelease];
		
		NSAttributedString* attr = [self textStorage] ;
		
		unsigned location;
		for( location = 0; location < [attr length]; )
		{
			NSRange effectiveRange;
			id attachment =  [attr attribute:NSAttachmentAttributeName
									 atIndex:location
					   longestEffectiveRange:&effectiveRange
									 inRange:NSMakeRange(0,[attr length])] ;
			if( attachment != nil )
			{
				[mattr appendAttributedString: [attr attributedSubstringFromRange: effectiveRange] ];
			}else
			{
				NSMutableString* originalRawString = [NSMutableString stringWithString: [[attr string] substringWithRange: effectiveRange]];
				
				[originalRawString replaceOccurrencesOfString:@"\n" withString:@"," options:0  range:NSMakeRange(0,[originalRawString length])];
				[originalRawString replaceOccurrencesOfString:@"\t" withString:@"," options:0  range:NSMakeRange(0,[originalRawString length])];
				
				NSArray* components = [originalRawString componentsSeparatedByString:@","];
				
				int hoge;
				for( hoge = 0; hoge < [components count]; hoge++ )
				{
					
					NSString* str = [components objectAtIndex:hoge];
					str = [str omitSpacesAtBothEnds];
					if( ![str isEqualToString:@""] )
					{
						
						NSAttributedString* newAttr = [ TagAttachmentCell newTagWithString:str selected:selectNewTag];
						[mattr appendAttributedString: newAttr ];
						addedNewTag = YES;
						

					}
				}
				
			}
			
			location = NSMaxRange(effectiveRange);
		}
		
		[[self textStorage] setAttributedString: mattr];
		if( addedNewTag )
		[[NSNotificationCenter defaultCenter] postNotificationName:@"TagAttachmentCellDidChangeNotificationName" object:self];

	}
	
	[super didChangeText];
}


-(void)setSelectNewTag:(BOOL)flag
{
	selectNewTag = flag;
}
-(IBAction)deselectAll:(id)sender
{
	[self deselectAllTags];
}
-(void)deselectAllTags
{
	unsigned hoge;
	NSRange range;
	for( hoge= 0; hoge < [[self textStorage] length];  )
	{
		NSDictionary* attr = [[self textStorage] attributesAtIndex:hoge longestEffectiveRange:&range inRange:[self fullRange]];
		
		id obj;
		if( (obj = [attr objectForKey: NSAttachmentAttributeName]) != nil )
		{
			if( [[obj attachmentCell] isKindOfClass:[TagAttachmentCell class]] )
			[[obj attachmentCell] setState:NSOffState];
		}
		
		hoge = NSMaxRange(range);
		
	}
	
	[[self window] display];
}

+(NSArray*)selectedCellsInAtributedString:(NSAttributedString*)attributedString
{
	NSMutableArray* cells = [NSMutableArray array];
	
	unsigned hoge;
	NSRange range;
	for( hoge= 0; hoge < [attributedString length];  )
	{
		NSDictionary* attr = [attributedString attributesAtIndex:hoge longestEffectiveRange:&range inRange:NSMakeRange(0,[attributedString length])];
		
		id obj;
		if( (obj = [attr objectForKey: NSAttachmentAttributeName]) != nil )
		{
			TagAttachmentCell *cell = [[obj attachmentCell] title];
			if( [cell isKindOfClass:[TagAttachmentCell class]] )
			{
				if( [cell state] == NSOnState  )
				{
					[cells addObject:cell];
				}
			}
		}
		
		hoge = NSMaxRange(range);
		
		
	}
	return cells;
	
}

+(NSArray*)selectedTagsInAtributedString:(NSAttributedString*)attributedString
{
	NSMutableArray* selectedTagTitles = [NSMutableArray array];
	
	unsigned hoge;
	NSRange range;
	for( hoge= 0; hoge < [attributedString length];  )
	{
		NSDictionary* attr = [attributedString attributesAtIndex:hoge longestEffectiveRange:&range inRange:NSMakeRange(0,[attributedString length])];
		
		id obj;
		if( (obj = [attr objectForKey: NSAttachmentAttributeName]) != nil )
		{
			TagAttachmentCell *cell = [obj attachmentCell];
			if( [cell isKindOfClass:[TagAttachmentCell class]] )
			{
				if( [cell state] == NSOnState  )
				{
					[selectedTagTitles addObject:[cell title]];
				}
			}
		}
		
		hoge = NSMaxRange(range);
		
		
	}
	return selectedTagTitles;
	
}

-(void)selectTags:(NSArray*)tags addWhenNotInList:(BOOL)flag 
//tags == string tags
{
	NSMutableArray* mutableTags = [NSMutableArray arrayWithArray:tags];
	
	unsigned hoge;
	NSRange range;
	for( hoge= 0; hoge < [[self textStorage] length];  )
	{
		NSDictionary* attr = [[self textStorage] attributesAtIndex:hoge longestEffectiveRange:&range inRange:[self fullRange]];
		
		id obj;
		if( (obj = [attr objectForKey: NSAttachmentAttributeName]) != nil )
		{
			if( [[obj attachmentCell] isKindOfClass:[TagAttachmentCell class]] )
			{
				if( [tags containsObject: [[obj attachmentCell] title]] )
				{
					[[obj attachmentCell] setState:NSOnState];
					[mutableTags removeObject:[[obj attachmentCell] title] ];
				}else
				{
					[[obj attachmentCell] setState:NSOffState];
				}
			}
		}
		
		hoge = NSMaxRange(range);
		
		
	}
	
	
	if( flag )
	{
		for( hoge= 0; hoge < [mutableTags count];hoge++  )
		{
			NSAttributedString *newTag = [TagAttachmentCell newTagWithString:[mutableTags objectAtIndex:hoge] selected:YES];
			[self insertText:newTag];
		}
	}
	
	[[self window] display]; 
}


#pragma mark Library



-(ButtonAttachmentCell*)mouseOnButtonAttachmentCell:(NSRange*)rangePointer
{
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	if( charIndex == NSNotFound || [self fullRange].length-1 < charIndex ) return NULL;
	
	id attachment = [[self textStorage] attribute:NSAttachmentAttributeName atIndex:charIndex longestEffectiveRange:rangePointer inRange:[self fullRange]];
	
	if ( attachment != nil && [[attachment attachmentCell] isKindOfClass: [ButtonAttachmentCell class]])
		return [attachment attachmentCell];
	
	
	return nil;	
}




#pragma mark -
#pragma mark Override

-(void)awakeFromNib
{
	selectNewTag = NO;
}





-(id)readTextFromPasteboard:(NSPasteboard *)pboard  draggingSource:(id <NSDraggingInfo>)sender
{
	
	NSArray* types = [pboard types];
	NSData* data = nil;
	NSAttributedString* attr = nil;
	
	//NSLog(@"readAttributedStringFromPasteboard... %@",[types description]);
	
	/*
	 BOOL UD_PASTE_AS_PLAIN_TEXT = YES; // new
	 BOOL UD_PASTE_AS_ARCHIVE = NO; // new 
	 */
	
	
	
	 if( [types containsObject: @"EdgiesTextPboardType"] ) 
	{
		data = [pboard dataForType:  @"EdgiesTextPboardType"];
		if( data == nil ) goto Done;
		
		
		attr = [NSKeyedUnarchiver unarchiveObjectWithData: data];
		//NSLog(@"readAttributedStringFromPasteboard EdgiesTextPboardType");
		
	}
	
	
Done:
		//NSLog(@"done");
		
		return attr;
	
	
	
}


-(BOOL)writeAttributedString:(NSAttributedString*)attr toPasteboard:(NSPasteboard *)pboard
{
	//NSLog(@"writeAttributedString");
	
	
	

	
	
	
	
	NSArray* basicTypes = [NSArray  arrayWithObjects: @"EdgiesTextPboardType", nil];
	
	
	

	
	
	//************
	
	[pboard declareTypes:basicTypes	owner:self];
	
	// 
	NSData* data = [NSKeyedArchiver archivedDataWithRootObject:attr ];
	[pboard setData:data forType:@"EdgiesTextPboardType"];
	
	
	//************

	
	return YES;
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
	//NSLog(@"performDragOperation attr");
	[[self window] restoreCachedImage];
	[[self window] flushWindow];
	[[self window] display];
	
	NSPasteboard *pboard = [sender draggingPasteboard];

	if( [[pboard types] containsObject:NSFilenamesPboardType]  )
	{
		NSArray* savedFiles = [pboard propertyListForType:NSFilenamesPboardType];
		[[TagManager sharedTagManager] showSheetForWindow:[self window] forFiles:savedFiles sender:droppingTarget];
		return YES;
	}


	return NO;
	 
}

- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard types:(NSArray *)types
{
	NSAttributedString* attr = [[self textStorage] attributedSubstringFromRange:[self selectedRange]];
	return [self writeAttributedString:attr toPasteboard:pboard];


}
- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard
{
	//NSLog(@"readSelectionFromPasteboard");
	id attr =  [self readTextFromPasteboard:pboard  draggingSource:nil];

	if( attr == nil ) return NO;
	
	[self pasteText: attr];
	
	return YES;
}



- (BOOL)dragSelectionWithEvent:(NSEvent *)event offset:(NSSize)mouseOffset slideBack:(BOOL)slideBack
{
	//NSLog(@"dragSelectionWithEvent");
	
	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard];
	[self writeSelectionToPasteboard:[NSPasteboard pasteboardWithName:NSDragPboard] types:AVAILABLE_TYPES];


	
	NSPoint origin;
	NSImage* image  = [self dragImageForSelectionWithEvent:event origin:&origin];

	[[self window] cacheImageInRect:[[self window] frame]];

	[self dragImage:image
				 at:origin
			 offset:mouseOffset
			  event:event
		 pasteboard:pb
			 source:self
		  slideBack:slideBack];
	
	return YES;
}


-(void)drawInsertionPoint
{
	
	NSPoint aPoint = [[self window] mouseLocationOutsideOfEventStream]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	if( insertionCharIndex == NSNotFound ) return;
	
	
	NSRange glyphRange = [[self layoutManager] 
	glyphRangeForCharacterRange:NSMakeRange(insertionCharIndex,1) actualCharacterRange:NULL ];
	
	NSRect rect = [[self layoutManager] boundingRectForGlyphRange:glyphRange inTextContainer:[self textContainer]];
	
	//rect = [self convertRect:rect toView:[[self window]contentView]];
	
	rect.size.width = 1;
	
	//[[[self window] contentView] lockFocus];
	[self lockFocus];
	
	[self drawInsertionPointInRect:rect color:[NSColor blackColor] turnedOn:YES];
	[self unlockFocus];
	
	[[self window] flushWindow];	
	
}
- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
	
	droppingTarget = nil;
	[[self window] cacheImageInRect:[[self window] frame]];
	
	
    NSPasteboard *pboard = [sender draggingPasteboard];
	if (self == [sender draggingSource]) //from self
		return NSDragOperationGeneric  ;
	
	else //from other
	{
		
		if([[pboard types] containsObject:NSFilenamesPboardType]  )
		{
			return   NSDragOperationCopy   ;
			
		}
		else
			return NSDragOperationCopy ;
	}	
}

- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender{
	
	
	BOOL needToBeUpdated = YES;
	
	NSPoint aPoint = [[self window] mouseLocationOutsideOfEventStream]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];

	//if( droppingInsertionPoint == insertionCharIndex ) needToBeUpdated = NO;
	droppingInsertionPoint = insertionCharIndex;
	
	


	//// display rect
	
	NSRange linkRange;
	NSRange cellRange;

	ButtonAttachmentCell* targetCell;

	

	
	droppingTarget = nil;
	
	if( needToBeUpdated == YES )
	{
		[[self window] flushWindow];	
		[[self window] display];
		[[self window] cacheImageInRect:[[self window] frame]];
	}
	
	[self drawInsertionPoint];

	
	////
	if (self == [sender draggingSource]) //from self
	{
		NSEvent* theEvent = [[self window] currentEvent];
		
		
		if( [theEvent modifierFlags] == 524576)
			return NSDragOperationCopy;
		else return NSDragOperationMove;
	}
	
	
	
	return NSDragOperationCopy;
	
}



- (void)draggingExited:(id <NSDraggingInfo>)sender
{
	[[self window] restoreCachedImage];
	[[self window] flushWindow];
	[[self window] display];
	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"EdgiesTextViewDraggingExitedNotification" object:self];
}



-(BOOL)dropText:(id)text draggingInfo:(id <NSDraggingInfo>)sender
{

	id  draggingSource = [sender draggingSource];
	NSPoint draggingLocation = [sender draggingLocation]; //point

	//
	
	NSAttributedString* attr;
	
	

	if( [text isKindOfClass:[NSString class]] )
		attr = [[NSAttributedString alloc] initWithString:text];
	else
		attr = text;
	
	
	NSRange rangeToBeDelete = {0,0};
	
	//delete original
	if( draggingSource == self )
	{
		// delete original if necessary.
		int modKey = GetCurrentKeyModifiers( );
		BOOL copyFlag = ( (modKey | 1024) == (2048 | 1024)  );
		if( ! copyFlag )
		{
			
			rangeToBeDelete = [self selectedRange];
			
		}		
		
	}
	
	
	
	
	NSRange insertedRange = NSMakeRange([self charIndexAtPoint:draggingLocation],0);
	
	if( insertedRange.location == NSNotFound ) insertedRange.location = [[[self textStorage] string] length];
	
	// do nothing if dropped onto originally selected range
	if( ( rangeToBeDelete.location <= insertedRange.location )
		&& ( insertedRange.location <= NSMaxRange(rangeToBeDelete) ) 
		&& rangeToBeDelete.length > 0 )
	{
		return NO;
	}
	
	[self setSelectedRange:insertedRange];
	
	
	unsigned _oldLength = [[self textStorage] length];
	
	[self pasteText:attr];
	
	unsigned _newLength = [[self textStorage] length];
	unsigned changeInLength = _newLength - _oldLength;
	
	insertedRange.length = changeInLength;
	
	if( rangeToBeDelete.location < insertedRange.location )
	{
		insertedRange.location -= rangeToBeDelete.length;
	}else
	{
		rangeToBeDelete.location += insertedRange.length;
	}
	[self setSelectedRange:rangeToBeDelete];
	[self pasteText:@""];
	
	
	[self setSelectedRange:insertedRange];

	return YES;
	
}

#pragma mark -



- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	
	id something = NULL;
	if( charIndex != NSNotFound )
	{
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:charIndex effectiveRange:NULL];
		
	}
	
	
	
	if(  
		  [ [something attachmentCell] isKindOfClass: [TagAttachmentCell class]] )
	{
		[self setSelectedRange:NSMakeRange(charIndex,1) ];
		
		[ [something attachmentCell] customizeContextualMenu:aContextMenu for:self];
		
	}
	
	
	[ TagAttachmentCell customizeContextualMenu:aContextMenu for:self];

	
	
	//alias
	
	


	
	
	return aContextMenu;
}
-(void)revealInFinder:(id)sender
{
	NSString* path = [[sender representedObject] path];
	
	if( path == nil )
	{
		
		NSBeep();
		return;
	}
	
	[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath: path];
	
}



#pragma mark @protocol ButtonAttachmentCellOwnerTextView 


-(void)buttonAttachmentCellUpdated:(ButtonAttachmentCell*)cell;
{
	NSLayoutManager* lm = [self layoutManager];
	NSRange fullRange = NSMakeRange(0,[[self textStorage] length]);
	
	[lm invalidateGlyphsForCharacterRange:fullRange
						   changeInLength:0 
					 actualCharacterRange:nil];
	
	[lm invalidateLayoutForCharacterRange:fullRange isSoft:NO
					 actualCharacterRange:nil];
		
	//[[self layoutManager] invalidateDisplayForCharacterRange:NSMakeRange(0,[[self textStorage] length]) ];
	[self display];
}



-(void)buttonAttachmentCellClicked:(ButtonAttachmentCell*)cell;
{
	if( [cell state] == NSOnState )
	[cell setState: NSOffState ];
	else
		[cell setState: NSOnState ];
	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"TagAttachmentCellDidChangeNotificationName" object:self];

}
-(void)buttonAttachmentCellDragged:(ButtonAttachmentCell*)cell atCharacterIndex:(unsigned)index;
{
	NSRange charRange = NSMakeRange(index,1);
	[self setSelectedRange:charRange];
	
	NSEvent* theEvent = [[self window] 
				nextEventMatchingMask:NSAnyEventMask
							untilDate:[NSDate dateWithTimeIntervalSinceNow:5.0]
							   inMode:NSEventTrackingRunLoopMode
							  dequeue:YES];
	
	if( [theEvent type] != NSLeftMouseDragged ) return;
	
	

	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard];
	
	NSAttributedString* attr = [[self textStorage] attributedSubstringFromRange:charRange] ;
	[self writeAttributedString:attr toPasteboard:pb];
	
	
	//NSEvent* theEvent = [NSApp currentEvent];
	
	NSRange grange = [[self layoutManager] glyphRangeForCharacterRange:charRange
												  actualCharacterRange:nil];
	NSRect grect = [[self layoutManager] boundingRectForGlyphRange:grange inTextContainer:[self textContainer]];
	grect.origin.y += grect.size.height;
	
	//NSPoint origin = [self convertPoint:[[self window] mouseLocationOutsideOfEventStream] fromView:nil];
	NSImage* image  = [cell buttonImageWithAlpha:0.5];
	
	[[self window] cacheImageInRect:[[self window] frame]];
	
	[self dragImage:image
				 at:grect.origin
			 offset:NSMakeSize(0,0)
			  event:theEvent
		 pasteboard:pb
			 source:self
		  slideBack:YES];	
}




- (BOOL)shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString
{
	createTag = ( [replacementString rangeOfString:@"\n"].location != NSNotFound )|
	( [replacementString rangeOfString:@"\t"].location != NSNotFound )|
	( [replacementString rangeOfString:@","].location != NSNotFound );

	
	NSArray* cells = [self buttonAttachmentCellArrayInRange: affectedCharRange];
	
	BOOL canChange = YES;
	
	unsigned int i, count = [cells count];
	for (i = 0; i < count; i++) {
		id cell = [cells objectAtIndex:i];
		if( ![cell textView:self shouldChangeTextInRange:affectedCharRange  replacementString:replacementString] )
			canChange = NO;
	}
	
	return canChange && [super shouldChangeTextInRange:(NSRange)affectedCharRange replacementString:(NSString *)replacementString];
}



-(NSArray*)attachmentCellArrayInRange:(NSRange)range forClass:(Class)class
{
	if( range.length == 0 ) return nil;
	
	
	NSMutableArray* cells = [NSMutableArray array] ;
	
	
	unsigned location;
	for( location = range.location; location < NSMaxRange(range); )
	{
		NSRange effectiveRange;
		id attachment =  [[self textStorage] attribute:NSAttachmentAttributeName
											   atIndex:location
										effectiveRange:&effectiveRange] ;
		if( attachment != nil )
		{
			NSCell* cell = [attachment attachmentCell];
			if( [cell isKindOfClass: class ] )
				[cells addObject:[attachment attachmentCell]];
		}
		
		location = NSMaxRange(effectiveRange);
	}
	
	return cells;	
}


-(NSArray*)buttonAttachmentCellArrayInRange:(NSRange)range
{
	return [self attachmentCellArrayInRange:range forClass:[ButtonAttachmentCell class]];
}



-(void)destroyButtonAttachmentTextView
{
	[self shouldChangeTextInRange:NSMakeRange(0, [[self textStorage] length]) replacementString:@""];
}



#pragma mark <TagAttachmentCellOwnerTextView>



-(NSArray*)tagAttachmentCellArrayInRange:(NSRange)range
{
	return [self attachmentCellArrayInRange:range forClass:[TagAttachmentCell class]];

}


-(NSArray*)tagAttachmentCellArrayInSelectedRange
{
	NSRange range = [self selectedRange];
	return [self tagAttachmentCellArrayInRange:range];
}
#pragma mark DEBUG 

// DEBUG

-(void)action:(id)sender
{
	NSAttributedString* attr = [TagAttachmentCell newTagWithString: @"Test"  selected:NO];
	
	[self insertText:attr ];
}




@end
