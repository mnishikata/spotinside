//
//  C3DTTexture.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTTexture.h,v 1.5 2003/07/08 11:19:10 pmanna Exp $
//
// $Log: C3DTTexture.h,v $
// Revision 1.5  2003/07/08 11:19:10  pmanna
// Modified to use a more generic generateTexture
//
// Revision 1.4  2003/06/29 20:22:40  pmanna
// Moved nextPowerOf2 to public
//
// Revision 1.3  2003/06/22 09:39:36  pmanna
// Refactoring of texture objects to consolidate common tasks
//
// Revision 1.2  2003/06/19 20:33:40  pmanna
// Modified init and class method names
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTStyle.h"
#import	"C3DTMath.h"
#import <Cocoa/Cocoa.h>
#import <OpenGL/glu.h>

#define	C3DT_REFRESH	@"C3DTRefresh"

extern int nextPowerOf2(int value);

@interface C3DTTexture : C3DTStyle {
    GLuint		_textureId;
    GLenum		_textureFormat;		// Format of texture (GL_RGB, GL_RGBA)
    NSSize		_textureSize;		// Width and height
    GLubyte		*_textureBytes;		// Texture data
    _C3DTVector	_color;				// The base color

    GLuint		_savedTextureId;
    BOOL		_textureSaved;
    _C3DTVector	_savedColor;
}

+ (C3DTTexture *)textureWithFile: (NSString *)aName;

- (id)initWithFile: (NSString *)aName;

- (void)generateTextureWithBytes: (void *)texBytes refresh: (BOOL)isRefresh;
- (BOOL)bitmapFromImageRep: (NSBitmapImageRep *)theImage;

- (_C3DTVector)color;
- (void)setColor: (_C3DTVector)aColor;
- (void)setColorRed: (float)redColor green: (float)greenColor blue: (float)blueColor alpha: (float)alphaValue;
- (id)initWithBitmapImageRep: (NSBitmapImageRep *)imageRep;

@end
