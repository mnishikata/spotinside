//
//  MyDocument.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/06/24.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import "MyDocument.h"
//#import "RBSplitSubview.h"
//#import "ThreadWorker.h"
#import "SpotlightTextContentRetriever.h"

#import "ListItem.h"
#import "SearchListItem.h"
#import "HistoryListItem.h"
#import "AliasListItem.h"
#import "SmartFolderListItem.h"
#import <PDFKit/PDFKit.h>

#import <OgreKit/OgreKit.h>
#import <MNCoverFlow/MNCoverFlow.h>

#import "PDFSelection+drawAddition.h"
#import "OASplitView.h"

#import "RBSplitView.h"
#import "TagAttachmentTextView.h"

#define SCRATCH_FOLDER [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/SpotInside/"]


#define FoundItem @"foundItem"
#define FoundItemTypes [NSArray arrayWithObjects:@"foundItem", nil]


#define START_SEARCH_WITH_EMPTY_STRING NO



//#import "TEC.h"
//#import "JapaneseString.h"

//#import "ControlCodeLayoutManager.h"

@class DefaultListItem;


@implementation MyDocument

/*
-(NSUndoManager*)undoManager
{
	
	NSLog(@"class %x %@",window, [[window firstResponder] className]);
	NSLog(@"class %x %@",[self windowForSheet], [[[self windowForSheet] firstResponder] className]);


	NSLog(@"undomanager");
	return [super undoManager];
}
*/

- (id)init
{
    self = [super init];
    if (self) {
		
		_searchKey = nil;
		//_searchKeywords = nil;
		
		//queryTemplate = nil;//[[NSMutableString alloc] init];
		//scope = nil;//[[NSMutableArray alloc] init];
		//query = nil;//[[NSMutableString alloc] init];
		
		flowView = nil;
		
		[self setHasUndoManager:YES];
			
		
		if( ! [[NSFileManager defaultManager] directoryContentsAtPath:SCRATCH_FOLDER] )
			[[NSFileManager defaultManager] createDirectoryAtPath:SCRATCH_FOLDER attributes:NULL];
		

		
		//_queryExact =  [[NSMetadataQuery alloc] init];
		
		
		
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
		
		
		[[windowUIObjectController selection] setValue: @"(kMDItemTextContent == \"%@*\"cd) && (kMDItemContentType != com.apple.mail.emlx) && (kMDItemContentType != public.vcard)" forKey:@"queryTemplate"];
		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		NSString* _format = [ud objectForKey:@"defaultQueryTemplate"];

		////NSLog(@"_format%@",_format);
		if( _format == nil || [_format isEqualToString:@""] )
		{

			[ud setObject:[[windowUIObjectController selection] valueForKey:@"queryTemplate"] forKey:@"defaultQueryTemplate"];
		}
		else 
		{
			[[windowUIObjectController selection] setValue:_format forKey:@"queryTemplate"];
		}
		
		
		//[queryTemplate retain];
		
		
		
		
		///
		
		//converters_ = [[NSMutableDictionary alloc] init];

		//NSLog(@"/init");
    }
    return self;
}




- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"MyDocument";
}

-(void)close
{
	////NSLog(@"close");

	[[findResult content] removeAllObjects ];
	[findResult setContent:nil];
	
	//[windowUIObjectController setContent:nil];
	
	/// save default table column.  
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	[ud removeObserver:self forKeyPath:@"disclosureButton"];
		

	[[NSUserDefaults standardUserDefaults] setBool:[[flowView window] isVisible] forKey:@"flowViewVisible"];

	
	NSMutableArray* array = [NSMutableArray array];
	
	NSArray* columns = [table tableColumns];
	int hoge;
	for( hoge = 0; hoge < [columns count]; hoge++)
	{
		[array addObject:[[columns objectAtIndex:hoge] identifier]];
			
		
	}
	[ud setObject:array forKey:@"defaultTableColumns"];

	
	//[ud setBool:([drawer state]==NSDrawerOpenState )  forKey:@"drawerOpened"];
	
	
	////
	
	
	//[metaList removeObjects:[metaList content]];
	
	[table saveTableColumn];
	NSArray* sortDescriptors = [findResult sortDescriptors];
	
	[ud setObject:[NSKeyedArchiver archivedDataWithRootObject: sortDescriptors] forKey:@"ResultTableSortDescriptors"];
	
	
	float dim = [[[splitView subviews] objectAtIndex:0] frame].size.height;
	[ud setFloat:dim forKey:@"rbsplitdimension"];
	
	
	dim = [flowPane dimension];
	[ud setFloat:dim forKey:@"flowPaneDimension"];
	


	//
//	[[NSApp delegate] setRecentItems: [field recentSearches]];
	[[NSApp delegate] saveOrganiser];

	
	[ud synchronize];

	[super close];
}

-(void)dealloc
{
//	if( _query != nil )
//	CFRelease(_query);
	
	
	
	[[self windowForSheet] makeFirstResponder : nil];
	NSNotificationCenter *nf = [NSNotificationCenter defaultCenter];
	[nf removeObserver:self ];
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"disclosureButton"];
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"tagPaneVisible"];
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:@"flowViewVisible"];

	//[queryTemplate release];

	//[fillPatternImage release];
	[_searchKey release];
	//[_searchKeywords release];

	
	//[converters_ release];
	
	
	[super dealloc];
	
	
}


- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	[ud synchronize];
	
	
	NSDisableScreenUpdates();
	
	////NSLog(@"windowControllerDidLoadNib %d", [NSBundle loadNibNamed:@"MyDocument" owner:self]);

    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.

	
	// setting up panes
	NSView* subview = [mainSplitView subviewAtPosition:0];
	BOOL isCollapsed = [subview isCollapsed];
	[subview expand];
	NSView* contentView = [drawer contentView];
	[contentView retain];
	[contentView removeFromSuperview];
	
	[contentView setFrameSize:[subview frame].size ];
	[contentView setFrameOrigin:NSZeroPoint];
	[contentView setAutoresizingMask: NSViewHeightSizable|NSViewWidthSizable ];
	[subview addSubview:contentView];
	
	[mainSplitView adjustSubviews];
	[drawer setParentWindow:nil];
	if( isCollapsed )
		[subview collapse];
	
	///
	//secondSplitView
	subview = [mainSplitView subviewAtPosition:1];
	
	[secondSplitView setFrameSize:[subview frame].size ];
	[secondSplitView setFrameOrigin:NSZeroPoint];
	[secondSplitView setAutoresizingMask: NSViewHeightSizable|NSViewWidthSizable ];
	[subview addSubview:secondSplitView];

	
	
	[mainSplitView adjustSubviews];
	//
	
	if( [ud boolForKey:@"flowViewVisible"] )
	{
		[self setupFlowView];		
		
	}
	/*

*/
	
	
	[mainSplitView adjustSubviews];
	

	
	/////
		

	
	/*
	id obj = [ud objectForKey:@"highlightColor"];
	if( obj != nil )
		highlightColor = [[NSUnarchiver unarchiveObjectWithData:obj] retain];
	
	else
	{
		highlightColor = [[NSColor colorWithCalibratedRed:0.2
													green:0.2
													 blue:1.0 alpha:1.0] retain];
		
		[ud setObject:[NSArchiver archivedDataWithRootObject:highlightColor]
			   forKey:@"highlightColor"];
	}
	*/
	

	//if( [ud boolForKey:@"drawerOpened"] )
	//	[drawer open];
	
	
	[[windowUIObjectController selection] setValue:[NSNumber numberWithInt:0] forKey:@"selectedTabIndex"];
	
//	[self setupTableColumn];
	
	//[field setRecentSearches:[[NSApp delegate] recentItems ]];
	[[aController window] performSelector:@selector(makeFirstResponder:) withObject:field afterDelay:0.2];

	[[aController window] setFrameUsingName: @"MyDocumentWindow"];
	[[aController window] setFrameAutosaveName: @"MyDocumentWindow"];
	[[aController window] setMovableByWindowBackground:YES];

	//[[aController window] setFlat:YES];
	
	
	//ControlCodeLayoutManager* lm = [[[ControlCodeLayoutManager alloc] init] autorelease];
	//[lm setBackgroundLayoutEnabled:YES];
	
	
	//[[textView textContainer] replaceLayoutManager:lm];

	[[OgreTextFinder sharedTextFinder] setTargetToFindIn:textView];
	[[OgreTextFinder sharedTextFinder] setShouldHackFindMenu:YES];
	
	//NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	/*
	NSString* str = [ud stringForKey:@"location"];
	if( str != nil )
		[location setStringValue:str];
	
	
	
	[location setDelegate:self];
	*/
	

	
	int _index = [ud integerForKey:@"splitDirection"];
	if( _index == 0 )//horizontal
	{
		[splitView setVertical:NO];
	}else
	{
		[splitView setVertical:YES];
	}
	
	float dim = [ud floatForKey:@"rbsplitdimension"];
	if( dim > 0 )
	{
		NSRect rect = [[[splitView subviews] objectAtIndex:0] frame];
		rect.size.height = dim;
		
		[[[splitView subviews] objectAtIndex:0] setFrameSize : rect.size ];
	}
	

	
	//NSLog(@"4");

	
	// DISCLOSURE
	
	//NSLog(@"[mainSplitView frame] %@",NSStringFromRect([mainSplitView frame]));
	//NSLog(@"[advancedBox frame] %@",NSStringFromRect([advancedBox frame]));

	
	// Advanced Search
	[ud addObserver:self forKeyPath:@"disclosureButton" options:NSKeyValueObservingOptionNew context:nil];

	if( [ud integerForKey:@"disclosureButton" ] ==  NSOffState )
	{
		
		NSSize size = [mainSplitView frame].size;
		size.height += [advancedBox frame].size.height;
		
		//NSLog(@"4a");

		[advancedBox setHidden:YES];
		//NSLog(@"4b");

		[mainSplitView setFrameSize: size];
		//NSLog(@"4c");

		[mainSplitView adjustSubviews];
	}
	
	//[windowUIObjectController setContent:self];

	//fillPatternImage = [[NSImage alloc] initByReferencingFile:
	//	[[NSBundle bundleForClass:[self class]] pathForResource:@"fillPattern" ofType:@"tiff"]];

	
	
	[[tagPane superview] adjustSubviews];
	
	
	
	[tagPane setDimension:[ud floatForKey:@"tagPaneDimension"] ];
	[flowPane setDimension:[ud floatForKey:@"flowPaneDimension"] ];

	
	//NSLog(@"tagPaneVisible %d",[ud boolForKey:@"tagPaneVisible"]);
	if( [ud boolForKey:@"tagPaneVisible"] )
	{
		if( [tagPane isCollapsed]   )  [tagPane expand];
		
		
	}
	
	else{
		//[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];
		//[tagPane setDimension:[ud floatForKey:@"tagPaneDimension"] ];
		
		if( ![tagPane isCollapsed]   )  [tagPane collapse];
	}
	[ud addObserver:self forKeyPath:@"tagPaneVisible" options:NSKeyValueObservingOptionNew context:nil];

	
	
	/*
	if( [ud boolForKey:@"flowViewVisible"] )
	{
		if( [flowPane isCollapsed]   )  [flowPane expand];
		
		
	}
	
	else{
		//[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];
		//[tagPane setDimension:[ud floatForKey:@"tagPaneDimension"] ];
		
		if( ![flowPane isCollapsed]   )  [flowPane collapse];
	}
	[ud addObserver:self forKeyPath:@"flowViewVisible" options:NSKeyValueObservingOptionNew context:nil];
	*/
	
	

	

	//NSLog(@"5");

	//setup table
	int hoge;

	NSArray* array = [table tableColumns];
	for( hoge = 0; hoge < [array count]; hoge++ )
	{
		[table removeTableColumn:[array objectAtIndex:hoge]];
	}
	
	for( hoge = 0; hoge < [[[NSApp delegate] allKeys] count]; hoge++ )
	{
		NSString* attr = [[[NSApp delegate] allKeys] objectAtIndex:hoge];
		

		
		[table addColumnWithIdentifier:attr
								 title:nil bindToObject:findResult ];
	}	
	
	//NSLog(@"6");

	[table setDoubleAction:@selector(launch:)];
	
	//d&d
	[table registerForDraggedTypes:[NSArray arrayWithObjects:NSStringPboardType, nil]];

	NSData* descriptors = [ud objectForKey:@"ResultTableSortDescriptors"];
	if( descriptors != nil )
	{
		[findResult setSortDescriptors:[NSKeyedUnarchiver unarchiveObjectWithData: descriptors]];
	}
	
	//NSLog(@"7");

	
	////[table loadTableColumnPosition];
	
	///
	
	[splitView setDelegate:self];
	[splitView setPositionAutosaveName:@"OASplitView"];

	
	//// file path bug fix
	
	//
	//[sliderSmall setBezelStyle:NSRecessedBezelStyle];
	//[sliderLarge setBezelStyle:NSRecessedBezelStyle];
	//[sliderSmall setShowsBorderOnlyWhileMouseInside:YES];
	//[sliderLarge setShowsBorderOnlyWhileMouseInside:YES];

	/*
	NSAttributedString* nullmsg = [[[NSAttributedString alloc] initWithString:@"Type tags to find / Drop file here to tag"] autorelease];
	
	NSDictionary* bindOptions = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:YES],NSContinuouslyUpdatesValueBindingOption, 
		@"KeyedUnarchiveFromDataTransformer", NSValueTransformerNameBindingOption,
		nullmsg, NSNullPlaceholderBindingOption,
		nil];
	
	[tagTextView bind:NSAttributedStringBinding  toObject:[NSUserDefaults standardUserDefaults] withKeyPath:@"values.tagList" options:bindOptions];
	*/
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tagChanged) name:@"TagAttachmentCellDidChangeNotificationName" object:tagTextView];
	
	
	
	

	[self setDefault];
	
	/*
	if( loadData != nil )
	{
		
		[self startQueryUsingData:loadData clearResultsFirst:YES overwriteFormat:YES];
		[loadData release];

	}*/

    
	NSEnableScreenUpdates();
	
}
- (void)splitViewDoubleClick:(OASplitView *)sender
{
	
	NSView* view = [[splitView subviews] objectAtIndex: 0 ];
	
	NSRect rect = [view frame];
	rect.size.height = 0;
	[view setFrame: rect];
	[splitView adjustSubviews];
}




#pragma mark - 

-(void)setDefault
{
	//NSLog(@"setDefault");
	
	//scope = [[NSArray arrayWithObject: NSMetadataQueryLocalComputerScope ] retain];
	
	[[windowUIObjectController selection] setValue:[NSArray arrayWithObject: NSMetadataQueryLocalComputerScope ]
								forKey:@"scope"];
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	NSString* _format = [ud objectForKey:@"defaultQueryTemplate"];
	
	if( _format != nil )
	{
		//[queryTemplate release];
		//queryTemplate = [[NSString stringWithString: _format] retain];
		[[windowUIObjectController selection] setValue:_format forKey:@"queryTemplate"];
	}
	
	[field setStringValue:@""];
	[_searchKey release];
	_searchKey = [@"" retain];
	
	//[_searchKeywords release];
	//_searchKeywords = nil;
	
	[[windowUIObjectController selection] setValue:[NSArray array ]
								forKey:@"searchKeywords"];


}




-(NSString*)queryTitle
{
	return _searchKey;
}
/*

- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError
{
	NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys: 
		_searchKey, @"query",
		queryTemplate, @"format",
		[location stringValue], @"searchScope",
		nil];
	
	NSString* str = [dict description];
	NSData* savedata = [str dataUsingEncoding:NSUTF8StringEncoding];
	return savedata;
	
}

- (BOOL)readFromData:(NSData *)fromdata ofType:(NSString *)typeName error:(NSError **)outError
{
	loadData = fromdata;
	[loadData retain];
	
	return YES;
}
*/

-(NSString*)escape:(NSString*)str
{
	if( str == nil ) str = @"";

	NSMutableString* mstr = [[[NSMutableString alloc] initWithString:str] autorelease];
	
	[mstr replaceOccurrencesOfString:@"\"" withString:@"\\\""
		   options:1
			 range:NSMakeRange(0,[mstr length])];

	[mstr replaceOccurrencesOfString:@"*" withString:@"\\*"
									options:1
									  range:NSMakeRange(0,[mstr length])];

	return (NSString*)mstr;
}


- (IBAction)launch:(id)sender
{
	
	////NSLog(@"launch:");
	
	
	////NSLog(@"findResult %d",[[findResult content] count]);
	
	id mi;
	
	int row = [findResult selectionIndex];
	if( row != NSNotFound )
	{
		mi = [[findResult selectedObjects] lastObject];
		
		NSString* path = [mi objectForKey: kMDItemPath ];
		
		
		[[NSWorkspace sharedWorkspace] openFile:path];
		
	}
	
}

-(IBAction)google:(id)sender
{
	NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
	NSString* str = [ud objectForKey: @"googleFormat"];
	
	NSString* str2 = [NSString stringWithFormat:str,_searchKey];
	
	
	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	[ws openURL:[NSURL URLWithString:[str2 stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] ];
}


- (IBAction)revealInFinder:(id)sender
{
	NSMetadataItem* mi;
	
	
	int row = [findResult selectionIndex];
	if( row != NSNotFound )
	{
		mi = [[findResult arrangedObjects] objectAtIndex:row];
		
		
		NSString* path = [mi objectForKey: kMDItemPath ];
		
		
		[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath: path];
		
	}
}

- (IBAction)search:(id)sender
{
	if ([[table window] makeFirstResponder:[table window]]) {
		/* All fields are now valid; it’s safe to use fieldEditor:forObject:
        to claim the field editor. */
	}
	else {
		/* Force first responder to resign. */
		[[table window] endEditingFor:nil];
	}
	
	[[NSApp delegate] saveOrganiser];

	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SearchAgentCancelRequestNotification"
														object:nil];

	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	float dim = [[[splitView subviews] objectAtIndex:0] frame].size.height;
	[ud setFloat:dim forKey:@"rbsplitdimension"];
	
	//[ud setValue:[location stringValue] forKey:@"location"];

	
	//
	
	//SECRET FILTER MODE
	
	int modKey = GetCurrentKeyModifiers( );
	////NSLog(@"%d",modKey);
	if(  (modKey | 1024) == (2048 | 1024) ) // option
	{

		
		if( ! [[field stringValue] isEqualToString:@"" ] )
		{
			NSPredicate* predicate = [NSPredicate predicateWithFormat:@"kMDItemPath matches %@",[field stringValue]];
			[findResult setFilterPredicate: predicate ];
		}else
		{
			[findResult setFilterPredicate: nil ];

		}
		[findResult rearrangeObjects];
		[flowView reloadData];

		return;
		
	}
	
	
	
	
	id item = [outlineView itemAtRow:[outlineView selectedRow]];
	
	[_searchKey release];
	_searchKey = [field stringValue];
	[_searchKey retain];
	
	//[self willChangeValueForKey:@"_searchKeywords"];
	//[_searchKeywords release];

	NSMutableSet* set = [NSMutableSet set];
	[set addObject:_searchKey];
	
	BOOL  findOption = [ud boolForKey:@"findOption"];
	if( findOption )
	{
		[set addObjectsFromArray: [_searchKey words]];

	}
	
	[set removeObject:@"AND"];
	[set removeObject:@"NOT"];
	[set removeObject:@"OR"];

	
	[[windowUIObjectController selection] setValue : [set allObjects] forKey:@"searchKeywords"];
	//_searchKeywords = [[set allObjects] retain];

	//[self didChangeValueForKey:@"_searchKeywords"];

	
	
	//delete all the objects
	[findResult setFilterPredicate:nil];
	[[findResult content] removeAllObjects ];
	[findResult rearrangeObjects];
	[flowView reloadData];

	
	NSArray* tags = [TagAttachmentTextView selectedTagsInAtributedString:[tagTextView textStorage] ];
	if(  (_searchKey == nil || [_searchKey isEqualToString:@""] ) && [tags count]==0 )
	{
		[item searchForArrayController:findResult withEndTarget:self andSelector:@selector(searchEnd:)];
		
		
		
	}else
	{
		NSMutableString *formattedQuery = nil;

		NSString* str = [field stringValue];
		
		if( ! (str == nil || [str isEqualToString:@""] )  )
		{

			str = [self escape:str];
			
			
			
			formattedQuery = [NSMutableString stringWithString: [[windowUIObjectController selection] valueForKey:@"queryTemplate"]];
			
			formattedQuery = [self expandBooleanLiteral:str withTemplate: formattedQuery];
			/*
			[formattedQuery replaceOccurrencesOfString:@"%@"
											 withString:str
												options:NSCaseInsensitiveSearch
											  range:NSMakeRange(0,[formattedQuery length])];
*/
			
		}
		// TAG
		if( [[NSUserDefaults standardUserDefaults] boolForKey:@"tagPaneVisible" ] )
			formattedQuery= [self addTagQuery:formattedQuery];

		
		[[windowUIObjectController selection] setValue:formattedQuery forKey:@"query"];
		
		
		
		//observe 
		[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:YES] forKey:@"isSearching"];

		//NSLog(@"posting searchWithAdditionalQuery to %@",[item className]);
		NSString* modifiedQuery = [item searchWithAdditionalQuery:formattedQuery
						 forArrayController:findResult
							  endTarget:self andSelector:@selector(searchEnd:)];


		[[windowUIObjectController selection] setValue:modifiedQuery forKey:@"query"];
		
		SearchListItem *historyItem = [[[SearchListItem alloc] init] autorelease];
		[[self undoManager] disableUndoRegistration];
		
		[historyItem setScope:[item scope]];
		[historyItem setQuery:modifiedQuery];
		[historyItem setQueryTemplate: [[windowUIObjectController selection] valueForKey:@"queryTemplate"] ];
		[historyItem setName:[field stringValue]];
		
		[[HistoryListItem sharedHistoryListItem] addHistory:historyItem ];
		
		[[self undoManager] enableUndoRegistration];

		[outlineView reloadData];
		
		[self setFileName:[field stringValue]];
	}
		
	NSLog(@"searched");
	
	return;
	
	
}

-(NSString*)expandBooleanLiteral:(NSString*)rawString withTemplate:(NSString*)template
{
	/*
	 A NOT B =  ((A) && ( !(B) ))
	 A AND B = ((A) && (B))
	 A OR B = ((A) || (B))
	 
	 */
	
	OGRegularExpression *regex = [OGRegularExpression regularExpressionWithString:@"OR|NOT|AND"];
	
	
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:rawString];
	OGRegularExpressionMatch    *match;	// マッチ結果
	NSMutableArray* components = [NSMutableArray array];

	unsigned location = 0;

	while ((match = [enumerator nextObject]) != nil) {	// 順番にマッチ結果を得る
		
		NSRange range = [match rangeOfMatchedString];
		
		if( range.location != 0 ) {
			NSString* leaf = [rawString substringWithRange: NSMakeRange(location, range.location -location) ];
			
			NSMutableString* formattedQuery = [NSMutableString stringWithFormat:@"(%@)", template ];
			 [formattedQuery replaceOccurrencesOfString:@"%@"
													 withString:[leaf omitSpacesAtBothEnds]
														options:NSCaseInsensitiveSearch
												  range:NSMakeRange(0,[formattedQuery length])];
					 
					 
			[components addObject: formattedQuery];
		}
		if( [[match matchedString] isEqualToString:@"NOT"] )
			[components addObject:@"&& !"];
		else [components addObject:[match matchedString]];
		location = NSMaxRange(range);
	}
	
	if( location < [rawString length]  )
	{
		NSString* leaf = [rawString substringWithRange: NSMakeRange(location, [rawString length] -location) ];

		NSMutableString* formattedQuery = [NSMutableString stringWithFormat:@"(%@)", template ];
		[formattedQuery replaceOccurrencesOfString:@"%@"
										withString:[leaf omitSpacesAtBothEnds]
										   options:NSCaseInsensitiveSearch
											 range:NSMakeRange(0,[formattedQuery length])];
		
		
		[components addObject: formattedQuery];
	}
	
	NSString* str = [components componentsJoinedByString:@" "];
	
	//NSLog(@"str %@",str);
	

	NSMutableString* afterFormat = [NSMutableString stringWithString:str];
	[afterFormat replaceOccurrencesOfString:@"AND" withString:@"&&" options:0 range:NSMakeRange(0,[afterFormat length])];
	[afterFormat replaceOccurrencesOfString:@"OR" withString:@"||" options:0 range:NSMakeRange(0,[afterFormat length])];
	

	return afterFormat;
	
	
	
}

-(void)searchEnd:(id)sender
{
	[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:NO] forKey:@"isSearching"];
	/*
	[[NSNotificationCenter defaultCenter] removeObserver:self
												 name:@"ListItemDidFinishSearchWithAdditionalQuery"
											   object:[notification object]];
	*/
	[findResult rearrangeObjects];
	int v_count =  [[findResult content] count];
	NSString* string = [NSString stringWithFormat:NSLocalizedString(@"%d files",@"") ,v_count];
	
	[[windowUIObjectController selection] setValue:string forKey:@"resultCount"];
	
	[flowView reloadData];
	
	//selectFirstRow
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	if( [ud boolForKey:@"selectFirstRow"] && v_count > 0)
	{
		[[self windowForSheet] makeFirstResponder :table];

		NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:0];
		[table selectRowIndexes:indexes byExtendingSelection:NO];
	}
	
}



-(void)displayWithFile:(NSString*)path
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	int opt = [[ud valueForKey:@"pdfOption"] intValue];
	
	BOOL focusOption = [ud boolForKey:@"focusPreview"];
	
	
	[pdfView setDocument: nil];

	NSString* uti = [self uti:path];
	NSArray* utiTree = [self utiTree:path];

	
	if( (opt == 0 ) && ( [uti isEqualToString:@"com.adobe.pdf"]||
		[uti isEqualToString:@"com.adobe.illustrator.ai-image"] ) )
	{
		[[windowUIObjectController selection] setValue:[NSNumber numberWithInt:1] forKey:@"selectedTabIndex"];
		
		PDFDocument* pdfDoc = [[[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:path]] autorelease];
		[pdfView setDocument: pdfDoc];
		[pdfView goToFirstPage:nil];
		
		
		if( focusOption )
			[[self windowForSheet] makeFirstResponder :pdfView];
		
	}else if(  [utiTree containsObject:@"public.image"]  && !( [[self uti:path] isEqualToString:@"com.adobe.pdf"]|| [uti isEqualToString:@"com.adobe.illustrator.ai-image"] ))
	{
		[[windowUIObjectController selection] setValue:[NSNumber numberWithInt:2] forKey:@"selectedTabIndex"];

	
		NSImage* image = [[NSImage alloc] initWithContentsOfFile: path];
		[imageView setImage: image];
		[image release];
		
		if( focusOption )
			[[self windowForSheet] makeFirstResponder :textView];

	}else{
		[[windowUIObjectController selection] setValue:[NSNumber numberWithInt:0] forKey:@"selectedTabIndex"];
		
		
		[[textView textStorage] setAttributedString:[self loadTextAt:path] ];
		
		[textView sizeToFit];
		
		if( focusOption )
			[[self windowForSheet] makeFirstResponder :textView];
		
	}
	

}


-(id)findResult
{
	return findResult;
}
/*
-(TECConverter*)createConverter:(NSStringEncoding) encoding
{
	TECConverter* converter;
	
	converter = [converters_ objectForKey: [NSNumber numberWithInt: encoding]];
	
	if (! converter) {
		converter = [[TECConverter alloc] initWithEncoding: encoding];
		[converters_ setObject: converter
						forKey: [NSNumber numberWithInt: encoding]];
		[converter release];
	}
	
	return converter;
}
*/
-(NSString*)uti:(NSString*)path
{
	// Get UTI of the given file
	NSString* uti = @"";
	NSString* targetFilePath = [path stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
	NSURL* anUrl = [NSURL URLWithString: targetFilePath];
	FSRef ref;
	CFURLGetFSRef(anUrl,&ref);
	CFTypeRef outValue;
	LSCopyItemAttribute (
						 &ref,
						 kLSRolesAll,
						 kLSItemContentType,
						 &outValue
						 );
	
	if( outValue != nil )
	{
		uti = [NSString stringWithString:outValue];
		CFRelease(outValue);
		
	}	
	return uti;
}

-(NSArray*)utiTree:(NSString*)path
{
	NSArray* returnArray = nil;
	
	MDItemRef item = MDItemCreate(kCFAllocatorDefault, (CFStringRef)path);
	if(item != NULL)
	{
		// Can't just get a dictionary of everything. Oh well.
		CFArrayRef attrNames = MDItemCopyAttribute( item, (CFStringRef)@"kMDItemContentTypeTree" );
		if(attrNames != NULL)
		{
			returnArray = [[NSArray alloc ]initWithArray:(NSArray*)attrNames];
			CFRelease(attrNames);
			
		}
		
		CFRelease(item);
	}
	return [returnArray autorelease];

}


-(NSAttributedString*)loadTextAt:(NSString*)path
{
	//check if path is directory
	NSFileManager* fm = [NSFileManager defaultManager];
	BOOL directory;
	
	[fm fileExistsAtPath: path isDirectory: &directory];
	if( directory == YES )
	{
		NSWorkspace* ws = [NSWorkspace sharedWorkspace];
		if( ! [ws isFilePackageAtPath:path] )
		return  [[[NSAttributedString alloc] initWithString:@""] autorelease];
	}
	
	
	path = [path lowercaseString];
	
	NSData* loadTextData = [[[NSData alloc] initWithContentsOfFile:path] autorelease];
	
	NSAttributedString* attr = nil;
	
	
	
	
	
	// Get UTI of the given file
	NSString* uti = [self uti:path];

	
	
	
	////NSLog(@"%@",path);
	if( [uti isEqualToString:@"com.apple.rtfd"] )
	{
		//NSString* aStr = [wrapper filename];
		
		NSFileWrapper* filewrapper = [[[NSFileWrapper alloc ] initWithPath: path] autorelease];
		attr = [[[NSAttributedString alloc] initWithRTFD:[filewrapper serializedRepresentation] documentAttributes:nil] autorelease];
	}
	
	
	else if([uti isEqualToString:@"public.rtf"])
	{
		
		
		attr = [[[NSAttributedString alloc] initWithRTF:loadTextData documentAttributes:nil] autorelease];
	}
	
	else if([uti isEqualToString:@"com.microsoft.word.doc"])
	{
		
		
		attr = [[[NSAttributedString alloc] initWithDocFormat:loadTextData documentAttributes:nil] autorelease];
	} 
	
	else if([uti isEqualToString:@"public.html"]  )
	{
		
		attr = [[[NSAttributedString alloc] initWithHTML:loadTextData documentAttributes:nil] autorelease];
	}
	
	
	else if( 0/*[uti isEqualToString:@"com.adobe.pdf"]*/ ) 
	{
		PDFDocument* pdfdoc = [[[PDFDocument alloc] initWithData:loadTextData] autorelease];
		
		
		attr = [[[NSAttributedString alloc] initWithString:[pdfdoc string]] autorelease];
		
	}

	
	
	else if( 0/*[uti isEqualToString:@"public.plain-text"]*/ ) 
	{
				
	/*
		// Open File
		NSData* data = [NSData dataWithContentsOfFile: (NSString*) path];
		if ( data) {
		
			// Detect Encoding
			NSStringEncoding encoding;
			encoding = [JapaneseString detectEncoding: data];
			
			// Convert Encoding
			NSString* contents;
			if (encoding == NSUnicodeStringEncoding ||
				encoding == CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF16BE) ||
				encoding == CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF16LE)) {
				contents = [[[NSString alloc] initWithData: data
												  encoding: encoding] autorelease];
			} else {
				TECConverter* converter = [self createConverter:encoding];
				contents = [converter convertToString: data];
			}
			
			if( contents )
			{
				
				attr = [[[NSAttributedString alloc] initWithString:contents] autorelease];
				
				
			}
			
		}
		*/
	}
		
		 
	if( attr == nil ) //use mdimporter
	{
		id textContent = [SISpotlightTextContentRetriever textContentOfFileAtPath: path ];
		
		if( textContent != nil )
			attr = [[[NSAttributedString alloc] initWithString:textContent] autorelease];
		
	}
	
	//----
	
	if( attr == nil )
		attr = [[[NSAttributedString alloc] initWithString:@""] autorelease];

	
	
	return attr;
}

-(void)highlight:(NSString*)key 
{	
	if( [[[windowUIObjectController selection] valueForKey:@"selectedTabIndex"] intValue] == 0 )
	{
//TEXT VIEW
		
		NSRange range = {0,0};
		NSRange firstRange = {0,0};
		int v_count = 0;
		
		NSRange searchRange = NSMakeRange(0, [[[textView textStorage] string] length]);
		
		[textView setSelectedRange:NSMakeRange(0,0)];
		
		
		while( 1 )
		{
			
			range = [[[textView textStorage] string] rangeOfString:key 
														   options: NSCaseInsensitiveSearch
															 range:searchRange];
			
			if( range.location != NSNotFound )
			{
					
					/*
				[[textView layoutManager] addTemporaryAttributes:[NSDictionary dictionaryWithObjectsAndKeys:highlightColor,
					@"TEMPORARY_HIGHLIGHT_ATTRIBUTENAME",nil]
											   forCharacterRange:range];
				*/
				
				
				
				if( firstRange.length == 0 ) firstRange = range;
				
				v_count++;
				
				
				
				searchRange.location = NSMaxRange(range);
				searchRange.length = [[[textView textStorage] string] length] - searchRange.location;
				
				
				if( searchRange.location >= [[[textView textStorage] string] length] || searchRange.length <= 0 ) break;

			}else 
			{
				break;
			}
		}
		
		[textView setSelectedRange: firstRange ];
		[textView scrollRangeToVisible:firstRange];
		
		[[windowUIObjectController selection] setValue:[NSString stringWithFormat:NSLocalizedString(@"%d matches",@"") ,v_count]
									forKey:@"matchCount"];
		//
		NSActionCell* cell = [[NSActionCell alloc] init];
		
		[cell setTag: NSFindPanelActionSetFindString];
		[textView performFindPanelAction:cell];
		
		[cell release];
	 
	}
	else
		
//  PDF
	{
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		
		BOOL focusOption = [ud boolForKey:@"focusPreview"];

		
		if( focusOption )
			[[self windowForSheet] makeFirstResponder:pdfView];

		PDFSelection* result =	[[pdfView document] findString:key fromSelection:[pdfView currentSelection] withOptions:NSCaseInsensitiveSearch];
		
		[pdfView setCurrentSelection:result];
		[pdfView display];
		[pdfView scrollSelectionToVisible:self];

		[[windowUIObjectController selection] setValue:@""
									forKey:@"matchCount"];
		

	}
}



/////
-(IBAction)slider:(id)sender
{
	int tag = [sender tag];
	
	if( tag != 0 )
	{
		[slider setFloatValue: [slider floatValue] + (float)tag/2.0 ];
	}

	NSSize 	originalSize = [textView frame].size;
	float zoom = [slider floatValue];
	NSSize  newSize;
	newSize.width = originalSize.width / powf( 2, zoom -5 );
	newSize.height = originalSize.height / powf( 2, zoom -5 );	
	
	NSScrollView* scrollView = [textView enclosingScrollView];
	
	//before zooming, retrieve the top most character
	NSRange glyhRange = [[textView layoutManager] glyphRangeForBoundingRect:[scrollView documentVisibleRect] 
															inTextContainer: [textView textContainer]];
	NSRange charRange = [[textView layoutManager] characterRangeForGlyphRange:glyhRange actualGlyphRange:NULL];
	
	
	//zoom
	[textView  setBoundsSize:newSize];
	[scrollView setDocumentView:textView];
	
	// scroll to visible the character retrieved before
	[textView scrollRangeToVisible:[textView selectedRange]];

	
	[pdfView setScaleFactor:powf( 2, zoom -5 )];
	
}

-(IBAction)website:(id)sender
{
	NSURL* url = [NSURL URLWithString:@"http://www.oneriver.jp/"];
	NSWorkspace* ws = [NSWorkspace sharedWorkspace];
	[ws openURL:url];
}

-(IBAction)showHideQueryDetails:(id)sender
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	BOOL flag = [ud boolForKey:@"disclosureButton"];
	[ud setBool: !flag forKey:@"disclosureButton"];
	
}



-(IBAction)showHideSource:(id)sender
{
	RBSplitSubview* view = [mainSplitView subviewAtPosition:0];
	if( [view isCollapsed]   ) [view expand];
	else [view collapse];
}
-(IBAction)focusField:(id)sender
{
	//search computer index
	
	int homePosition = [[NSUserDefaults standardUserDefaults] integerForKey:@"homePosition"];
	
	unsigned index;
	
	if( homePosition == 0 ) {
		index = 0;
	}
	else
	{
		NSArray* dataArray = [[NSApp delegate] organiserArray];
		for( index = 0; index < [dataArray count]; index ++ )
		{
		if( [[dataArray objectAtIndex:index] isKindOfClass:[DefaultListItem class]] )
			break;
			
		}
	}
	
	NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:index];
	[outlineView selectRowIndexes:indexes byExtendingSelection:NO];
	
	[[table window] makeFirstResponder:field ];
}

- (IBAction)actionButtonPressed:(id)sender
{
	[NSMenu popUpContextMenu:[sender menu] withEvent:[[self windowForSheet] currentEvent ] forView:sender];
}

- (IBAction)save:(id)sender
{
	if ([[table window] makeFirstResponder:[table window]]) {
		/* All fields are now valid; it’s safe to use fieldEditor:forObject:
        to claim the field editor. */
	}
	else {
		/* Force first responder to resign. */
		[[table window] endEditingFor:nil];
	}
	
	
	
	//queryTemplate, scope, query
	NSString* name;
	
	if( _searchKey != nil && ![_searchKey isEqualToString:@""] ) name = _searchKey;
	else name = NSLocalizedString(@"Untitled Search",@"");
	
	SearchListItem *item = [[[SearchListItem alloc] init] autorelease];
	[item setScope:[[windowUIObjectController selection] valueForKey:@"scope"]];
	[item setQuery:[[windowUIObjectController selection] valueForKey:@"query"]];
	[item setQueryTemplate: [[windowUIObjectController selection] valueForKey:@"queryTemplate"] ];
	[item setName:name];

	[[[NSApp delegate] organiserArray] addObject: item];
	[outlineView reloadData];
}
/*
-(void)setQueryTemplate:(id)value
{
	if( value == nil )
	{
		value = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultQueryTemplate"];

		
	}

	[queryTemplate release];
	queryTemplate = [value retain];
	
	
}


-(void)setQuery:(id)value
{
	[query release];
	query = [value retain];
}
*/
-(IBAction)shiborikomi:(id)sender
{
	NSArray* allKeys = [[NSApp delegate] allKeys];
	
	NSString* string = [shiborikomiField stringValue];
	
	if( [string isEqualToString:@"" ] )
	{
		[findResult setFilterPredicate:nil];
		return;
	}
	BOOL regex = ([shiborikomiRegex state] == NSOnState? YES:NO);
	

	NSString* predicateString;
			
			if( regex )
			{
				predicateString = @" (kMDItemPath matches %@) " ;
			}else
			{
				predicateString =  @" (kMDItemPath like[c] %@) " ;
				string = [NSString stringWithFormat:@"*%@*",string];
			}
					
			NSPredicate* predicate = [NSPredicate predicateWithFormat:predicateString,  string];


			[findResult setFilterPredicate:predicate];
			[findResult rearrangeObjects];
			[flowView reloadData];

			return;
}


-(NSOutlineView*)outlineView
{
	return outlineView;	
}
#pragma mark Meta

-(IBAction)showViewOption:(id)sender
{
	
	//metalist
	
	[metaList setContent:[[NSApp delegate] metadataList]];
	[metaList rearrangeObjects];
	

		

	
	//check and uncheck
	
	int hoge;
	for( hoge = 0; hoge < [[metaList content] count]; hoge++ )
	{
		NSMutableDictionary* dic = [[metaList content] objectAtIndex:hoge];
		NSString* attr = [dic objectForKey:@"attribute"];
		
		if( [[[NSApp delegate] allKeys] containsObject:attr] ) [dic setObject:[NSNumber numberWithBool:YES ] forKey:@"enable"];
		else	[dic setObject:[NSNumber numberWithBool:NO ] forKey:@"enable"];
		
	}
	
	
	//[metaList rearrangeObjects];
	[NSApp beginSheet:metaListWindow modalForWindow:[self windowForSheet]
		modalDelegate:self didEndSelector:@selector(sheetDidEnd:returnCode:contextInfo:) contextInfo:nil];
	
}
-(void)metaListClosed:(id)sender
{
	[metaListWindow close];
	[NSApp endSheet:metaListWindow returnCode:[sender tag] ];
	
	
	int hoge;
	for( hoge = 0; hoge < [[metaList content] count]; hoge++ )
	{
		NSDictionary* dic = [[metaList content] objectAtIndex:hoge];
		NSString* attr = [dic objectForKey:@"attribute"];
		BOOL enable =  [[dic objectForKey:@"enable"] boolValue];
		
		if( enable == YES )
		{
			if( ! [[[NSApp delegate] allKeys] containsObject:attr] )
			{


				
				[table addColumnWithIdentifier:attr title:nil bindToObject:findResult ];
				[[[NSApp delegate] allKeys] addObject:attr];
				
			}
		}else //disable
		{
			if(  [[[NSApp delegate] allKeys] containsObject:attr] )
			{
				[table removeColumnWithIdentifier:attr];
				[[[NSApp delegate] allKeys] removeObject:attr];
			}
			
		}
	}
	

}

- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)item;
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SourceReselectRequestNotification"
														object:nil];	

	
}

- (IBAction)metaListFilter:(id)sender
{
	
	
	NSString* searchString = [sender stringValue];
	
	
	
	if( [searchString isEqualToString:@"" ] )
	{
		
		[metaList setFilterPredicate:nil];
		[metaList rearrangeObjects];
		
		return;
		
	}
	
	
	
	
	NSPredicate *predicateToRun = nil;
	
	predicateToRun = [NSPredicate predicateWithFormat: @"(attributeDisplayName contains[wc] %@)|| (attribute contains[wc] %@) || ( attributeDescription contains[wc] %@ )", searchString,searchString,searchString];
	
	[metaList setFilterPredicate:nil];
	[metaList setFilterPredicate:predicateToRun];
	[metaList rearrangeObjects];
	return;
	
}


- (void)tellMeTargetToFindIn:(id)textFinder
{
	if( [NSApp keyWindow] == infoPanel )
		[textFinder setTargetToFindIn:[infoPanel firstResponder]];
	else
	[textFinder setTargetToFindIn:textView];
}


-(void)quickLook:(id)sender
{
	[table showQuickLookPanelFromResults:nil];

}
#pragma mark Table Delegate


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{

	
	if( [keyPath isEqualToString:@"tagPaneVisible"] )
	{
		float dim;
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		
		if( [ud boolForKey:@"tagPaneVisible" ] )
		{
			[tagPane expandWithAnimation];
			[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];
			
		}
		
		else{
			[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];
			
			[tagPane collapseWithAnimation];
		}
		
		[ud synchronize];
	}

	if( [keyPath isEqualToString:@"flowViewVisible"] )
	{
		float dim;
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		
		if( [ud boolForKey:@"flowViewVisible" ] )
		{
			//[flowPane expandWithAnimation];
			[ud setFloat:[tagPane dimension] forKey:@"flowPaneDimension"];
			
			[self setupFlowView];
			
		}
		
		else{
			[ud setFloat:[tagPane dimension] forKey:@"flowPaneDimension"];
			[[flowView window] orderOut:nil];

			//[flowPane collapseWithAnimation];
		}
		
		[ud synchronize];
	}
	
	if( [keyPath isEqualToString:@"disclosureButton"] )
	{

		//NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		int state = [[object valueForKey:@"disclosureButton"] intValue];
		
		if( state == NSOnState )
		{
			NSSize size = [mainSplitView frame].size;
			size.height -= [advancedBox frame].size.height;
			[advancedBox setHidden:NO];
			[mainSplitView setFrameSize: size];
			
		}else
		{
			NSSize size = [mainSplitView frame].size;
			size.height += [advancedBox frame].size.height;

			[advancedBox setHidden:YES];
			[mainSplitView setFrameSize: size];

		}
		[[mainSplitView window] display];
	
	}
}


- (NSTimeInterval)splitView:(RBSplitView*)sender willAnimateSubview:(RBSplitSubview*)subview withDimension:(float)dimension 
{
	if( sender == secondSplitView )
	{
		[flowView setHidden:YES];
	}
	return 0;
}

- (void)splitView:(RBSplitView*)sender wasResizedFrom:(float)oldDimension to:(float)newDimension 
{
	if( sender == secondSplitView )
	{
		//NSLog(@"to %f",newDimension);
		
		if( newDimension ==0 )
		{
			
		}
	}
	
	if( sender == outlineSplitView )
		[sender adjustSubviewsExcepting:[sender subviewAtPosition:1]];

		else
	[sender adjustSubviewsExcepting:[sender subviewAtPosition:0]];
}

- (unsigned int)splitView:(RBSplitView*)sender dividerForPoint:(NSPoint)point inSubview:(RBSplitSubview*)subview {
	if ( sender == mainSplitView && [knob mouse:[knob convertPoint:point fromView:sender] inRect:[knob bounds]]) 
		return 0;
	
	if( subview == tagPane && [tagKnob mouse:[tagKnob convertPoint:point fromView:sender] inRect:[tagKnob bounds]])
		return 0;

	return NSNotFound;
}

-(BOOL)isDocumentEdited{return NO;}



- (void)tableViewSelectionDidChange:(NSNotification *)aNotification
{
	NSDictionary* dic;
	//NSLog(@"here");
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	int selectionCount = [[findResult selectionIndexes] count];
	
	if( selectionCount >1 )
	{
		[[windowUIObjectController selection] setValue:[NSNumber numberWithInt:3] forKey:@"selectedTabIndex"];
		return;

	}
	
	
	int row = [findResult selectionIndex];
	if( row != NSNotFound )
	{
		[flowView selectCoverAtIndex:row];
		
		[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:YES] forKey:@"isSearching"];
		
		
		dic = [[findResult selectedObjects] lastObject];
		
		////NSLog(@"here");
		
		NSString* path = [dic objectForKey: kMDItemPath ];// MDItemCopyAttribute (mdref, kMDItemTextContent );
		[self displayWithFile:path];
		
		
		
		// add to history
		if( [ud boolForKey:@"historyAddFiles"] )
		{
			id newItem;
			
			if( [[path pathExtension] isEqualToString:@"savedSearch"] )
				newItem = [[[SmartFolderListItem alloc] init] autorelease];
			else
				newItem = [[[AliasListItem alloc] init] autorelease];
			
			[newItem setAliasWithPath: path ];
			[[HistoryListItem sharedHistoryListItem] addHistory:newItem ];
			
		}
		
		
		
		
		NSArray* keywords = [[windowUIObjectController selection] valueForKey:@"searchKeywords"];
		if( [[[windowUIObjectController selection] valueForKey:@"selectedTabIndex"] intValue] == 0 && [keywords count] >0 )
		{
			
			NSMutableString* expressionString = [[[NSMutableString alloc] initWithString:@""] autorelease];
			

			int hoge=0;
			for( hoge = 0; hoge < [keywords count]; hoge++ )
			{
				if( hoge>0 ) [expressionString appendString:@"|"];
				[expressionString appendString: [NSString stringWithFormat:@"(%@)", [OGRegularExpression regularizeString:[keywords objectAtIndex:hoge]] ]];

			}

			
			int v_count = 0;

			
			if( [[[textView textStorage] string] rangeOfRegularExpressionString:expressionString options:OgreIgnoreCaseOption].length != 0 && [[NSUserDefaults standardUserDefaults] boolForKey:@"commandG"] )
			{
				
				/*OgreAdvancedFindPanelController*/ 
				[[OgreTextFinder sharedTextFinder] setSyntax:OgreRubySyntax];
				id controller = [[OgreTextFinder sharedTextFinder] findPanelController];
				
				[[controller valueForKey:@"findTextView"] setString: expressionString];
				[controller setValue:[NSNumber numberWithBool: YES] forKey:@"regularExpressionsOption"] ;
				[controller setValue:[NSNumber numberWithBool: YES] forKey:@"ignoreCaseOption"] ;
				
				
				////NSLog(@"highlighting %@",expressionString);
				
				OGRegularExpression* regex = [OGRegularExpression regularExpressionWithString:expressionString options:OgreIgnoreCaseOption];
				
				id	adapter = [[OgreTextFinder sharedTextFinder] adapterForTarget:textView];
				id thread = [[[NSClassFromString(@"OgreHighlightThread") alloc] initWithComponent:adapter] autorelease];
				[thread setRegularExpression:regex];
				[thread setHighlightColor:[NSColor colorWithCalibratedRed:0.2
																	green:0.2
																	 blue:1.0 alpha:1.0]];
				[thread setOptions:OgreIgnoreCaseOption];
				[thread setInSelection:NO];
				//[thread setDidEndSelector:@selector(didEndThread:) toTarget:self];
				//[thread setProgressDelegate:sheet];
				[thread setAsynchronous:NO];
				
				[thread detach];



				[controller findNext:self];
				v_count = [thread numberOfMatches];

			}
			// 
				
			[[windowUIObjectController selection] setValue:[NSString stringWithFormat:NSLocalizedString(@"%d matches",@"") ,v_count]
										forKey:@"matchCount"];

			
			//
			//[self highlight:_searchKey ];

		}
		else
			[self highlight:_searchKey ];

			
		[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:NO] forKey:@"isSearching"];

		
		[self setInfoView];

			
	}else
	{
		[textView setString:@""];
	}
	
	
}

/*
- (BOOL)tableView:(NSTableView *)aTableView writeRows:(NSArray *)rows toPasteboard:(NSPasteboard *)pboard
{
	[pboard declareTypes:FoundItemTypes owner:self];
	
	NSMutableArray* pbDataArray = [NSMutableArray array];
	int hoge;
	for( hoge = 0; hoge < [rows count]; hoge++ )
	{
		NSDictionary* dic = [[findResult arrangedObjects] objectAtIndex:[[rows objectAtIndex:hoge] intValue]];
		[pbDataArray addObject:dic];
		
	}
	
	NSData* pbData = [NSKeyedArchiver archivedDataWithRootObject: pbDataArray ];
	
	[pboard setData:pbData forType:FoundItem];
	
	return YES;
}*/

- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
	return;
	
	
	
	
	// attribute name [aTableColumn identifier]
	// path  [[[findResult arrangedObjects] objectAtIndex:rowIndex] objectForKey:kMDItemPath ]
	id originalObject = [[[findResult arrangedObjects] objectAtIndex:rowIndex] objectForKey:kMDItemPath ];
	
	id item = [[findResult arrangedObjects] objectAtIndex:rowIndex];
	

	////NSLog(@"Changed %@",[item objectForKey:kMDItemPath ]);
	
	NSString* path = [item objectForKey:kMDItemPath ];
	NSString* contentType = [item objectForKey:kMDItemContentType ];
	NSString* identifier = [aTableColumn identifier];
	
	NSFileManager* fm = [NSFileManager defaultManager];
	
	if( [fm fileExistsAtPath:path] == YES )
	{
	
		
		if( [contentType isEqualToString:@"public.rtf"] )
		{
		
			NSDictionary* dic;
			
			NSAttributedString* attr = [[[NSAttributedString alloc] initWithPath:path
															 documentAttributes:&dic] autorelease];
			
			NSMutableDictionary* mdic = [NSMutableDictionary dictionaryWithDictionary:dic];
			
			if( [identifier isEqualToString:kMDItemComment] )
				[mdic setObject:anObject forKey:NSCommentDocumentAttribute];
			
			
			
			NSData* rtfData = [attr RTFFromRange:NSMakeRange(0,[attr length]) documentAttributes:mdic];
			[rtfData writeToFile:path atomically:YES];
			
		}else if(  [contentType isEqualToString:@"com.apple.rtfd"] )
		{
			NSDictionary* dic;

			NSFileWrapper* filewrapper = [[[NSFileWrapper alloc ] initWithPath: path] autorelease];
			NSAttributedString* attr = [[[NSAttributedString alloc] initWithRTFD:[filewrapper serializedRepresentation] documentAttributes:&dic] autorelease];
			
			NSMutableDictionary* mdic = [NSMutableDictionary dictionaryWithDictionary:dic];
			
			if( [identifier isEqualToString:kMDItemComment] )
				[mdic setObject:anObject forKey:NSCommentDocumentAttribute];
			
			
			filewrapper = [attr RTFDFileWrapperFromRange:NSMakeRange(0,[attr length])
						documentAttributes:mdic ];
			
			
			[filewrapper writeToFile: path
									   atomically:YES updateFilenames:YES]; 
			
			
		}else if( [contentType isEqualToString:@"com.pukeko.edgies.edgy"] )
		{
			NSData* readDocData =[[[NSData alloc] initWithContentsOfFile:path] autorelease];
			id dic = [NSKeyedUnarchiver unarchiveObjectWithData:readDocData];

			NSMutableDictionary* mdic = [NSMutableDictionary dictionaryWithDictionary:dic];
			
			if( [identifier isEqualToString:kMDItemComment] )
				[mdic setObject:anObject forKey:@"p_comment"];
			
			readDocData = [NSKeyedArchiver archivedDataWithRootObject:mdic];
			
				[readDocData writeToFile:path atomically:YES];
		}
		
	}

}
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex
{
	
	
	return NO;
	

}

- (NSString *)tableView:(NSTableView *)aTableView toolTipForCell:(NSCell *)aCell rect:(NSRectPointer)rect tableColumn:(NSTableColumn *)aTableColumn row:(int)row mouseLocation:(NSPoint)mouseLocation;
{
	if( [[NSApp delegate] OSVersion] >= 105 )
	{
		//NSLog(@"%d",MAC_OS_X_VERSION_10_5 );
		return nil;
	}
	
	
		id item = [[findResult arrangedObjects] objectAtIndex:row];
	id value = [item objectForKey: [aTableColumn identifier]];
	
	if( [value isKindOfClass:[NSString class]] )	return value;
	if( [value isKindOfClass:[NSNumber class]] )	return [value stringValue];
	if( [value isKindOfClass:[NSDate class]] ){
		NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
		
		[formatter setFormatterBehavior:NSDateFormatterBehavior10_4];
		[formatter setDateStyle:kCFDateFormatterLongStyle];
		[formatter setTimeStyle:kCFDateFormatterShortStyle];
		
		return [formatter stringFromDate:value];
	}
	if( [value isKindOfClass:[NSArray class]] && [value count]>0){
	
		
		if( [[value objectAtIndex:0] isKindOfClass:[NSString class]] )
		{
			
			return [value componentsJoinedByString:@"\n"];
		}
		
		
	}

	//NSBeep();
	return nil;


}
#pragma mark FlowView DataSource

-(NSString*)titleForCoverAtIndex:(unsigned)index
{
	id item = [[findResult arrangedObjects] objectAtIndex:index];
	
	
	////NSLog(@"Changed %@",[item objectForKey:kMDItemPath ]);
	
	NSString* path = [item objectForKey:kMDItemPath ];
	return [path lastPathComponent];	
}
-(NSString*)subtitleForCoverAtIndex:(unsigned)index
{
	id item = [[findResult arrangedObjects] objectAtIndex:index];
	
	
	////NSLog(@"Changed %@",[item objectForKey:kMDItemPath ]);
	
	NSString* path = [item objectForKey:kMDItemPath ];
	return path;	
	
}
-(NSImage*)imageForCoverAtIndexImmedeately:(unsigned)index
{
	return [self imageForCoverAtIndex:index];
}


-(NSImage*)imageForCoverAtIndex:(unsigned)index
{
	
	id item = [[findResult arrangedObjects] objectAtIndex:index];	
	NSString* path = [item objectForKey:kMDItemPath ];
	
	NSImage* icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
	return icon;
}//previous frame identical to this frame corrupt stack?

-(unsigned)numberOfCovers
{
//	NSLog(@"numberOfCovers count %d",[[findResult arrangedObjects] count]);
	return [[findResult arrangedObjects] count];
}

-(void)flowView:(FlowView*)aFlowView didSelectCoverAtIndex:(unsigned)index
{
	return;
	
	
	NSIndexSet* indexes = [table selectedRowIndexes];
	if( ! [indexes containsIndex: index] )
	{
			
		 indexes = [NSIndexSet indexSetWithIndex:index];
		[table selectRowIndexes:indexes byExtendingSelection:NO];
	}
}
-(void)flowView:(FlowView*)aFlowView doubleClickedCoverAtIndex:(unsigned)index;
{
	NSIndexSet* indexes = [table selectedRowIndexes];
	if( ! [indexes containsIndex: index] )
	{
		
		indexes = [NSIndexSet indexSetWithIndex:index];
		[table selectRowIndexes:indexes byExtendingSelection:NO];
	}	
}

-(void)setupFlowView
{
	if( flowView ==nil )
	{
		NSView* subview = [mainSplitView subviewAtPosition:0];

	

	
	
	flowView = [[FlowView alloc] initWithFrame:[flowViewTemplate frame] ];
	[flowView _initCameraAndGL];
	[flowView awakeFromNib];
	
	[flowView setFrameSize:[flowViewTemplate frame].size ];
	[flowView setFrameOrigin:NSZeroPoint];
	[flowView setAutoresizingMask: NSViewHeightSizable|NSViewWidthSizable ];
	//[subview addSubview:flowView];
	[flowView setDataSource:self];
	[flowView setDelegate:self];

	[[[flowViewTemplate window] contentView] addSubview: flowView];
	[flowViewTemplate removeFromSuperview];

				[[flowView window] makeKeyAndOrderFront:nil];

	//[secondSplitView adjustSubviews];
	}
}

-(IBAction)openFlowView:(id)sender
{
	[self setupFlowView];
	[[flowView window] makeKeyAndOrderFront:nil];

}

-(void)tableViewDidReloadData:(NSTableView*)aTable
{
	
	if( flowView )
	[flowView reloadData];
}
#pragma mark etc
- (void)windowDidResignKey:(NSNotification *)notification

{
	[outlineView setBackgroundColor: [NSColor controlHighlightColor] ];

}
- (void)windowDidBecomeKey:(NSNotification *)notification
{
	
	[outlineView setBackgroundColor: [NSColor colorWithCalibratedRed:211.0/255.0 green:221.0/255.0 blue:232.0/255.0 alpha:1.0] ];
}

-(NSArray*)wordsFor:(NSString*)string
{
	NSTextView* tv = [[NSTextView alloc] initWithFrame:NSMakeRect(0,0,100,100)];
		
	[tv insertText: string];
	
	NSMutableSet* words = [[[NSMutableSet alloc] init] autorelease];
	unsigned hoge;
	NSRange range = NSMakeRange(0, [string length] );
	
	for( hoge = range.location; hoge < NSMaxRange(range); )
	{
		NSRange wordRange =
		[tv selectionRangeForProposedRange:NSMakeRange(hoge,1) granularity:NSSelectByWord];
		
		NSString* oneWord = [[[tv textStorage] string] substringWithRange:wordRange];
		
		if( ![oneWord isEqualToString:@" "] && !([oneWord length] == 1 && [oneWord characterAtIndex:0] == 0x3000) )
		[words addObject:oneWord];
		
		hoge = hoge + wordRange.length;
	}
		
	
	[tv release];
//	//NSLog(@"words %@",[words allObjects]);
	
	return [words allObjects];
}



-(IBAction)findNext:(id)sender
{
	
	int findNextOption = [[NSUserDefaults standardUserDefaults] integerForKey:@"findNextWordOrCompleteMatch"];
	

	if( [[[windowUIObjectController selection] valueForKey:@"selectedTabIndex"] intValue] == 0  )
	{
		if( _searchKey && ! [_searchKey isEqualToString:@""] )
		{
			id controller = [[OgreTextFinder sharedTextFinder] findPanelController];
			
			if( [sender tag] == NSFindPanelActionNext )
					[controller findNext:self];
			else
				[controller findPrevious:self];
		}

		//[textView performFindPanelAction:sender];
	}
	else
	{
		int option;
		if( [sender tag] == NSFindPanelActionNext )
			option = NSCaseInsensitiveSearch;
		else
			option = NSCaseInsensitiveSearch | NSBackwardsSearch ;
			
		if( _searchKey && ! [_searchKey isEqualToString:@""] )
		{
			PDFSelection* result =	[[pdfView document] findString:_searchKey fromSelection:[pdfView currentSelection] withOptions:option];
			if( result != nil )
			{
				[pdfView setCurrentSelection:result];
				[pdfView scrollSelectionToVisible:self];
			}else
			{
				NSBeep();
			}
			

		}
	}
	
	
}

-(IBAction)resetSorting:(id)sender
{
	[findResult setSortDescriptors:nil];
	
}


-(IBAction)showInfoView:(id)sender
{
	if( [infoPanel isVisible] )
		[infoPanel orderOut:self];
	else
	//MDSchemaCopyDisplayNameForAttribute
	[infoPanel makeKeyAndOrderFront:self];
	[self setInfoView];
}
-(void)setInfoView
{
	if( ![infoPanel isVisible] ) return;

	int row = [findResult selectionIndex];
	if( row != NSNotFound )
	{
		CFTypeRef typeRef;
		NSMutableArray* attrs = [NSMutableArray array];
		NSMutableDictionary* attrDic = [NSMutableDictionary dictionary];
		
		
		//infoAttributedString
		NSMutableAttributedString* attr = [[[NSMutableAttributedString alloc] init] autorelease];
		
		
		NSDictionary* dic = [[findResult selectedObjects] lastObject];
		NSString* path = [dic objectForKey: kMDItemPath ];
		
		MDItemRef mditem = MDItemCreate( kCFAllocatorDefault, path);
		
		typeRef = MDItemCopyAttributeNames(mditem);
		if( typeRef != nil ) {
			[attrs addObjectsFromArray: typeRef];
			CFRelease(typeRef);
		}
		
		typeRef = MDItemCopyAttributes(mditem,attrs);
		if( typeRef != nil ) {
			[attrDic addEntriesFromDictionary: typeRef];
			CFRelease(typeRef);
		}

		
		
		NSDictionary* titleAttr = [NSDictionary dictionaryWithObject:[NSFont boldSystemFontOfSize:12] forKey:NSFontAttributeName];
		NSDictionary* attrNameAttr = [NSDictionary dictionaryWithObjectsAndKeys: [NSFont boldSystemFontOfSize:12] , NSFontAttributeName, [NSColor lightGrayColor], NSForegroundColorAttributeName,nil];

		NSDictionary* textAttr = [NSDictionary dictionaryWithObject:[NSFont userFontOfSize:12] forKey:NSFontAttributeName];
		

		//first 
		NSMutableArray* allKeys = [NSMutableArray arrayWithArray: [[NSApp delegate] allKeys]];

		[allKeys removeObject:kMDQueryResultContentRelevance];
		
		[attrs removeObjectsInArray: allKeys];
		[allKeys addObjectsFromArray: attrs];

		attrs = allKeys;
		
		
		
		NSDictionary* attrDic2 = nil;
		
		
		typeRef = MDItemCopyAttributes(mditem,allKeys);
		if( typeRef != nil ) {
			attrDic2 = [NSDictionary dictionaryWithDictionary: typeRef];
			CFRelease(typeRef);
		}
		
		

		[attrDic addEntriesFromDictionary:attrDic2];
	
		//NSLog(@"attrDic %@",[attrDic description]);
		
		// second
		int hoge;
		for(hoge= 0;hoge< [attrs count] ; hoge++ )
		{
			
			NSString* key = [attrs objectAtIndex:hoge];
			id value = nil;
			//tagbot
			
			if( [key isEqualToString:@"TagBotTagColumn.SpotInsideInternalAttribute"]  )
			{
				typeRef = MDItemCopyAttribute(mditem,kMDItemFinderComment);
				if( typeRef != nil ) {
					value = [[NSValueTransformer valueTransformerForName:@"TagBotTagsValueTransformer" ]
						transformedValue:typeRef];

					CFRelease(typeRef);
				}
			}
			else
			{
				value = [attrDic objectForKey:key];
			}
			
			NSString* content = @"";
			
			if( value != nil )
			{
				if( [value isKindOfClass:[NSArray class]] )
				{
					content = [value componentsJoinedByString:@"\n"];	
				}else  if( CFGetTypeID(value) == CFBooleanGetTypeID() )
				{
					if( [value boolValue] ) content = NSLocalizedString(@"YES",@""); else content = NSLocalizedString(@"NO",@"");
				}else
				{
					content = [NSString stringWithFormat:@"%@", value];	
				}
			
			
				CFStringRef keyToDisplay = MDSchemaCopyDisplayNameForAttribute(key);
				
				if( keyToDisplay == nil ) {
					
					if( [key isEqualToString:@"TagBotTagColumn.SpotInsideInternalAttribute"] )
						keyToDisplay = NSLocalizedString(@"TagBot Tags",@"");

					else
						keyToDisplay = key;
					
					CFRetain(keyToDisplay);
				}
				
				
				// Add title
				NSString* titleString = [NSString stringWithFormat:@"%@", keyToDisplay];
				if( keyToDisplay != nil ) CFRelease(keyToDisplay);

				
				//NSLog(@"keyToDisplay%@",keyToDisplay);
				NSAttributedString* _title = [[[NSAttributedString alloc] initWithString:titleString attributes:titleAttr] autorelease];
				[attr appendAttributedString: _title];			
				

				//Add Attrname
				
				NSString* attrNameString = [NSString stringWithFormat:@" (%@)", key];
				
				NSAttributedString* _attrName = [[[NSAttributedString alloc] initWithString:attrNameString attributes:attrNameAttr] autorelease];
				[attr appendAttributedString: _attrName];	
				
				
				
				//Add Content
				
				content = [NSString stringWithFormat:@"\n%@\n\n",content ];
				NSAttributedString* _text = [[[NSAttributedString alloc] initWithString:content attributes:textAttr] autorelease];
				
				[attr appendAttributedString: _text];			
			
			}
			
			
		}
		
		
		[[windowUIObjectController selection] setValue:attr forKey:@"infoAttributedString"];
		//[infoView setString: [attrDic description]];
		
		if( mditem != nil ) CFRelease(mditem);
	}
}



- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard*)pboard
{
	////NSLog(@"tableView");
	if( [ rowIndexes count ] == 0 ) return NO;

	NSMutableString* string = [[[NSMutableString alloc] init] autorelease];
	NSMutableArray* dictArray = [[[NSMutableArray alloc] init] autorelease];

	[pboard declareTypes:[NSArray arrayWithObjects: NSStringPboardType,nil ] owner:self];


		
	[table setDraggingSourceOperationMask:NSDragOperationGeneric forLocal:NO];


	

	unsigned hoge;
	for( hoge = 0; hoge < [table numberOfRows] ; hoge++ )
	{
		if( [rowIndexes containsIndex:hoge] )
		{
			NSDictionary* dict = [[findResult arrangedObjects] objectAtIndex: hoge];
			
			if( [string length] >0 ) [string appendString:@"\n"];
			[string  appendString:[dict objectForKey:kMDItemPath]];
			
			[dictArray addObject: [dict objectForKey:kMDItemPath]];
		}
	}
	
	
	[pboard setString:string forType:NSStringPboardType];
	
	[pboard setPropertyList:dictArray forType:NSFilenamesPboardType];
	 
	 
	return YES;
}


- (void)saveAsPDF:(id)sender
{
	[table saveAsPDF:sender];

}

- (void)saveMore:(id)sender // Convert Documents to Text Files
{
	if( convertWindow == nil )
	{
		[NSBundle loadNibNamed:@"MyDocumentAddition" owner:self];	
	}
	
	
	
	// Get save Folder
	
	NSOpenPanel* sp = [NSOpenPanel savePanel];
	[sp setPrompt:NSLocalizedString(@"Choose",@"")];
	[sp setMessage: NSLocalizedString(@"SpotInside will convert documents in the find result table to plain text files. Select destination to save text files.",@"")];
	[sp setCanChooseFiles:NO];
	[sp setCanChooseDirectories:YES];
	[sp setCanCreateDirectories:YES];

	int result = [sp runModal];
	
	if( result != NSFileHandlingPanelOKButton ) return;// END when canceled

		
	NSString *dest  = [sp filename];
	
	
	/// Prepare UI
	[convertBar setDoubleValue: 0 ];

	
	[NSApp beginSheet:convertWindow modalForWindow:[self windowForSheet] modalDelegate:nil didEndSelector:nil contextInfo:nil];
	[convertBar setUsesThreadedAnimation:YES]; 
	[convertBar setIndeterminate:NO];

		
	//
	//NSIndexSet* set = [table selectedRowIndexes];
	NSArray* objects = [[[table delegate] findResult] selectedObjects];
	[convertStatus setStringValue: [NSString stringWithFormat:@"0/%d", [objects count] ]];

	
	int hoge;
	for( hoge = 0; hoge < [objects count]; hoge++ )
	{
		[convertBar setDoubleValue: (double)hoge+1 / (double)[objects count] ];
		[convertStatus setStringValue: [NSString stringWithFormat:@"%d/%d",hoge+1, [objects count] ]];
		[convertWindow display];
		NSString* path = [[objects objectAtIndex: hoge] valueForKey:kMDItemPath];
		
		NSString* text = [[self loadTextAt:path] string];
		NSString* filename = [path lastPathComponent];
		filename = [filename stringByAppendingString:@".txt"];
		filename = [filename uniqueFilenameForFolder:dest];
		
		
		NSString* fullpath = [dest stringByAppendingPathComponent: filename]; 
		
		[text writeToFile:fullpath atomically:NO encoding:NSUTF8StringEncoding error:nil];
		
	}
	
	[NSApp endSheet:convertWindow];
	[convertWindow close];
	
	
}
-(NSString*)addTagQuery:(NSString*)oldQuery
{
	NSArray* tagStrings = [TagAttachmentTextView selectedTagsInAtributedString: [tagTextView textStorage] ];
	
	if( tagStrings == nil || [ tagStrings count] == 0 ) return oldQuery;
	
	
	
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSString* prefix = [ud objectForKey:@"tagPrefix"];
	NSString* suffix = [ud objectForKey:@"tagSuffix"];
	
	NSMutableString* query = [[[NSMutableString alloc] init] autorelease];
	
	NSString* template = @"(kMDItemFinderComment == '%@%@%@*'cd)"; // '*&amp;test*'cd
	
	int hoge; for( hoge = 0; hoge < [tagStrings count] ; hoge++ )
	{
		if( hoge > 0 )
		{
			if( [[[windowUIObjectController selection] valueForKey:@"tagAND"] boolValue]  )
				
			[query appendString:@"&&"];
			else
				[query appendString:@"||"];

		}
		
		NSString* aTag = [tagStrings objectAtIndex: hoge];
		[query appendFormat: template, prefix, aTag, suffix];
	}
	
	//NSLog(@"tag query %@",query);
	
	if( oldQuery == nil || [oldQuery isEqualToString:@""] ) return query;
	
	return [NSString stringWithFormat:@"%@ && (%@) ",oldQuery, query];
	
}

-(IBAction)tagBoolean:(id)sender
{
	int tag = [ sender state];
	//NSLog(@"%d tag", tag);
		
	[self tagChanged];
}

-(void)tagChanged
{
	[self search:nil];
}
-(IBAction)toggleTagPane:(id)sender
{
	float dim;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	if( [tagPane isCollapsed] ) {
		[tagPane expandWithAnimation];
		//[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];

	}
	
	else {
		//[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];

		[tagPane collapseWithAnimation];
	}
	
	
}

-(IBAction)toggleFlowPane:(id)sender
{
	float dim;
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];

	if( [flowPane isCollapsed] ) {
		[flowPane expandWithAnimation];
		//[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];

	}
	
	else {
		//[ud setFloat:[tagPane dimension] forKey:@"tagPaneDimension"];

		[flowPane collapseWithAnimation];
	}
	
	
}

- (void)splitView:(RBSplitView*)sender didCollapse:(RBSplitSubview*)subview 
{
	if( [ [subview identifier]	isEqualToString:@"tagPane"] )
	{
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"tagPaneVisible"];
	}
	
	
	if( [ [subview identifier]	isEqualToString:@"flowPane"] )
	{
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"flowViewVisible"];
	}
}
- (void)splitView:(RBSplitView*)sender didExpand:(RBSplitSubview*)subview 
{
	if( [ [subview identifier]	isEqualToString:@"flowPane"] )
	{
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"flowViewVisible"];
	}
}

@end

