//
//  ListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "ListItem.h"
#import <OgreKit/OgreKit.h>

@implementation ListItem

- (id) init {
	self = [super init];
	if (self != nil) {
		name = nil;
		canDelete = YES;
		canMove = YES;
		canDrop = NO;
		canSelect = YES;
		canExpand = NO;
		isExpanded = NO;
		query = nil;
		dragging = NO;
		scope = [[NSArray arrayWithObject:kMDQueryScopeComputer] retain];
		
		icon = nil;
		

		
		searchAgent = [[SearchAgent alloc] initWithEndTarget:self
												 andSelector:@selector(finish:)
														keys:[[NSApp delegate] allKeys ]];
		contents = nil;
	}
	return self;
}


-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];

	[searchAgent end];

	[name release];	
	[query release];	
	[scope release];
	[icon release];
	[searchAgent release];

	[super dealloc];
}




- (void)encodeWithCoder:(NSCoder *)encoder
{
	
	
//	 [super encodeWithCoder: encoder];
	 [encoder encodeObject:		name					forKey:@"name"];
	 [encoder encodeBool:	canDelete		forKey:@"canDelete"];
	 [encoder encodeBool:	canMove		forKey:@"canMove"];
	 [encoder encodeBool:	canDrop		forKey:@"canDrop"];
	 [encoder encodeBool:	canSelect		forKey:@"canSelect"];
	 [encoder encodeBool:	canExpand		forKey:@"canExpand"];
	 [encoder encodeBool:	isExpanded		forKey:@"isExpanded"];

	 [encoder encodeObject:		query					forKey:@"query"];
	 [encoder encodeObject:		scope					forKey:@"scope"];

	 


	return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	// self = [super initWithCoder: coder];
	self = [self init]; //Not super
	if (self != nil) {
		
		dragging = NO;
		
		name = [[coder decodeObjectForKey:@"name"] retain];
		canDelete = [coder decodeBoolForKey:@"canDelete"] ;
		canMove = [coder decodeBoolForKey:@"canMove"] ;
		canDrop = [coder decodeBoolForKey:@"canDrop"] ;
		canSelect = [coder decodeBoolForKey:@"canSelect"] ;
		canExpand  = [coder decodeBoolForKey:@"canExpand"] ;
		isExpanded  = [coder decodeBoolForKey:@"isExpanded"] ;
		query = [[coder decodeObjectForKey:@"query"] retain];
		scope = [[coder decodeObjectForKey:@"scope"] retain];

		contents = nil;

		

	}
	return self;
	
	
}


- (id)copyWithZone:(NSZone *)zone
{
	
	ListItem* copy = [[ListItem alloc] init];
	[copy setValue:[self valueForKey:@"name"] forKey:@"name"];
	[copy setValue:[self valueForKey:@"canDelete"] forKey:@"canDelete"];
	[copy setValue:[self valueForKey:@"canMove"] forKey:@"canMove"];
	[copy setValue:[self valueForKey:@"canDrop"] forKey:@"canDrop"];
	[copy setValue:[self valueForKey:@"canSelect"] forKey:@"canSelect"];
	[copy setValue:[self valueForKey:@"canExpand"] forKey:@"canExpand"];
	[copy setValue:[self valueForKey:@"isExpanded"] forKey:@"isExpanded"];

	[copy setValue:[self valueForKey:@"query"] forKey:@"query"];
	[copy setValue:[self valueForKey:@"scope"] forKey:@"scope"];
	 
	 
	return copy;
}

-(NSString*)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{
	//NSLog(@"searchWithAdditionalQuery %@",aQuery );
	requestedArrayController = [anArrayController retain];
		
	endTarget = [aTarget retain];
	endSelector = aSelector;

	
	isSearching = YES;
	[searchAgent search:@"" 
				 format:aQuery
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishForAdditionalQuery:)];
	
	return aQuery;
}
-(void)finishForAdditionalQuery:(id)result
{
	//NSLog(@"finishForAdditionalQuery");
	
	
	if( result == nil ) result = [NSArray array];
	//NSLog(@"result %d",[result count]);
	
	isSearching = NO;
	
	NSPredicate* predicate = [self filterPredicate];
	
	
	//NSLog(@"predicate %@",[predicate predicateFormat] );

	NSArray* filteredResult;
	
	if( predicate != nil ) //filter results
		filteredResult = [result filteredArrayUsingPredicate:predicate];
	else filteredResult = result;
	
	//NSLog(@"filteredResult %d",[filteredResult count]);

	
	[requestedArrayController addObjects: filteredResult];
	
	
	[endTarget performSelector:endSelector withObject:self];
	[endTarget release];
	[requestedArrayController release];
	endTarget = nil;
	endSelector = nil;
	requestedArrayController = nil;
	
	//[[NSNotificationCenter defaultCenter] postNotificationName:@"ListItemDidFinishSearchWithAdditionalQuery" object:self];
	
	[self postNotification];
	
}


-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{
	requestedArrayController = [arrayController retain];

	
	if( isRenaming ) {
		
		[self finishWithEndTarget:nil];
		return;
	}
	
	[contents release];
	contents = nil;
	
	endTarget = [aTarget retain];
	endSelector = aSelector;
	
	
	isSearching = YES;
	[searchAgent search:@"" 
				 format:[self query]
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishWithEndTarget:)];
}

-(void)finishWithEndTarget:(id)result
{
	
	
	if( result != nil )
	{
		contents = [[NSArray arrayWithArray:result] retain];
		[requestedArrayController addObjects: result];
	
	}
	
	isSearching = NO;
	

	[endTarget performSelector:endSelector withObject:self];
	[endTarget release];
	[requestedArrayController release];
	endTarget = nil;
	endSelector = nil;
	requestedArrayController = nil;

	[self postNotification];
	
}


/*
-(void)search
{
	if( isRenaming ) {
		
		return;
	}
	
	[contents release];
	contents = nil;
	
	isSearching = YES;
	
	if( [self query] == nil || [[self query] isEqualToString:@""]) //do nothing
	{
		[self finish:nil];
		return;
	}
	
	
	[searchAgent search:@"" 
				 format:[self query]
				  scope:[self scope]
		   cancelPreviousSearch:YES
		 target:self selector:@selector(finish:)];
}
-(void)finish:(id)result
{
	//NSLog(@"Finished %d",[result count]);
	
	if( result != nil )
		contents = [[NSArray arrayWithArray:result] retain];
	
	isSearching = NO;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"ListItemDidFinishSearching" object:self];
	
	[self postNotification];

}*/


-(NSPredicate*)filterPredicate
{
	if( contents == nil  ) return nil;
	
	NSMutableArray* paths = [[[NSMutableArray alloc] init] autorelease];
	
	unsigned hoge;
	for (hoge=0; hoge<[contents count]; hoge++) {
		NSString* path = [[contents objectAtIndex:hoge] objectForKey:kMDItemPath ];
		
		if( path != nil )
			[paths addObject:path ];
		
	}
	
	
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@" %@ CONTAINS kMDItemPath",paths];
	
	return predicate;	
}



-(void)postNotification
{
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OutlineChangedNotification" object:nil];
}

#pragma mark -
-(void)setName:(NSString*)aName
{
	isRenaming = NO;
	
	
	if( ![aName isEqualToString:name] )
	{
		[UNDO_MANAGER registerUndoWithTarget:self 
									selector:@selector(undoRename:) object:[self name]];
		[UNDO_MANAGER setActionName: NSLocalizedString(@"Rename",@"")];
	}
	
	[name release];
	name = [aName retain];
	
	[self postNotification];
}

-(void)undoRename:(NSString*)toName
{
	[self setName:	toName];

}

-(void)setCanDelete:(BOOL)flag
{
	canDelete = flag;
}
-(void)setCanMove:(BOOL)flag
{
	canMove = flag;
}
-(void)setCanDrop:(BOOL)flag
{
	canDrop = flag;
}
-(void)setCanSelect:(BOOL)flag
{
	canSelect = flag;
}
-(void)setCanExpand:(BOOL)flag
{
	canExpand = flag;
}
-(void)setExpanded:(BOOL)flag
{
	isExpanded = flag;	
}
-(void)setQuery:(NSString*)string
{
	[query release];
	query = [string retain];
	
	[contents release];
	contents = nil;
}
-(void)setScope:(NSArray*)array
{
	[scope release];
	scope = [array retain];
	
	[contents release];
	contents = nil;
}

-(BOOL)isDragging
{
	return		dragging;
}
-(void)setDragging:(BOOL)flag
{
	dragging = flag;	
}
-(BOOL)isSearching
{
	return isSearching;	
}

#pragma mark Accessor

-(NSString*)name {return name;}

-(NSArray*)scope {return scope;}
-(BOOL)canDelete {return canDelete;}
-(BOOL)canMove {return canMove;}
-(BOOL)canDrop {return canDrop;}
-(BOOL)canSelect {return canSelect;}
-(BOOL)canExpand {return canExpand;}
-(BOOL)isExpanded {return isExpanded;}


#pragma mark Overridden
-(NSString*)kind {return @"";}
-(NSString*)query {return query;}

-(NSImage*)icon
{
	/*
	if( icon == nil )
		icon = [[NSImage imageNamed:@"resultIcon.tiff"] retain];
	*/
	
	if( isSearching )
		return [NSImage imageNamed:@"isSearching.tiff"];
	
	return icon;
	
}


-(void)dropItem:(id)object
{
}
-(int)numberOfChildren
{
return 0;	
}
-(NSArray*)children
{
	return [NSArray array];	
}
-(NSArray*)contents
{
	return contents;	
}
-(void)removeItemFromChildren:(ListItem*)item
{
}

-(NSArray*)keywords
{
	NSString* myQueryString = [self query];
	
	if( myQueryString == nil || [myQueryString isEqualToString:@""] ) return nil;

	NSMutableString* query = [NSMutableString stringWithString: myQueryString];
	
	[query replaceOccurrencesOfString:@"\\'" withString:@"##apostrophe###" options:0 range:NSMakeRange(0, [query length])];
	[query replaceOccurrencesOfString:@"\\\"" withString:@"##doublequotation###" options:0 range:NSMakeRange(0, [query length])];

	
	
	OGRegularExpression *regex = [OGRegularExpression regularExpressionWithString:@"['\"][^'\"]*['\"]" options:OgreIgnoreCaseOption];
	
	NSMutableSet* strSet = [NSMutableSet set];
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:query];
	OGRegularExpressionMatch    *match;	// マッチ結果
	while ((match = [enumerator nextObject]) != nil) {	// 順番にマッチ結果を得る

		//NSLog(@"matchedStringraw %@", [match matchedString]);

		
		NSMutableString* matchedString = [NSMutableString stringWithString: [match matchedString]];
		
		[matchedString replaceOccurrencesOfString:@"'"  withString:@"" options:0 range:NSMakeRange(0, [matchedString length])];
		[matchedString replaceOccurrencesOfString:@"\""  withString:@"" options:0 range:NSMakeRange(0, [matchedString length])];

		
		[matchedString replaceOccurrencesOfString:@"##apostrophe###"  withString:@"\\'" options:0 range:NSMakeRange(0, [matchedString length])];
		[matchedString replaceOccurrencesOfString:@"##doublequotation###"  withString:@"\\\"" options:0 range:NSMakeRange(0, [matchedString length])];

		//NSLog(@"matchedString %@", matchedString);
		

		[strSet addObject:  matchedString];
		[strSet addObjectsFromArray:  [matchedString words]];
		
		 
	}
	
//Remove 1 or 0
	[strSet removeObject:@"0"];
	[strSet removeObject:@"1"];


	return [strSet allObjects];
	
	//(?<=["'])[^'"]*(?=['"])
}

-(BOOL)hasChild:(ListItem*)item
{
	return NO;
}
-(NSString*)queryTemplate {
	
	return nil;
}
-(void)setQueryTemplate:(NSString*)string
{
}

- (void)contextualMenu:(NSMenu*)menu
{
	if( canDelete )
	{
		NSMenuItem* customMenuItem;
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Rename",@"")
													action:@selector(rename) keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"MN"];
		[customMenuItem setTarget:self];
		
		[menu addItem:customMenuItem];
		[customMenuItem release];	

		//
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Delete",@"")
													action:@selector(delete) keyEquivalent:@""];
		[customMenuItem setRepresentedObject:@"MN"];
		[customMenuItem setTarget:self];
		
		[menu addItem:customMenuItem];
		[customMenuItem release];	

	}
}

-(void)rename
{
	isRenaming = YES;
	
	NSDocument* document = [[NSDocumentController sharedDocumentController] currentDocument];
	NSOutlineView* view = [document valueForKey:@"outlineView"];
	
	int row = [view rowForItem:self];
	
	NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:row];
	[view selectRowIndexes:indexes byExtendingSelection:NO];
	
	
	[view editColumn:0 row:row withEvent:nil select:YES];
	
}


-(void)delete
{
	if( canDelete )
	{
		//[searchAgent end];

		[self retain];
		{

			
			NSMutableArray* array = [[NSApp delegate] organiserArray];
			[array makeObjectsPerformSelector:@selector(removeItemFromChildren:) withObject:self];
			[array removeObjectIdenticalTo:self
				 usingUndoManager:UNDO_MANAGER
									 title:[NSString stringWithFormat:NSLocalizedString(@"Delete %@",@""),name ]];

			
			
			[self postNotification];
		}
		[self release];


	}
}

-(void)expandOrCollapseInView:(NSOutlineView*)outlineView
{
	////NSLog(@"expandOrCollapseInView");
	
	if( [self isExpanded] )
	{
		//NSLog(@"expandOrCollapseInView");

		[outlineView expandItem: self];
	}
	else
		[outlineView collapseItem: self];
	
	[[self children] makeObjectsPerformSelector:@selector(expandOrCollapseInView:) withObject:outlineView];
}


@end
