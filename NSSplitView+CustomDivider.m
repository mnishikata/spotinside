//
//  NSSplitView+CustomDivider.m
//  KantanTextEdit
//
//  Created by Masatoshi Nishikata on 07/09/11.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "NSSplitView+CustomDivider.h"


@implementation NSSplitView (CustomDivider)

- (void)drawDividerInRect:(NSRect)aRect //in flipped view
{
	if( ! [self isVertical] )
	{
	NSImage* image = [NSImage imageNamed:@"mainSplitterBar"];
	[image setFlipped:YES];
	[image drawInRect:aRect
			 fromRect:NSMakeRect(0,0,[image size].width,  [image size].height)
			operation:NSCompositeSourceOver 
			 fraction:1.0];
	
	NSImage* dimple = [NSImage imageNamed:@"mainSplitterDimple"];
	[dimple setFlipped:YES];
	[dimple drawInRect:NSMakeRect( (aRect.size.width - [dimple size].width)/2,aRect.origin.y,
								   [dimple size].width,  [dimple size].height )
			 fromRect:NSMakeRect(0,0,[dimple size].width,  [dimple size].height)
			operation:NSCompositeSourceOver 
			 fraction:1.0];
	
	}else{
		/*
		NSImage* image = [NSImage imageNamed:@"mainSplitterBarVertical"];
		[image setFlipped:YES];
		[image drawInRect:aRect
				 fromRect:NSMakeRect(0,0,[image size].width,  [image size].height)
				operation:NSCompositeSourceOver 
				 fraction:1.0];
		 */
	}
}
- (float)dividerThickness
{
		if( ! [self isVertical] )
		{
			return 10.0;
		}else return 1.0;
}

@end
