/*
 colorCheckbox
 m.nishikata
 http://homepage.mac.com/mnishikata/iblog/index.html
 
 REFERENCE:
 - tableView base code from
	http://homepage.mac.com/mkino2/cocoaProg/index.html
 
 - addRoundedRectToPath from Apple
 
 - Checkbox resources from iCal resource
 
 
 */

#import <Cocoa/Cocoa.h>


@interface ColorCheckbox : NSObject {

	
	
	NSImage* cutImage; // cut
	
	NSImage* glassImage; // reflection image
	
	NSImage* shadowImage; // shadow
	
	NSImage* checkImage; // check mark
	
}
+ (NSImage*)colorCheckbox:(NSColor* )aColor checkFlag:(BOOL)flag;
	// return color checkbox image

+ (NSImage*)roundedBox:(NSColor* )aColor size:(NSSize)boxSize curve:(float)boxCurve;
	// return roounded rectangle image

+ (void)roundedBoxFrame:(NSColor*)aColor frame:(NSRect)frame curve:(float)boxCurve openEnd:(int)openEnd;


	void addRoundedRectToPath(CGContextRef context, CGRect rect, 
							  float ovalWidth,float ovalHeight);
	void addRoundedRectWithOpenEndToPath(CGContextRef context, CGRect rect, 
										 float ovalWidth,float ovalHeight, int openEnd);
@end
