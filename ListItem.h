//
//  ListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SearchAgent.h"
#define UNDO_MANAGER [[[NSDocumentController sharedDocumentController] currentDocument]  undoManager]

@interface ListItem : NSObject {

	NSString* name;
	BOOL canDelete;
	BOOL canMove;
	BOOL canDrop;
	BOOL canSelect;
	BOOL canExpand;
	BOOL isExpanded;

	
	NSString* query;
	NSArray* scope;
	
	
	NSImage* icon;
	
	BOOL dragging;
	BOOL isSearching;
	
	SearchAgent* searchAgent;
	
	NSArray* contents;
	
	NSArrayController* requestedArrayController;
	
	
	/// 
	
	BOOL isRenaming;
	
	id endTarget;
	SEL endSelector;
}

- (id) init ;
-(void)dealloc;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
- (id)copyWithZone:(NSZone *)zone;
-(NSString*)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController;
-(void)finishForAdditionalQuery:(id)result;
-(void)search;
-(void)finish:(id)result;
-(NSPredicate*)filterPredicate;
-(void)postNotification;
-(void)setName:(NSString*)aName;
-(void)setCanDelete:(BOOL)flag;
-(void)setCanMove:(BOOL)flag;
-(void)setCanDrop:(BOOL)flag;
-(void)setCanSelect:(BOOL)flag;
-(void)setCanExpand:(BOOL)flag;
-(void)setExpanded:(BOOL)flag;
-(void)setQuery:(NSString*)string;
-(void)setScope:(NSArray*)array;
-(BOOL)isDragging;
-(void)setDragging:(BOOL)flag;
-(BOOL)isSearching;
-(NSString*)name ;
-(NSArray*)scope ;
-(BOOL)canDelete ;
-(BOOL)canMove ;
-(BOOL)canDrop ;
-(BOOL)canSelect ;
-(BOOL)canExpand ;
-(BOOL)isExpanded ;
-(NSString*)kind ;
-(NSString*)query ;
-(NSImage*)icon;
-(void)dropItem:(id)object;
-(int)numberOfChildren;
-(NSArray*)children;
-(NSArray*)contents;
-(void)removeItemFromChildren:(ListItem*)item;
-(BOOL)hasChild:(ListItem*)item;
-(NSArray*)keywords;


@end
