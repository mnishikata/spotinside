//
//  TagBotTagsValueTransformer.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "AvoidNullStringValueTransformer.h"


@implementation AvoidNullStringValueTransformer



+ (Class)transformedValueClass {
    return [NSString class];
}
+ (BOOL)allowsReverseTransformation { return YES; }

-(id)reverseTransformedValue:(id)value{
	if (value == nil || ![value isKindOfClass:[NSString class]]) {
        return @"";
    }
	return value;
	
	}
- (id)transformedValue:(id)value {

    if (value == nil || ![value isKindOfClass:[NSString class]]) {
        return @"";
    }
	return value;
	
}

@end
