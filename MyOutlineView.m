#import "MyOutlineView.h"

#import "ListItem.h"
#define OutlineRowType @"outline"
#define OutlineIndexesType @"outlineIndexes"
#define OutlineRowTypes [NSArray arrayWithObjects:@"outline", nil]

@class NDAlias;
@implementation MyOutlineView



-(void)drawRect:(NSRect)rect
{
	//NSLog(@"class %x %@",[self window], [[[self window] firstResponder] className]);

	[super drawRect:rect];
	
	// draw selected row count number
	NSIndexSet* selectedRowIndexes =   [self selectedRowIndexes];
	if( [ selectedRowIndexes count ] > 0 )
	{
		unsigned hoge;
		for( hoge = 0; hoge < [self numberOfRows] ; hoge++ )
		{
			if( [selectedRowIndexes containsIndex:hoge] )
			{
				id item = [self itemAtRow:hoge ];
				[[self delegate]  outlineView:self willDisplayCell:NULL 
							   forTableColumn:[self tableColumnWithIdentifier:@"C1" ] item:item];
			}
		}
	}
	
	
}
/*
- (BOOL)acceptsFirstResponder
{
	id firstResponder = [[self window] firstResponder];
	
	if( [firstResponder isKindOfClass:[NSTextView class]] )
	{
		////NSLog(@"Check?");
		
		if( [firstResponder hasMarkedText] )
		{
			NSBeep();
			return NO;
		}
	}
	return YES;
}*/


//---

typedef struct {
    float red1, green1, blue1, alpha1;
    float red2, green2, blue2, alpha2;
} _twoColorsType;

static void _linearColorBlendFunction(void *info, const float *in, float *out)
{
    _twoColorsType *twoColors = info;
    
    out[0] = (1.0 - *in) * twoColors->red1 + *in * twoColors->red2;
    out[1] = (1.0 - *in) * twoColors->green1 + *in * twoColors->green2;
    out[2] = (1.0 - *in) * twoColors->blue1 + *in * twoColors->blue2;
    out[3] = (1.0 - *in) * twoColors->alpha1 + *in * twoColors->alpha2;
}

static void _linearColorReleaseInfoFunction(void *info)
{
    free(info);
}

static const CGFunctionCallbacks linearFunctionCallbacks = {0, &_linearColorBlendFunction, &_linearColorReleaseInfoFunction};

/*
 End CoreGraphics gradient helpers
 */


// API


// NSView subclass



// NSTableView subclass 


- (id)_highlightColorForCell:(NSCell *)cell;
{

	if( [[NSApp delegate] OSVersion] >= 105 )
	{
		//NSLog(@"%d",MAC_OS_X_VERSION_10_5 );

	}else
	{

		NSAttributedString* attr = [cell attributedStringValue];
		if( attr == nil ) return nil;
		
		NSMutableAttributedString* mattr = [[NSMutableAttributedString alloc] initWithAttributedString: attr];
		[mattr addAttribute:NSForegroundColorAttributeName
					  value:[NSColor whiteColor] range:NSMakeRange(0,[mattr length])];
		[cell setAttributedStringValue: mattr];
		[mattr release];
	}
    return nil;
}

- (void)highlightSelectionInClipRect:(NSRect)rect;
{
    // Take the color apart
    NSColor *alternateSelectedControlColor = [NSColor alternateSelectedControlColor];
    float hue, saturation, brightness, alpha;
    [[alternateSelectedControlColor colorUsingColorSpaceName:NSDeviceRGBColorSpace] getHue:&hue saturation:&saturation brightness:&brightness alpha:&alpha];
	
    // Create synthetic darker and lighter versions
    // NSColor *lighterColor = [NSColor colorWithDeviceHue:hue - (1.0/120.0) saturation:MAX(0.0, saturation-0.12) brightness:MIN(1.0, brightness+0.045) alpha:alpha];
    NSColor *lighterColor = [NSColor colorWithDeviceHue:hue saturation:MAX(0.0, saturation-.12) brightness:MIN(1.0, brightness+0.30) alpha:alpha];
    NSColor *darkerColor = [NSColor colorWithDeviceHue:hue saturation:MIN(1.0, (saturation > .04) ? saturation+0.12 : 0.0) brightness:MAX(0.0, brightness-0.045) alpha:alpha];
    
    // If this view isn't key, use the gray version of the dark color. Note that this varies from the standard gray version that NSCell returns as its highlightColorWithFrame: when the cell is not in a key view, in that this is a lot darker. Mike and I think this is justified for this kind of view -- if you're using the dark selection color to show the selected status, it makes sense to leave it dark.
    if ([[self window] firstResponder] != self || ![[self window] isKeyWindow]) {
        alternateSelectedControlColor = [[alternateSelectedControlColor colorUsingColorSpaceName:NSDeviceWhiteColorSpace] colorUsingColorSpaceName:NSDeviceRGBColorSpace];
        lighterColor = [[lighterColor colorUsingColorSpaceName:NSDeviceWhiteColorSpace] colorUsingColorSpaceName:NSDeviceRGBColorSpace];
        darkerColor = [[darkerColor colorUsingColorSpaceName:NSDeviceWhiteColorSpace] colorUsingColorSpaceName:NSDeviceRGBColorSpace];
    }
    
    // Set up the helper function for drawing washes
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    _twoColorsType *twoColors = malloc(sizeof(_twoColorsType)); // We malloc() the helper data because we may draw this wash during printing, in which case it won't necessarily be evaluated immediately. We need for all the data the shading function needs to draw to potentially outlive us.
    [lighterColor getRed:&twoColors->red1 green:&twoColors->green1 blue:&twoColors->blue1 alpha:&twoColors->alpha1];
    [darkerColor getRed:&twoColors->red2 green:&twoColors->green2 blue:&twoColors->blue2 alpha:&twoColors->alpha2];
    static const float domainAndRange[8] = {0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0};
    CGFunctionRef linearBlendFunctionRef = CGFunctionCreate(twoColors, 1, domainAndRange, 4, domainAndRange, &linearFunctionCallbacks);
    
#ifdef DO_IT_PANTHER
    NSIndexSet *selectedRowIndexes = [self selectedRowIndexes];
    unsigned int rowIndex = [selectedRowIndexes indexGreaterThanOrEqualToIndex:0];
#else
    NSEnumerator *selectedRowEnumerator = [self selectedRowEnumerator];
    NSNumber *rowIndexNumber = [selectedRowEnumerator nextObject];
    unsigned int rowIndex = rowIndexNumber ? [rowIndexNumber unsignedIntValue] : NSNotFound;
#endif
	
    while (rowIndex != NSNotFound) {
        unsigned int endOfCurrentRunRowIndex, newRowIndex = rowIndex;
        do {
            endOfCurrentRunRowIndex = newRowIndex;
#ifdef DO_IT_PANTHER
            newRowIndex = [selectedRowIndexes indexGreaterThanIndex:endOfCurrentRunRowIndex];
#else
            rowIndexNumber = [selectedRowEnumerator nextObject];
            newRowIndex = rowIndexNumber ? [rowIndexNumber unsignedIntValue] : NSNotFound;
#endif
        } while (newRowIndex == endOfCurrentRunRowIndex + 1);
		
        NSRect rowRect = NSUnionRect([self rectOfRow:rowIndex], [self rectOfRow:endOfCurrentRunRowIndex]);
        
        NSRect topBar, washRect;
        NSDivideRect(rowRect, &topBar, &washRect, 1.0, NSMinYEdge);
        
        // Draw the top line of pixels of the selected row in the alternateSelectedControlColor
        [alternateSelectedControlColor set];
        NSRectFill(topBar);
		
        // Draw a soft wash underneath it
        CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
        CGContextSaveGState(context); {
            CGContextClipToRect(context, (CGRect){{NSMinX(washRect), NSMinY(washRect)}, {NSWidth(washRect), NSHeight(washRect)}});
            CGShadingRef cgShading = CGShadingCreateAxial(colorSpace, CGPointMake(0, NSMinY(washRect)), CGPointMake(0, NSMaxY(washRect)), linearBlendFunctionRef, NO, NO);
            CGContextDrawShading(context, cgShading);
            CGShadingRelease(cgShading);
        } CGContextRestoreGState(context);
		
        rowIndex = newRowIndex;
    }
	
    
    CGFunctionRelease(linearBlendFunctionRef);
    CGColorSpaceRelease(colorSpace);
}

- (void)selectRow:(int)row byExtendingSelection:(BOOL)extend;
{
    [super selectRow:row byExtendingSelection:extend];
    [self setNeedsDisplay:YES]; // we display extra because we draw multiple contiguous selected rows differently, so changing one row's selection can change how others draw.
}

- (void)deselectRow:(int)row;
{
    [super deselectRow:row];
    [self setNeedsDisplay:YES]; // we display extra because we draw multiple contiguous selected rows differently, so changing one row's selection can change how others draw.
}



-(IBAction)reveal:(id)sender
{
	id item = [self itemAtRow:[self selectedRow]];
	
	if( [[item objectForKey:@"kind"] isEqualToString:@"item"] || 
		[[item objectForKey:@"kind"] isEqualToString:@"savedSearch"] )
	{
		
		NDAlias* alias = [item objectForKey:@"ndalias"];
		NSString* path = [alias path];
		
		if( path != nil )
			[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath: path];
		
	}		
	
}

#pragma mark -



- (void)concludeDragOperation:(id <NSDraggingInfo>)sender
{
 //FInish dragging
	unsigned num = [self numberOfRows];
	unsigned hoge;
	for( hoge = 0; hoge < num; hoge++ )
	{
	id item = [self itemAtRow: hoge];
		[item setDragging:NO];
	}
	
	[super concludeDragOperation:sender];
}

-(void)mouseDown:(NSEvent*)event
{
	//NSLog(@"makeFirstResponder");
	[[self window] makeFirstResponder:self];
	[super mouseDown:event];
}

- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	NSMenu* menu = [[[NSMenu alloc] initWithTitle:@""] autorelease];
	
	
	NSPoint mouseLoc = [[self window]  mouseLocationOutsideOfEventStream]; 
	mouseLoc = [self convertPoint:mouseLoc fromView:[[self window] contentView]];
	
	////DBLOG(@"c %@  ", NSStringFromPoint(mouseLoc));
	
	NSRange range;
	int columnIndex;
	int rowIndex;
	
	range = [self columnsInRect:NSMakeRect(mouseLoc.x, mouseLoc.y, 1,1)];
	if( range.length == 0 ) columnIndex = -1;
	else
		columnIndex = range.location;
	
	range = [self rowsInRect:NSMakeRect(mouseLoc.x, mouseLoc.y, 1,1)];
	if( range.length == 0 ) rowIndex = -1;
	else
		rowIndex = range.location;
	
	
	
	if( rowIndex != -1 )
	{
	
		ListItem* item = [self itemAtRow:rowIndex];
		[item contextualMenu:menu];
	}
	
	return menu;
	
}

/// cell editing

- (void)textDidEndEditing:(NSNotification *)notification
{ 
    if ([[[notification userInfo] objectForKey:@"NSTextMovement"] intValue] == NSReturnTextMovement)
    {
        NSMutableDictionary * newUserInfo;
        NSNotification * newNotification;
        newUserInfo = [NSMutableDictionary dictionaryWithDictionary:[notification userInfo]]; 
        [newUserInfo setObject:[NSNumber numberWithInt:NSIllegalTextMovement] forKey:@"NSTextMovement"];
        newNotification = [NSNotification notificationWithName:[notification name] object:[notification object] userInfo:newUserInfo];
        [super textDidEndEditing:newNotification];
        [[self window] makeFirstResponder:self]; 
    } else {
        [super textDidEndEditing:notification]; 
    }
}
/*
-(NSUndoManager*)undoManager
{
	NSLog(@"undoManager OV");
	return [[[NSDocumentController sharedDocumentController] currentDocument]  undoManager];	
}

 
 //RBSplitView の中に入っていると、レスポンダチェーンに加わらないの？
 
 

- (BOOL)acceptsFirstResponder
{
	NSLog(@"accepsts");

	return YES;
}

- (BOOL)becomeFirstResponder;
{
	NSLog(@"become");

    return YES;
}

- (BOOL)resignFirstResponder;
{
	NSLog(@"resign");

    return YES;
}
*/



@end

@interface NSResponder (ext)
@end
@implementation  NSResponder (ext)
-(NSUndoManager*)undoManager
{
	//NSUndoManager* um = [[[NSDocumentController sharedDocumentController] currentDocument]  undoManager];
	//if( [self isKindOfClass:[NSView class]] )
	NSUndoManager* um = [[[NSDocumentController sharedDocumentController] currentDocument]  undoManager];
		return um;
	//return nil;
}
@end
