//
//  SearchAgent.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <CoreServices/CoreServices.h>

@interface SearchAgent : NSObject {
	//MDQueryRef _query;
	id endTarget;
	SEL endSelector;
	NSArray* keysToRetrieve;
	MDQueryRef _query;
	
	NSLock* lock;
	BOOL cancel;
	
}

@end
