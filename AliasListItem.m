//
//  FolderListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "AliasListItem.h"

@implementation AliasListItem



- (id) init {
	self = [super init];
	if (self != nil) {

		alias = nil;

		canDelete = YES;
		canMove = YES;
		canDrop = NO;
		canSelect = YES;
		
		[icon release]; 
		icon = nil;
	}
	return self;
}

-(void)dealloc
{
	
	[super dealloc];
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	
	
	[super encodeWithCoder: encoder];
	
	[encoder encodeObject:		alias					forKey:@"alias"];

	
	return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		canDelete = YES;
		canMove = YES;
		canDrop = NO;
		canSelect = YES;
		
		id obj = [coder decodeObjectForKey:@"alias"];
		
		if( obj != nil  )
		{
			[alias release];
			alias = [obj retain];
			
			NSString* path = [self path];
			
			if( path != nil )
			{
				[icon release];
				icon = [[[NSWorkspace sharedWorkspace] iconForFile: path] retain];
				[icon setSize:NSMakeSize(16,16)];
				[self setName:[path lastPathComponent]];
			}
			
		}
		
		
	}
	return self;
	
	
}


- (id)copyWithZone:(NSZone *)zone
{
	
	AliasListItem* copy = [[AliasListItem alloc] init];
	[copy setAliasWithPath:[alias path]];
	
	
	
	return copy;
}

-(void)setAliasWithPath:(NSString*)path
{
	[alias release];
	alias = [[NDAlias aliasWithPath:path] retain];
	
	[icon release];
	icon = [[[NSWorkspace sharedWorkspace] iconForFile: path] retain];
	[icon setSize:NSMakeSize(16,16)];
	[self setName:[path lastPathComponent]];
	
	[self postNotification];
}

-(BOOL)isMissing
{
	[self path];

	if( alias == nil ) return YES;
	return NO;
}

-(NSString*)path
{
	NSString* path = [alias path];
	
	if( path == nil )
	{
		if( alias != nil ) {
			[alias release];
			alias = nil;
			
			[icon release];

			icon = [[NSImage imageNamed:@"missing.tiff"] retain];
			[self postNotification];

		}
		return nil;
		
	}
	
	
	return path;	
}

-(NSString*)kind {return @"item";}

/*
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{	
	requestedArrayController = anArrayController;
	endTarget = [aTarget retain];
	endSelectr = aSelector;

	isSearching = YES;
	[searchAgent search:@"" 
				 format:aQuery
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishForAdditionalQuery:)];
}*/


-(BOOL)_isFolder
{//NSLog(@"_isFolder");
	
	NSString* path = [self path];
	

	if( path == nil ) return NO;
	
	BOOL isDir;
	[[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:&isDir];

	
	//NSLog(@"isDir %d",isDir);
			//NSLog(@"%d",[[NSWorkspace sharedWorkspace] isFilePackageAtPath:path ]);

	if( isDir )
		if( ![[NSWorkspace sharedWorkspace] isFilePackageAtPath:path ] )
			return YES;
	
	return NO;
				
}

-(NSArray*)scope {
	
	if( [self _isFolder] ) return [NSArray arrayWithObject: [self path]];
	
	return [super scope];

}


-(NSArray*)contents
{
	if( contents == nil )
	{
		NSMutableArray* array = [NSMutableArray array];

		NSString* path = [self path];
		if( path == nil ) {
			contents =  [[NSArray array] retain];
			return contents;
		}
			
			
		//NSLog(@"contents is not nil");
		
		if( [self _isFolder] )		
		{
			////NSLog(@"contents is folder");

			NSArray* subpaths = [[NSFileManager defaultManager] directoryContentsAtPath:path];
			
			unsigned hoge;
			for (hoge=0; hoge<[subpaths count]; hoge++) {
				NSString* aSubpath = [subpaths objectAtIndex:hoge];
				if( ![aSubpath hasPrefix:@"."] )
					[array addObject: [path stringByAppendingPathComponent:aSubpath]];
			}
			
		}else
		{
			////NSLog(@"contents is %@",path);

			[array addObject: path];
		}
		
		
		
		NSMutableArray* attributesArray = [NSMutableArray array];
		
		unsigned hoge;
		for (hoge=0; hoge<[array count]; hoge++) {
			MDItemRef mditem =  MDItemCreate (nil,[array objectAtIndex:hoge]);
			if( mditem != nil )
			{
				NSMutableDictionary* attributes = [searchAgent extractAttributesFrom:mditem];
				
				[attributes setObject:[NSNumber numberWithFloat:1.0] forKey:kMDQueryResultContentRelevance];
				
				CFRelease(mditem);
				[attributesArray addObject: attributes];
				
			}else
			{
				NSMutableDictionary* attributes = [NSMutableDictionary dictionary];
				
				[attributes setObject:[array objectAtIndex:hoge] forKey:kMDItemPath];
				[attributes setObject:[NSNumber numberWithFloat:1.0] forKey:kMDQueryResultContentRelevance];
				
				[attributesArray addObject: attributes];
				
			}
			
		}
		////NSLog(@"contents attributesArray %@",[ attributesArray description]);
		contents = [attributesArray retain];
	}
	
	return contents;
}

-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{
	requestedArrayController = [arrayController retain];

	[contents release];
	contents = nil;
	
	endTarget = [aTarget retain];
	endSelector = aSelector;

	isSearching = YES;

	
	NSString* path = [self path];
	if( path != nil )
	{
		NSMutableArray* array = [self contents];
		
		
		[self finishWithEndTarget:array];
		return;
	}
	
	
	[self finishWithEndTarget:[NSArray array]];

}
/*
-(void)search
{
	[contents release];
	contents = nil;
	
	NSString* path = [self path];
	if( path != nil )
	{
		NSMutableArray* array = [self contents];
	
				
		[self finish:array];
		return;
	}
		
	
	
	[self finish:[NSArray array]];


	
}*/

-(NSPredicate*)filterPredicate
{
	NSString* path = [self path];
	if( path == nil ) return nil;

	if( [self _isFolder] )	
	{
		NSPredicate *predicate = [NSPredicate predicateWithFormat:@" kMDItemPath BEGINSWITH  %@",path];

		return predicate;
	}
	
	
	return [super filterPredicate];

}

- (void)contextualMenu:(NSMenu*)menu{
	NSMenuItem* customMenuItem;
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Reveal in Finder",@"")
												action:@selector(reveal) keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"MN"];
	[customMenuItem setTarget:self];
	[customMenuItem autorelease];	
	
	[menu addItem:customMenuItem];
	
	[super contextualMenu:menu];

}
-(void)reveal
{
	NSString* path = [self path];
	
	[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath: path];
	
}


@end
