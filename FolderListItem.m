//
//  FolderListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "FolderListItem.h"

@implementation FolderListItem



- (id) init {
	self = [super init];
	if (self != nil) {

		children = [[NSMutableArray alloc] init];
		contents = nil;
		
		name = [ NSLocalizedString(@"Untitled Group",@"") retain];
		canDelete = YES;
		canMove = YES;
		canDrop = YES;
		canSelect = YES;
		canExpand = YES;
		
		icon = nil;
		
	}
	return self;
}

-(void)dealloc
{
	[children release];	
	
	[super dealloc];
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	
	
	[super encodeWithCoder: encoder];
	
	[encoder encodeObject:		children					forKey:@"children"];

	
	
	
	return;
}


- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		children = [[NSMutableArray alloc] init];
		contents = nil;

		canDelete = YES;
		canMove = YES;
		canDrop = YES;
		canSelect = YES;
		canExpand = YES;
		
		icon = nil;
		
		
		NSArray* array = [coder decodeObjectForKey:@"children"];
		
		if( array != nil && [array count] > 0 )
			[children addObjectsFromArray: array];
		
		
	}
	return self;
	
	
}


- (id)copyWithZone:(NSZone *)zone
{
	
	FolderListItem* copy = [[FolderListItem alloc] init];
	
	[[copy children] addObjectsFromArray: children];
	
	
	return copy;
}

-(NSMutableArray*)children
{
	return children;	
}

-(NSString*)kind {return @"folder";}

-(NSImage*)icon
{
	if( icon == nil )
		icon = [[NSImage imageNamed:@"group.tiff"] retain];
	

	return [super icon];
	
}



-(void )setQuery:(id)value
{
}
-(void )setChildren:(id)value
{
}

-(NSString*)query {
	
	NSMutableString* string = [[[NSMutableString alloc] init] autorelease];
	int hoge;
	for( hoge = 0; hoge < [children count]; hoge++ )
	{
		NSString* childQuery = [[children objectAtIndex:hoge] query ];
		
		if( childQuery != nil && ![childQuery isEqualToString:@""] )
		{
			if( hoge >0 )
				[string appendString:@"||"];

			
			[string appendString:@"("];
			[string appendString: childQuery];
			[string appendString:@")"];
		
		}

	}
	
	return string;
}



-(int)numberOfChildren
{
	return [children count];	
}


-(BOOL)canExpand
{
	return ([children count]>0? YES:NO);
}


/*
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{

	requestedArrayController = anArrayController;

	endTarget = [aTarget retain];
	endSelectr = aSelector;

	
	isSearching = YES;
	[searchAgent search:@"" 
				 format:aQuery
				  scope:[self scope]
   cancelPreviousSearch:YES
				 target:self selector:@selector(finishForAdditionalQuery:)];
	
}

*/

-(NSString*)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector
{
	
	if( isRenaming || [children count] == 0) {
		[aTarget performSelector:aSelector withObject:self];
		[self postNotification];
		
		return aQuery;
	}
	
	requestedArrayController = [anArrayController retain];
	
	
	[contents release];
	contents = [[NSArray array] retain];
	
	childrenInSearching = [[NSMutableArray alloc] initWithArray: children];
	
	endTarget = [aTarget retain];
	endSelector = aSelector;
	
	isSearching = YES;
	
	
	
	unsigned hoge;
	for (hoge=0; hoge< [children count]; hoge++) {
		
		
		[[children objectAtIndex: hoge] searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:self andSelector:@selector(childFinishSearch:)];
		
	}

	return aQuery;

}



-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{

	if( isRenaming || [children count] == 0) {
		[aTarget performSelector:aSelector withObject:self];
		[self postNotification];

		return;
	}
	
	requestedArrayController = [arrayController retain];

	
	[contents release];
	contents = [[NSArray array] retain];
	
	childrenInSearching = [[NSMutableArray alloc] initWithArray: children];
	
	endTarget = [aTarget retain];
	endSelector = aSelector;
	
	isSearching = YES;
	
	
	
	unsigned hoge;
	for (hoge=0; hoge< [children count]; hoge++) {
		
		
		[[children objectAtIndex: hoge] searchForArrayController:arrayController 
												   withEndTarget:self andSelector:@selector(childFinishSearch:)];

	}

}

-(void)childFinishSearch:(id)sender
{
	//
	NSArray* contentsOfChild = [sender contents];
	
	if( contentsOfChild != nil )
	{
		NSMutableArray* newContents = [[NSMutableArray alloc] initWithArray: contents];
		[newContents addObjectsFromArray:[sender contents]  ];
		
		//NSLog(@"[item contents] %@",[[item contents] description]);
		
		[contents release];
		contents = [[NSArray alloc] initWithArray: newContents];
		
		[newContents release];
	}
	
	
	// 
	[childrenInSearching removeObject:sender];
	
	if( [childrenInSearching count] == 0 )
	{
		[childrenInSearching release];
		childrenInSearching = nil;
		
		[endTarget performSelector:endSelector withObject:self];
		[endTarget release];
		endTarget = nil;
		endSelector = nil;		
		
		isSearching = NO;
		[self postNotification];

	}
	
}

/*
-(void)search
{
	if( isRenaming ) return;

	
	[contents release];
	contents = [[NSArray array] retain];
	
	countForFinishedChildrenSearching = [children count];
	
	
	if( countForFinishedChildrenSearching == 0 )
	{
		[self finish:nil];
		return;
	}
	
	unsigned hoge;
	for (hoge=0; hoge<countForFinishedChildrenSearching; hoge++) {

		
		
		[[NSNotificationCenter defaultCenter] addObserver:self
												 selector:@selector(searchListener:) 
													 name:@"ListItemDidFinishSearching"
												   object:[children objectAtIndex:hoge]];
		
		//[[children objectAtIndex:hoge] search];		
		////NSLog(@"search %@ count %d", [[children objectAtIndex:hoge] name] ,hoge);
	}
	

	[children makeObjectsPerformSelector:@selector(search)];
	
	[self postNotification];
}
-(void)searchListener:(NSNotification*)notification
{
	//NSLog(@"searchListener");
	isSearching = YES;

	ListItem* item = [notification object];
	

	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:@"ListItemDidFinishSearching"
												  object:item];
	countForFinishedChildrenSearching--;
	

	NSMutableArray* newContents = [[NSMutableArray alloc] initWithArray: contents];
	[newContents addObjectsFromArray:[item contents]  ];
	
	//NSLog(@"[item contents] %@",[[item contents] description]);
	
	[contents release];
	contents = [[NSArray alloc] initWithArray: newContents];
	
	[newContents release];



	
	if( countForFinishedChildrenSearching==0 )
	{
		//NSLog(@"here");
		
		

		[self finish:nil];

	}

}

-(void)finish:(id)result
{
	
	//contents = [[NSArray arrayWithObjects:result] retain];
	isSearching = NO;
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"ListItemDidFinishSearching" object:self];
	
	[self postNotification];

}*/

-(void)removeItemFromChildren:(ListItem*)item
{
	[children removeObjectIdenticalTo: item
		 usingUndoManager:UNDO_MANAGER
								title:[NSString stringWithFormat:NSLocalizedString(@"Delete %@",@""),[item name] ]];

	[children makeObjectsPerformSelector:@selector(removeItemFromChildren:) withObject:item];
}
-(BOOL)hasChild:(ListItem*)item
{
	if( [children indexOfObjectIdenticalTo: item] != NSNotFound ) {
		//NSLog(@"indexOfObjectIdenticalTo");
		return YES;
	}
	
	unsigned hoge;
	for( hoge = 0; hoge < [children count] ; hoge++ )
	{
	
		id child = [children objectAtIndex:hoge];
		//NSLog(@"chid [child hasChild:item] %d",[child hasChild:item]);
		if( [child hasChild:item] ) return YES;
	}
	
	return NO;
}

@end
