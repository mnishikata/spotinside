//
//  C3DTNode.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTNode.h,v 1.2 2003/06/29 13:52:49 pmanna Exp $
//
// $Log: C3DTNode.h,v $
// Revision 1.2  2003/06/29 13:52:49  pmanna
// Added recursive name search
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTObject.h"


@interface C3DTNode : C3DTObject {
    BOOL			_needsUpdate;

    C3DTNode		*_parent;
    NSMutableArray	*_children;
}

/*
 * Parent and children accessors
 */
- (int)depth;
- (C3DTNode *)parent;
- (void)setParent: (C3DTNode *)newParent;

- (NSMutableArray *)children;
- (void)setChildren: (NSMutableArray *)newChildren;

// Search for a node by name
- (C3DTObject *)findNamedObject: (NSString *)objName inArray: (NSArray *)array;

/*
 * Tree handling
 */
- (void)addChild: (C3DTNode *)child;
- (void)insertAboveNode: (C3DTNode *)aNode;

- (void)removeAllChildren;
- (void)removeChild: (C3DTNode *)child;
- (void)removeChildNamed: (NSString *)childName;

- (void)transferChild: (C3DTNode *)child toNode: (C3DTNode *)newParent;
- (void)transferChildNamed: (NSString *)childName toNode: (C3DTNode *)newParent;

- (void)removeAndTransferChild: (C3DTNode *)child;
- (void)removeAndTransferChildNamed: (NSString *)childName;

@end

/*
 * Recursive tree operations
 */
@interface C3DTNode (C3DTRecursiveOperations)

- (void)updateWith: (id)anObject;
- (void)useWith: (id)anObject;
- (void)animate;
- (C3DTObject *)findNamedObject: (NSString *)objName;

@end
