#import "FileLocationWell.h"




@implementation FileLocationWell

- (void) awakeFromNib
{

	[self setTarget : self];
	[self setAction : @selector(_pushed:)];
}

- (void)_pushed:(NSEvent *)theEvent
{
	
	NSOpenPanel* aPanel = (NSOpenPanel*)[NSOpenPanel openPanel]; 
	
	[aPanel setTitle:NSLocalizedString(@"Choose Location",@"")];
	[aPanel setPrompt:NSLocalizedString(@"Choose",@"")];
	[aPanel setCanChooseFiles:YES];
	[aPanel setCanChooseDirectories:YES];
	[aPanel setCanCreateDirectories:NO];
	
	
	if( [aPanel runModalForDirectory:nil file:nil types:nil] == NSFileHandlingPanelOKButton )
	{ 

		//TITLE BINDING
		
		/*
		NSDictionary* observerInfo = [self infoForBinding:@"title"];
		id observer = [observerInfo objectForKey:@"NSObservedObject"];
		if( observer != nil ) [observer setValue:[aPanel filename] forKey:[observerInfo objectForKey:@"NSObservedKeyPath"]];
*/
		[self setTitle:[aPanel filename]];

		return ;
	} 
	
	
}
-(IBAction)playSound:(id)sender
{
	NSSound* sound = [[[NSSound alloc] initWithContentsOfFile:[self title] byReference:NO] autorelease];
	if( sound != nil )
	[sound play];
}

@end
