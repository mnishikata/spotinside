//
//  PointToMilTransformer.m
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "KeyedUnarchiveFromDataTransformer.h"

@implementation KeyedUnarchiveFromDataTransformer

+(Class)transformedValueClass{
	return [NSObject class];
}

+(BOOL)allowsReverseTransformation{
	return YES;
}




-(id)transformedValue:(id)value{
	
	if( value == nil ) return value;
	
	id obj;
	
	@try {
		obj = [NSKeyedUnarchiver unarchiveObjectWithData:value ];
	}
	@catch (NSException * e) {
		obj = nil;
	}
	
	
	return obj; // point
	
}

-(id)reverseTransformedValue:(id)value{
	
	if( value == nil ) return value;

	
	id obj;
	
	@try {
		obj = [NSKeyedArchiver  archivedDataWithRootObject : value ];
	}
	@catch (NSException * e) {
		obj = nil;
	}

	
	return obj; // point
}


@end
