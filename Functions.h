#import <Cocoa/Cocoa.h>
#include <Carbon/Carbon.h>

@class MNNodeObject;

typedef struct{

	double angle;
	double r;
	double z;
	double rotation;
	double spacing;
	
}CoverCoordinate;



int Calc_r(double N, int R);
double Calc_N(int R,  double r, int n);
int Calc_n(int R,  double N);

double PatternTemplate(double time);
double PatternTemplateForRotation(double time);

double Spacer(double time, Boolean flag);
double Rotation(double time, double N, double Ns, double Ns_previous);

double Rotaion_Animation(double time, double startAngle, double endAngle, Boolean animateClockwise);
CoverCoordinate CoverCoordinateForN(double N, int R);
double Angle( double time, double Ns, double Ns_previous, int R );

NSComparisonResult CompareCoordinates(CoverCoordinate c1, CoverCoordinate c2);
double ConvertToLinear( double time, CoverCoordinate coordinate, NSArray* nodeObjects, double Ns, double Ns_previous, double min, double max );
double ConvertToLinear_RelativeXFromMin( double time, CoverCoordinate coordinate, NSArray* nodeObjects, double Ns, double Ns_previous, double min, double max );


double Z_Animation(double time, double startz, double endz );
double Z(double time, double N, double Ns, double Ns_previous);

CoverCoordinate AddCoordinates(CoverCoordinate c1, CoverCoordinate c2);
CoverCoordinate SubtractCoordinates(CoverCoordinate c1, CoverCoordinate c2);

CoverCoordinate RegularizeCoordinate(CoverCoordinate c);

CoverCoordinate Angle_Animation( double progress, double startN,  double endN, int R, double *progressN );


double RawVelocity(double x, double d0, double d1, double dd);

double RawIntegral(double x, double d0, double d1, double dd);

double RegularizedIntegral(double x, double d0, double d1, double dd);

double Spacing_Animation(double time, double start, double end );


/*
 unused
 
 */
//double  __Spacer(double progress, Boolean flag);
