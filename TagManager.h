/* TagManager */

#import <Cocoa/Cocoa.h>

@interface TagManager : NSObject
{
    IBOutlet id comment;
    IBOutlet id doAll;
	IBOutlet id donotMove;

    IBOutlet id filename;
    IBOutlet id image;
    IBOutlet id textView;
    IBOutlet id window;
	
	NSMutableArray* targetPathArray;
	NSWindow* windowForSheet;
	
	id activeCell;
}
- (IBAction)buttonClicked:(id)sender;
@end
