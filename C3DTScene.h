//
//  C3DTScene.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTScene.h,v 1.2 2003/06/29 13:54:49 pmanna Exp $
//
// $Log: C3DTScene.h,v $
// Revision 1.2  2003/06/29 13:54:49  pmanna
// Added handling of picking objects
//
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import "C3DTNode.h"
#import "C3DTCamera.h"
#import "C3DTGeometry.h"
#import "C3DTStyle.h"
#import "C3DTLight.h"

@interface C3DTScene : C3DTNode {
    NSMutableArray		*_geometries;	// Geometries and styles are not necessarily stored here, as they are
    NSMutableArray		*_styles;		// retained by Entities or Transforms: but, if a scene has common
                               // objects of these classes (i.e. common colors or textures, or a repetition
                               // of the same object with different transformations), it's a good idea to store
                               // them here and use refs in the Entities/Transforms
    NSMutableArray		*_lights;		// Lights are attributes of a scene, they've their place here
    NSMutableArray		*_selection;	// List of selected objects
}

+ (C3DTScene *)defaultScene;

- (NSArray *)geometries;
- (void)addGeometry: (C3DTGeometry *) aGeometry;
- (void)removeGeometry:(C3DTGeometry *)aGeometry;
- (void)removeGeometryNamed:(NSString *)aGeometryName;

- (NSArray *)styles;
- (void)addStyle: (C3DTStyle *) aStyle;
- (void)removeStyle:(C3DTStyle *)aStyle;
- (void)removeStyleNamed:(NSString *)aStyleName;

- (NSArray *)lights;
- (void)addLight: (C3DTLight *) aLight;
- (void)removeLight:(C3DTLight *)aLight;
- (void)removeLightNamed:(NSString *)aLightName;

@end
