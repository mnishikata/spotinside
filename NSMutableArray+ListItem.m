//
//  NSMutableArray+ListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/13.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "NSMutableArray+ListItem.h"

/*
@interface NSTextField (ext)
@end


@implementation NSTextField (ext) // nstextfield の　undo は必要ないので省いてしまう
-(NSUndoManager*)undoManager
{
	return nil;
}
@end
*/

@implementation NSMutableArray (ListItem)
-(void)undoRemoveListItem:(id)undoDictionary
{
	id object = [undoDictionary objectForKey:@"object"];
	unsigned index = [[undoDictionary objectForKey:@"index"] intValue];
	NSUndoManager* undoManager = [undoDictionary objectForKey:@"undoManager"];
	
	if( object == nil ) return;
	
	if( undoManager != nil )
	{
		[undoManager registerUndoWithTarget:self selector:@selector(undoInsertListItem:) object:undoDictionary];	
	}
	
	
	if( index  >= [self count] ) [self addObject:object];
	else [self insertObject:object atIndex:index];
	
	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OutlineChangedNotification" object:nil];
	
}
-(void)undoInsertListItem:(id)undoDictionary
{
	[[NSApp keyWindow] endEditingFor:nil];
	
	id object = [undoDictionary objectForKey:@"object"];
	NSUndoManager* undoManager = [undoDictionary objectForKey:@"undoManager"];

	if( object == nil ) return;
	
	
	if( undoManager != nil )
	{
		[undoManager registerUndoWithTarget:self selector:@selector(undoRemoveListItem:) object:undoDictionary];	
	}

		
		
	[self removeObjectIdenticalTo: object];
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"OutlineChangedNotification" object:nil];
	
}

-(void) removeObjectIdenticalTo:(id)object usingUndoManager:(NSUndoManager*)undoManager title:(NSString*)title
{
	[[NSApp keyWindow] endEditingFor:nil];

	
	unsigned index = [self indexOfObjectIdenticalTo: object];
	
	if( index != NSNotFound )
	{
		if( undoManager != nil )
		{
			NSDictionary* undoDictionary = [NSDictionary dictionaryWithObjectsAndKeys:object,@"object",[NSNumber numberWithInt:index],@"index", undoManager, @"undoManager",nil];
			 [undoManager registerUndoWithTarget:self 
									selector:@selector(undoRemoveListItem:) object:undoDictionary];
			 [undoManager setActionName:title];
		}
		 [self removeObjectIdenticalTo:object];

	}

}


- (void)addObject:(id)anObject usingUndoManager:(NSUndoManager*)undoManager title:(NSString*)title

{
	
	if( undoManager != nil )
	{


		NSDictionary* undoDictionary = [NSDictionary dictionaryWithObjectsAndKeys: anObject,@"object", undoManager , @"undoManager", nil];
		[undoManager registerUndoWithTarget:self 
								   selector:@selector(undoInsertListItem:) object:undoDictionary];
				
		[undoManager setActionName:title];

	}
	
	[self addObject:(id)anObject ];
}
- (void)insertObject:(id)anObject atIndex:(unsigned)index usingUndoManager:(NSUndoManager*)undoManager  title:(NSString*)title

{
	if( undoManager != nil )
	{
		
		NSDictionary* undoDictionary = [NSDictionary dictionaryWithObjectsAndKeys: anObject,@"object", undoManager , @"undoManager", nil];
		[undoManager registerUndoWithTarget:self 
								   selector:@selector(undoInsertListItem:) object:undoDictionary];
		
		[undoManager setActionName:title];

	}
	
	
	[self insertObject:(id)anObject atIndex:(unsigned)index];
}


@end

@interface NSUndoManager (FixStrangeJapanese)
@end

@implementation NSUndoManager (FixStrangeJapanese)
- (NSString *)undoMenuTitleForUndoActionName:(NSString *)actionName
{
	if( actionName == nil ) return NSLocalizedString(@"Undo",@"");
	return [NSString stringWithFormat:NSLocalizedString(@"Undo %@",@""), actionName];
}

- (NSString *)redoMenuTitleForUndoActionName:(NSString *)actionName
{
	if( actionName == nil ) return NSLocalizedString(@"Redo",@"");
	return [NSString stringWithFormat:NSLocalizedString(@"Redo %@",@""), actionName];
}
@end
