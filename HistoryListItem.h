//
//  FolderListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ListItem.h"

@interface HistoryListItem : ListItem {

	NSMutableArray* children;
	int historyCount;
}
+ (void)initialize;
+(HistoryListItem*)sharedHistoryListItem;
- (id) init ;
-(void)dealloc;
- (void)encodeWithCoder:(NSCoder *)encoder;
-(void)addHistory:(ListItem*)history;
- (id)initWithCoder:(NSCoder *)coder;
- (id)copyWithZone:(NSZone *)zone;
-(NSMutableArray*)children;
-(void)removeItemFromChildren:(ListItem*)item;
-(NSString*)kind ;
-(NSImage*)icon;
-(void )setQuery:(id)value;
-(void )setChildren:(id)value;
-(NSString*)query ;
-(int)numberOfChildren;
-(BOOL)canExpand;
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController;
-(void)finishForAdditionalQuery:(id)result;
-(void)search;
- (void)contextualMenu:(NSMenu*)menu;
-(void)clearHistory;



@end
