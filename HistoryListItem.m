//
//  HistoryListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "HistoryListItem.h"

//#define DEFAULT_HISTORY_COUNT 99

@implementation HistoryListItem

HistoryListItem *sharedHistoryListItem = nil;


+ (void)initialize
{
    if ( self == [HistoryListItem class] ) {

		sharedHistoryListItem = [[HistoryListItem alloc] init];
	}
}
+(HistoryListItem*)sharedHistoryListItem
{
	return sharedHistoryListItem;
}

- (id) init {
	self = [super init];
	if (self != nil) {

		children = [[NSMutableArray alloc] init];
		contents = nil;
		
		name = [NSLocalizedString(@"History",@"") retain];
		canDelete = NO;
		canMove = NO;
		canDrop = NO;
		canSelect = NO;
		canExpand = YES;
		
		icon = nil;
		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		id historyCountNumber = [ud objectForKey:@"historyCount"];
		if( historyCountNumber == nil )
		{
			[ud setInteger:20 forKey:@"historyCount"];
			historyCount = 20;
		}else
		{
			historyCount = [historyCountNumber intValue];

		}
		
		
	}
	return self;
}

-(void)dealloc
{
	[children release];	
	
	[super dealloc];
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
	
	
	[super encodeWithCoder: encoder];
	
	[encoder encodeObject:		children					forKey:@"children"];

	[encoder encodeInt:		historyCount					forKey:@"historyCount"];

	
	
	return;
}

-(void)addHistory:(ListItem*)history
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	int historyCountNumber = [ud integerForKey:@"historyCount"];

	
	if( [ud boolForKey:@"historyOrder"] )
	{
		[children addObject: history ];
		if( [children count] > historyCountNumber ) [children removeObjectAtIndex:0];

	}else
	{
		[children insertObject: history atIndex:0];
		if( [children count] > historyCountNumber ) [children removeLastObject];
	}
	[self postNotification];

}

- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		children = [[NSMutableArray alloc] init];
		contents = nil;

		canDelete = NO;
		canMove = NO;
		canDrop = NO;
		canSelect = NO;
		canExpand = YES;
		
		icon = nil;
		
		
		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		id historyCountNumber = [ud objectForKey:@"historyCount"];
		if( historyCountNumber == nil )
		{
			[ud setInteger:20 forKey:@"historyCount"];
			historyCount = 20;
		}else
		{
			historyCount = [historyCountNumber intValue];
			
		}
		
		
		NSArray* array = [coder decodeObjectForKey:@"children"];
		
		if( array != nil && [array count] > 0 )
			[children addObjectsFromArray: array];
		
		[sharedHistoryListItem release];
		sharedHistoryListItem = self;
		
	}
	return self;
	
	
}


- (id)copyWithZone:(NSZone *)zone
{
	
	HistoryListItem* copy = [[HistoryListItem alloc] init];
	
	[[copy children] addObjectsFromArray: children];
	
	
	return copy;
}

-(NSMutableArray*)children
{
	return children;	
}

-(void)removeItemFromChildren:(ListItem*)item
{
	[children removeObjectIdenticalTo: item usingUndoManager:UNDO_MANAGER
								title:[NSString stringWithFormat:NSLocalizedString(@"Delete %@",@""),[item name] ]];
	[children makeObjectsPerformSelector:@selector(removeItemFromChildren:) withObject:item];
}

-(NSString*)kind {return @"history";}

-(NSImage*)icon
{
	if( icon == nil )
		icon = [[NSImage imageNamed:@"History.tif"] retain];
	

	return [super icon];
	
}



-(void )setQuery:(id)value
{
}
-(void )setChildren:(id)value
{
}

-(NSString*)query {
	

	return nil;
}



-(int)numberOfChildren
{
	return [children count];	
}


-(BOOL)canExpand
{
	return ([children count]>0? YES:NO);
}



-(void)searchForArrayController:(NSArrayController*)arrayController withEndTarget:(id)aTarget andSelector:(SEL)aSelector
{

	[aTarget performSelector:aSelector];
	//do nothing;

	
}



-(void)searchWithEndTarget:(id)aTarget andSelector:(SEL)aSelector
{
	[aTarget performSelector:aSelector];
//do nothing;
}

/*
-(void)search
{
	if( isRenaming ) return;

	
	[contents release];
	contents = [[NSArray array] retain];
	
	[self finish:nil];
	
}*/



- (void)contextualMenu:(NSMenu*)menu{
	NSMenuItem* customMenuItem;
	
	customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Clear History",@"")
												action:@selector(clearHistory) keyEquivalent:@""];
	[customMenuItem setRepresentedObject:@"MN"];
	[customMenuItem setTarget:self];
	[customMenuItem autorelease];	
	
	[menu addItem:customMenuItem];
	
	[super contextualMenu:menu];
}
-(void)clearHistory
{

	[children removeAllObjects];
	[self postNotification];

}

@end
