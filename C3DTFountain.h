//
//  C3DTFountain.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Thu May 29 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTFountain.h,v 1.2 2003/06/22 09:38:11 pmanna Exp $
//
// $Log: C3DTFountain.h,v $
// Revision 1.2  2003/06/22 09:38:11  pmanna
// Modified to support ARB_point_parameters extension if present
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTGeometry.h"

typedef struct {
    _C3DTVector	position;
    _C3DTVector	direction;
    float		size;
    float		speed;
    float		life;
    float		fade;
} _C3DTParticle;

@interface C3DTFountain : C3DTGeometry {
    _C3DTParticle	*particles;
    int				_particleCount;
    _C3DTVector		_position;
    _C3DTVector		_direction;
    float			_size;
    float			_speed;

    BOOL			_usePoints;
}

+ (C3DTFountain *)fountainWithCount: (int)count position: (_C3DTVector)pos direction: (_C3DTVector)dir
                               size: (float)size speed: (float)speed;

- (id)initWithCount: (int)count position: (_C3DTVector)pos direction: (_C3DTVector)dir
               size: (float)size speed: (float)speed;

- (_C3DTVector)position;
- (void)setPosition: (_C3DTVector)aPos;

- (_C3DTVector)direction;
- (void)setDirection: (_C3DTVector)aDir;

- (float)size;
- (void)setSize: (float)aSize;

- (float)speed;
- (void)setSpeed: (float)aSpeed;

@end
