//
//  C3DTObject.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTObject.h,v 1.1.1.1 2003/06/10 18:09:24 pmanna Exp $
//
// $Log: C3DTObject.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:24  pmanna
// Initial import
//
//

#import <Foundation/Foundation.h>


@interface C3DTObject : NSObject <NSCoding> {
    NSString	*_name;
}

- (id)initWithName: (NSString *)name;
- (NSString *)name;
- (void)setName: (NSString *)newName;

- (void)encodeWithCoder:(NSCoder *)coder;
- (id)initWithCoder:(NSCoder *)coder;

@end
