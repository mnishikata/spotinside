//
//  C3DTInteractiveView.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue Jun 24 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTInteractiveView.h,v 1.1 2003/06/24 22:29:03 pmanna Exp $
//
// $Log: C3DTInteractiveView.h,v $
// Revision 1.1  2003/06/24 22:29:03  pmanna
// Initial release
//
//

#import "C3DTView.h"


@interface C3DTInteractiveView : C3DTView {

	BOOL needsCleaning;
}

@end
