//
//  WindowAnimation.h
//  Docs
//
//  Created by Masatoshi Nishikata on 07/12/07.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface WindowAnimation : NSAnimation {

	NSRect startRect;
	NSRect endRect;
	
	NSWindow* fullWindow;

	NSRect s_frame;
	NSRect e_frame;
	BOOL scaleDown;
	
	double scaleFactor;
	

	
}

@end
