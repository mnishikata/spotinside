//
//  SearchAgent.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/18.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SearchAgent.h"
#import "ThreadWorker.h"
#import "NSFileManagerExt.h"

@implementation SearchAgent

- (id)initWithEndTarget:(id)target andSelector:(id)selector keys:(NSArray*)keys
// keys ... is dynamically used by this agent
{
    self = [super init];
    if (self) {
		

		
		endTarget = target;
		endSelector = selector;
		keysToRetrieve = [keys retain];
		
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopSearch)
													 name:@"SearchAgentCancelRequestNotification"
												   object:self];
		
		cancel =NO;
		
		
    }
    return self;
}

-(void)end
{
	[self stopSearch];
	endTarget = nil;
	endSelector = nil;
	[keysToRetrieve release];
	keysToRetrieve = nil;
	
	[[NSNotificationCenter defaultCenter] removeObserver:self];

}


-(void)dealloc
{

	[[NSNotificationCenter defaultCenter] removeObserver:self];

	[super dealloc];
	
	
}

-(NSString*)escape:(NSString*)str
{
	if( str == nil ) str = @"";
	
	NSMutableString* mstr = [[[NSMutableString alloc] initWithString:str] autorelease];
	
	[mstr replaceOccurrencesOfString:@"\"" withString:@"\\\""
		   options:1
			 range:NSMakeRange(0,[mstr length])];

	[mstr replaceOccurrencesOfString:@"*" withString:@"\\*"
							 options:1
							   range:NSMakeRange(0,[mstr length])];

	return (NSString*)mstr;
}



#pragma mark -


-(NSMutableArray*)search:(NSString*)query format:(NSString*)format scope:(NSArray*)scope cancelPreviousSearch:(BOOL)cancelFlag target:(id)anEndTarget selector:(SEL)aSelector
{
	endTarget = [anEndTarget retain];
	endSelector = aSelector;
	
	//NSLog(@"search query %@,  format,%@  , scope %@",query,format,[scope description]);
	
	if( cancelFlag == YES)
		[self stopSearch];
	
	
	cancel = NO;
	
	if( format == nil || [format isEqualToString:@""] ) goto ERROR;
	
	


	
	
	query = [self escape:query];
	
	
	
	NSMutableString *predicateFormat = [NSMutableString stringWithString: format];
	[predicateFormat replaceOccurrencesOfString:@"%@"
									 withString:query
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0,[predicateFormat length])];
	
	{

		
		
		_query = MDQueryCreate (
								NULL,
								predicateFormat,
								nil, // value list
								[NSArray arrayWithObject:kMDQueryResultContentRelevance] // sort
								);	
		
		
		if( _query != nil )
		{
		
			if( _query != nil && scope != nil && [scope count] != 0 )
			{
				MDQuerySetSearchScope ( _query, scope,  0  );
				
			}else
			{
				MDQuerySetSearchScope ( _query, [NSArray arrayWithObject:kMDQueryScopeComputer ],  0  );
			}
			
			
			[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finish:) name:kMDQueryDidFinishNotification object:_query];
			
			
			
			MDQueryExecute ( _query, 0 );
		}
		
		return;
	}

	
	
	// Error
	
ERROR:
	NSBeep();
	
	_query = nil;


	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:kMDQueryDidFinishNotification
												  object:nil];
	

	
	if( [endTarget respondsToSelector:endSelector] )
	{
		[endTarget performSelectorOnMainThread:endSelector withObject:nil waitUntilDone:NO];
	}
			
	[endTarget release];
		

	
}




/*
-(NSMutableArray*)search:(NSString*)query format:(NSString*)format scope:(NSArray*)scope cancelPreviousSearch:(BOOL)cancelFlag
{
	////NSLog(@"search query %@,  format,%@  , scope %@",query,format,[scope description]);
	[[NSApp mainWindow] display];

	
	if( cancelFlag == YES)
		[self stopSearch];

	
	cancel = NO;
	
	

	NSPredicate *predicateToRun = nil;
	
	query = [self escape:query];
	
	
	
	NSMutableString *predicateFormat = [NSMutableString stringWithString: format];
	[predicateFormat replaceOccurrencesOfString:@"%@"
									 withString:query
										options:NSCaseInsensitiveSearch
										  range:NSMakeRange(0,[predicateFormat length])];
	

	 _query = MDQueryCreate (
							NULL,
							predicateFormat,
							nil, // value list
							[NSArray arrayWithObject:kMDQueryResultContentRelevance] // sort
							);	
	
	
	
	
	if( scope != nil && [scope count] != 0 )
	{
		MDQuerySetSearchScope ( _query, scope,  0  );
		
	}else
	{
		MDQuerySetSearchScope ( _query, [NSArray arrayWithObject:kMDQueryScopeComputer ],  0  );
	}
	

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finish:) name:kMDQueryDidFinishNotification object:_query];
	
	
	
	MDQueryExecute ( _query, 0 );
	searchCount++;

}*/


-(void)finish:(NSNotification*)notif
{
	//NSLog(@"Query Finished");
	
	MDQueryRef finishedQuery = [notif object];
	
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:kMDQueryDidFinishNotification
												  object:finishedQuery];
	
	//[self finishFinished:[self processFoundData:notif] ];
	
	
	[NSThread detachNewThreadSelector:@selector(thread:) toTarget:self withObject:notif];
}

-(void)thread:(id)object
{
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	NSMutableArray* findResult = [self processFoundData:object];
	
	//NSLog(@"findResult %d",[findResult count]);
			
	cancel = NO;
	
	
	if( [endTarget respondsToSelector:endSelector] )
	{
		[endTarget performSelectorOnMainThread:endSelector withObject:findResult waitUntilDone:NO];
	}
	
	[pool release];
	
	[endTarget release];
	
	//if( _query != nil )
	//CFRelease(_query);
	
}



-(NSMutableArray*)processFoundData:(NSNotification*)notif
{
	
	MDQueryRef finishedQuery = [notif object];

	
	MDItemRef miref;	
	CFIndex idx;
	
	NSMutableArray* findResult = [NSMutableArray array];
	// dictionaryからなるarray構造
	// dictionary には、kMDQueryResultContentRelevance, kMDItemPath
	
	idx = MDQueryGetResultCount(finishedQuery);
	
	
	if( idx == 0 ){
		return nil;	
		
		
	}else
	{
	
	
	//
	
	

	float max = 0.0;
	
	CFIndex hoge;
	for( hoge = 0; hoge < idx; hoge++ )
	{
		
		if( cancel == YES ) return nil;

		
		NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
		
		miref = MDQueryGetResultAtIndex( finishedQuery, hoge);
		
		
		if( miref != nil )
		{
			NSNumber* score = (NSNumber*)MDQueryGetAttributeValueOfResultAtIndex (
								finishedQuery, kMDQueryResultContentRelevance, hoge  );
			NSNumber* score2;
			
			if( score != nil )
			{
				
				score2 = [NSNumber numberWithFloat:[score floatValue]];
				
				CFRelease(score);
			}else
			{
				
				score2 = [NSNumber numberWithFloat:0];
			}
			
			
			
			
			NSMutableDictionary* newDictionary = [self extractAttributesFrom:miref];

			
			[newDictionary setObject:score2 forKey:kMDQueryResultContentRelevance];
			

			
			[findResult insertObject: newDictionary  atIndex:0];
			
			
			float x = [score2 floatValue];
			if( max < x ) max = x;
			
			CFRelease(miref);
		}
		
		[pool release];


	}
	
	
	
	//regulate score
	
	
	for( hoge = 0; hoge < [findResult count]; hoge++ )
	{		
		if( cancel == YES ) return nil;

		
		NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

		
		float x = [[[findResult objectAtIndex:hoge] objectForKey:kMDQueryResultContentRelevance] floatValue];
		
		x = x / max;
		
		
		[[findResult objectAtIndex:hoge] setObject:[NSNumber numberWithFloat:x] forKey:kMDQueryResultContentRelevance];
		

		[pool release];
	}
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	if( [ud boolForKey:@"calculatePackage"] && [keysToRetrieve containsObject:kMDItemFSSize] )
	{
		/// calc package size
		for( hoge = 0; hoge < [findResult count]; hoge++ )
		{		
			if( cancel == YES ) return nil;

			
			
			
			NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];

		
			NSArray* utis = [[findResult objectAtIndex:hoge] objectForKey:@"kMDItemContentTypeTree"];
			if( [utis containsObject:@"com.apple.package"] )
			{
					
				NSFileManager* fm = [NSFileManager defaultManager];
				unsigned long long size = [fm fileSizeAtPath:[[findResult objectAtIndex:hoge] objectForKey:kMDItemPath]];
				
				[[findResult objectAtIndex:hoge] setObject:[NSNumber numberWithUnsignedLongLong:size] forKey:kMDItemFSSize];
			}

			[pool release];
				
		}
	}
	
	

	}
	
	
	return findResult;
	
	

}





-(NSMutableDictionary*)extractAttributesFrom:(MDItemRef)ref
{
	NSMutableDictionary* newDictionary;
	
	NSMutableArray* tempAllKeys = [NSMutableArray arrayWithArray: keysToRetrieve];
	if( ! [tempAllKeys containsObject:kMDItemPath] )
		[tempAllKeys addObject:kMDItemPath];
	
	
	if( ! [tempAllKeys containsObject:kMDItemContentType] )
		[tempAllKeys addObject:kMDItemContentType];
	
	
	if( ! [tempAllKeys containsObject:@"kMDItemContentTypeTree"] )
		[tempAllKeys addObject:@"kMDItemContentTypeTree"];

	if( [tempAllKeys containsObject:@"TagBotTagColumn.SpotInsideInternalAttribute"] )
		[tempAllKeys addObject:kMDItemFinderComment];

	
	
	
	id value = MDItemCopyAttributes(ref, tempAllKeys );
	
	if( value != nil )
	{
		newDictionary = [NSMutableDictionary dictionaryWithDictionary: value];
		CFRelease(value);

	}else
	{
		newDictionary = [NSMutableDictionary  dictionary];
	}
	
	
	return newDictionary;
}




-(void)stopSearch
{
	
	if( _query != nil )
	{
		MDQueryStop(_query);
		//NSLog(@"stop");
		cancel = YES;
	}
	
	

	
	//	[[NSNotificationCenter defaultCenter] removeObserver:self  name:kMDQueryDidFinishNotification object:nil];
	
//	MDQueryStop (_query  );	
	
}


@end
