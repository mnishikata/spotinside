//
//  NSFileManagerExt.h
//  Jikeiretsu
//
//  Created by Masatoshi Nishikata on 07/03/05.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface NSFileManager (extension) 
-(unsigned long long)fileSizeAtPath:(NSString*)path;
+(BOOL)createDirectoryIfNotExists:(NSString*)path;
+(BOOL)fileExistsAtPath:(NSString*)path;

@end
