#import "OutlineDelegate.h"
#import "ImageAndTextCell.h"
#import "NDAlias.h"
#import "SearchAgent.h"
#import "MyDocument.h"

#import "ListItem.h"
#import "AliasListItem.h"
#import "SmartFolderListItem.h"
#import "FolderListItem.h"
#import "TagListItem.h"
#import "DefaultListItem.h"
#import "TooltipWindow.h"

#define OutlineRowType @"outline"
#define OutlineIndexesType @"outlineIndexes"
#define OutlineRowTypes [NSArray arrayWithObjects:@"outline", @"outlineIndexes", @"foundItem", NSFilenamesPboardType, nil]
#define FoundItem @"foundItem"


#define OutlineChangedNotification @"OutlineChangedNotification"


/*
 
 NSMutableArray dataArray  (organiserArray in AppController)
 
 @"name"
 @"ndalias"
 @"kind" {folder, item, result, missing, etc}
 @"children"
 @"uniqueCode"
 
 
 @"queryFormat"
 
 @"icon" // volatile
 
 @"undeletable" {YES}
 @"immovable" {YES}
 @"donotAcceptDrop" {YES}
 @"dropNotification" {notificationName}

 
 */


@implementation OutlineDelegate

-(void)postOutlineChangedNotification
{

	///[[NSNotificationCenter defaultCenter] postNotificationName:OutlineChangedNotification object:outlineView];
}



- (void)awakeFromNib {
	
	[windowUIObjectController retain]; //in case this dealloc before me
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	id obj = [ud objectForKey:@"counterMode"];
	if( obj == nil )
	{
		[ud setInteger:1 forKey:@"counterMode"];
	}
	
	
	
	dataArray = [[NSApp delegate] organiserArray];
	
	//dataArray = [[NSMutableArray alloc] init];
	


	
		
	[outlineView  registerForDraggedTypes:OutlineRowTypes];
	
	//[outlineView setDoubleAction:@selector(launch:)];



	
	
	// table cell
	
	NSTableColumn *tableColumn = nil;
	ImageAndTextCell *imageAndTextCell = nil;
	NSButtonCell *buttonCell = nil;
	
	// Insert custom cell types into the table view, the standard one does text only.
	// We want one column to have text and images, and one to have check boxes.
	tableColumn = [outlineView tableColumnWithIdentifier: @"C1"];
	imageAndTextCell = [[[ImageAndTextCell alloc] init] autorelease];
	[imageAndTextCell setEditable: YES];
	[imageAndTextCell setLineBreakMode: 	NSLineBreakByTruncatingTail];

	
	//[imageAndTextCell setImage:docIcon ];
	
	[tableColumn setDataCell:imageAndTextCell];

	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(updateOutlineView:)
												 name:@"OutlineChangedNotification"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(outlineViewSelectionDidChange:)
												 name:@"SourceReselectRequestNotification"
											   object:nil];


	/*
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(listItemDidFinishSearching:)
												 name:@"ListItemDidFinishSearching"
											   object:nil];
	*/
	
	//expand
	//[outlineView reloadData];
	
	[outlineView reloadData];
	[dataArray makeObjectsPerformSelector:@selector(expandOrCollapseInView:) withObject:outlineView];

	// tooltip
	
	tooltip = [[TooltipWindow alloc] init];
    //[TooltipWindow setDefaultDuration:3.0];
	
	
	///set home position
	[myDocument focusField:self];
	[outlineView setAllowsEmptySelection:NO];

	
	
}


-(void)dealloc
{
	//NSLog(@"delegate dealloc");
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[windowUIObjectController release];
	
	[super dealloc];
}




-(void)listItemDidFinishSearching:(id)sender
{
		

	[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:NO] forKey:@"isSearching"];
	
	int v_count =  [[findResult content] count];
	NSString* string = [NSString stringWithFormat:NSLocalizedString(@"%d files",@"") ,v_count];
	
	[[windowUIObjectController selection] setValue:string forKey:@"resultCount"];

	
	///check if the window is displayed
	if( ![[outlineView window] isVisible] ) return;

	
	
	// when window is displayed....
	
	[findResult rearrangeObjects];
	[outlineView reloadData];
	
	//selectFirstRow
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	if( [ud boolForKey:@"selectFirstRow"] && [sender contents] > 0)
	{
		//[[outlineView windowForSheet] makeFirstResponder :table];
		
		NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:0];
		[table selectRowIndexes:indexes byExtendingSelection:NO];
	}
	
	//[flowView reloadData];
	
}
-(void)updateOutlineView:(NSNotification*)notif
{
	
	//NSLog(@"updateOutlineView");
	
	/*
	id item = [outlineView itemAtRow:[outlineView selectedRow]];
	if( item != nil )
		[findResult setFilterPredicate:[item filterPredicate]];
*/
	
	[findResult rearrangeObjects];

	
	[outlineView reloadData];
	[outlineView display];
	
}
- (IBAction)addTagger:(id)sender
{
	TagListItem* item = [[TagListItem alloc] init];
	
	[dataArray addObject:item usingUndoManager:UNDO_MANAGER
				   title: NSLocalizedString(@"Add New TagBot Tag",@"") ];

	[item release];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];

	NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:[outlineView numberOfRows]-1];
	[outlineView selectRowIndexes:indexes byExtendingSelection:NO];
	
	
	[outlineView editColumn:0 row:[outlineView numberOfRows]-1 withEvent:nil select:YES];
	
	
}

- (IBAction)addFolder:(id)sender
{

	FolderListItem* item = [[FolderListItem alloc] init];
	
	[dataArray addObject:item usingUndoManager:UNDO_MANAGER
				   title: NSLocalizedString(@"Add New Group",@"") ];

	[item release];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];

	NSIndexSet* indexes = [NSIndexSet indexSetWithIndex:[outlineView numberOfRows]-1];
	[outlineView selectRowIndexes:indexes byExtendingSelection:NO];

	
	[outlineView editColumn:0 row:[outlineView numberOfRows]-1 withEvent:nil select:YES];
		
}

- (IBAction)addSmartFolder:(id)sender
{
	NSRunAlertPanel( NSLocalizedString(@"Sorry...",@""),
					 NSLocalizedString(@"To add Smart Folder, create one in Finder and drag & drop it onto the Source list.",@""),nil,nil,nil);	
}

/*
- (IBAction)addResult:(id)sender
{
	ListItem* item = [[ListItem alloc] init];
	
	[item setName:[myDocument queryTitle]];
//	[item setQueryFormat:[myDocument queryFormat]];

	[dataArray addObject:item
		usingUndoManager:UNDO_MANAGER];
	[item release];
	
	[outlineView reloadData];
	[self postOutlineChangedNotification];
	


}*/


- (IBAction)removeItem:(id)sender
{
	if( [outlineView selectedRow] == -1 ) return;
	
	[[outlineView window] endEditingFor:nil];
	
	id item = [outlineView itemAtRow: [outlineView selectedRow]];

	if( ![item canDelete] ) return;
	
	[dataArray makeObjectsPerformSelector:@selector(removeItemFromChildren:) withObject:item];
	[dataArray removeObjectIdenticalTo: item
					  usingUndoManager:UNDO_MANAGER
								 title:[NSString stringWithFormat:NSLocalizedString(@"Delete %@",@"") ,[item name] ]];

				
	[outlineView reloadData];
	[self postOutlineChangedNotification];

}
-(IBAction)reload:(id)sender
{
	if( [outlineView selectedRow] == -1 ) return;
	
	[[findResult content] removeAllObjects];
	
	[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:YES] forKey:@"isSearching"];

	id item = [outlineView itemAtRow: [outlineView selectedRow]];
	
	[item searchForArrayController:findResult withEndTarget:self andSelector:@selector(listItemDidFinishSearching:)];
	
	[outlineView reloadData];
}



-(IBAction)setting:(id)sender
{
	return;
	/*
	id item = [outlineView itemAtRow:[outlineView selectedRow]];

	if( [[item objectForKey:@"kind"] isEqualToString:@"result"] )
	{

			[[NSApp delegate] beginModal:item forWindow:[myDocument windowForSheet ]];
		
	}
	*/
}




#pragma mark -

- (void)outlineView:(NSOutlineView *)ov willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item
{
	
	NSAttributedString* attr = [cell attributedStringValue];
	
	NSMutableAttributedString* mattr = [[NSMutableAttributedString alloc] initWithAttributedString: attr];
	[mattr addAttribute:NSForegroundColorAttributeName
				  value:[NSColor blackColor] range:NSMakeRange(0,[mattr length])];
	[cell setAttributedStringValue: mattr];
	[mattr release];
	
		
	[cell setImage:[item icon]];

		
	
	int counterMode = [[NSUserDefaults standardUserDefaults] integerForKey:@"counterMode"];
	if( counterMode == 0 ) {
		[cell setCounter:0];
		return;
	}
	
	else if( counterMode ==1 || counterMode == 2 )
	{
	
	id obj;
	
	if( counterMode == 1 ) obj = [item children];
	if( counterMode == 2 ) obj = [item contents];

	if( obj == nil ) 
	{
		[cell setCounter:0];
	}
	else
	{
		[cell setCounter:[obj count]];
	}
	}else if( counterMode == 3 )
	{
		[cell setBothCounterGroup:[[item children] count] andResult	:[[item contents] count]];
	}
	

}

- (int)outlineView:(NSOutlineView *)ov numberOfAllChildrenOfItem:(id)item 
{
	int counter = 0;
	
	if(item == nil)
	{
		
		unsigned hoge;
		for( hoge = 0; hoge < [dataArray count]; hoge ++ )
		{
			
			counter += [self outlineView:ov  numberOfAllChildrenOfItem:[dataArray objectAtIndex:hoge]];
			
			counter++;
			
		}
		
	}else
	{

		NSArray* childrenArray = [item children];
		unsigned hoge;
		for( hoge = 0; hoge < [childrenArray count]; hoge ++ )
		{
			
			counter += [self outlineView:ov  numberOfAllChildrenOfItem:[childrenArray objectAtIndex:hoge]];
			
			counter++;
			
		}	
		
		
	}
	
    return counter;
}


- (int)outlineView:(NSOutlineView *)ov numberOfChildrenOfItem:(id)item 
{
	
    return (item == nil) ? [dataArray count] : [item numberOfChildren];
}

- (BOOL)outlineView:(NSOutlineView *)ov isItemExpandable:(id)item 
{
	return [item canExpand];

	
}

- (id)outlineView:(NSOutlineView *)ov child:(int)index ofItem:(id)item 
{
    if (item == nil) return [dataArray objectAtIndex:index];
	
	NSArray* children = [item children];
	
	if( [children count] <= index ) return nil;
	
	return [children objectAtIndex:index];
	
}

- (id)outlineView:(NSOutlineView *)ov objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item 
{
    return (item == nil) ? @"" : [item name];
}


- (void)outlineView:(NSOutlineView *)ov setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item
{
	if( [object isKindOfClass:[NSAttributedString class]] )
		object = [object string];
		
	[item setName:object ];

	if( [item isKindOfClass:[TagListItem class]] )
	{
		if( [item contents] == nil )
		{
			[item searchForArrayController:findResult withEndTarget:self andSelector:@selector(listItemDidFinishSearching:)];

		}
	}
}



- (BOOL)outlineView:(NSOutlineView *)ov acceptDrop:(id <NSDraggingInfo>)info item:(id)item childIndex:(int)index
{
	[tooltip hideToolTip];
	
	if( item != nil && ![item canDrop] ) return NO;
	
	
	
	
	
	NSPasteboard* pb = [info draggingPasteboard];

	

				
	//// drag from table
	
	/*
	 			name, @"filename",
	 path, @"path",
	 displayname, @"displayname",
	 score, @"score",
	 
	 */
	if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:NSFilenamesPboardType] )
	{
		NSArray *files = [pb propertyListForType:NSFilenamesPboardType];
		if( files == nil || [files count] == 0 ) return NO;
		
		
		int hoge;
		for( hoge = 0; hoge < [files count]; hoge ++ )
		{
			
			//
			NSString* path = [files objectAtIndex:hoge] ;

			id newItem;
			
			if( [[path pathExtension] isEqualToString:@"savedSearch"] )
				newItem = [[[SmartFolderListItem alloc] init] autorelease];
			else
				newItem = [[[AliasListItem alloc] init] autorelease];
			
			[newItem setAliasWithPath: path ];
			
			
			
			if( item == nil )
			{
				if( index == -1 )
					[dataArray addObject:newItem
						usingUndoManager:UNDO_MANAGER
								   title:[NSString stringWithFormat: NSLocalizedString(@"Add %@",@"") ,[newItem name] ]];

				else
				{
					[dataArray insertObject:newItem atIndex:index 
						usingUndoManager:UNDO_MANAGER
									  title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[newItem name] ]];


				}
				
			}else
			{
				if( [item isKindOfClass:[FolderListItem class]] )
				{
				
					if( index == -1 )
						[[item children] addObject:newItem
							usingUndoManager:UNDO_MANAGER
											 title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[newItem name] ]];

					else
					{
						[[item children] insertObject:newItem atIndex:index 
							usingUndoManager:UNDO_MANAGER
												title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[newItem name] ]];

						
					}
				
				}else if( [item isKindOfClass:[TagListItem class]] )
				{
					[item dropItem: newItem];
				}
			}
			
			

		}

		[outlineView reloadData];

		
		[self postOutlineChangedNotification];
		
		return YES;
		
		
	}
	
	
	
	//// drag from outline
	
	[item retain];
	

	
	NSDragOperation op = [info draggingSourceOperationMask];
	NSArray* movedItems = [NSArray array];
	
	if( [[pb availableTypeFromArray:OutlineRowTypes] isEqualToString:OutlineRowType] )
	{
		movedItems = [NSKeyedUnarchiver unarchiveObjectWithData:[pb dataForType:OutlineRowType]];
		
		id object = [movedItems objectAtIndex:0];
		
		[object setDragging:NO];
		
		if( item == nil )
		{
			if( index == -1 )
				[dataArray addObject:object
					usingUndoManager:UNDO_MANAGER
							   title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[object name] ]];

			else
			{
				[dataArray insertObject:object atIndex:index 
					usingUndoManager:UNDO_MANAGER
								  title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[object name] ]];

				
			}
			
		}
		else if( [item isKindOfClass: [FolderListItem class] ] )
		{
			if( index == -1 )
				[[item children] addObject:object
					usingUndoManager:UNDO_MANAGER
									 title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[object name] ]];

			else
			{
				[[item children] insertObject:object atIndex:index 
					usingUndoManager:UNDO_MANAGER
										title:[NSString stringWithFormat:NSLocalizedString(@"Add %@",@"") ,[object name] ]];

				
			}
		}
		

		[item dropItem:object];
		
		[outlineView reloadData];

		
		if( [item isKindOfClass: [TagListItem class] ] )
		{
			return;
		}
		
		
		
		// ajust expand/collapse
		[object expandOrCollapseInView:outlineView];
		
		//remove
		
		
		int rowInView;
		for( rowInView = 0; rowInView < [outlineView numberOfRows]; rowInView++ )
		{
			id item = [outlineView itemAtRow:rowInView];
			if( [item isDragging] && ![item hasChild:object])
			{
				[dataArray makeObjectsPerformSelector:@selector(removeItemFromChildren:) withObject:item];
				[dataArray removeObjectIdenticalTo: item
		  usingUndoManager:UNDO_MANAGER
					 title:[NSString stringWithFormat:NSLocalizedString(@"Move %@",@"") ,[item name] ]];

				[UNDO_MANAGER setActionName: [NSString stringWithFormat:NSLocalizedString(@"Move %@",@"") ,[item name] ]];
				break;
			}
		}
		
		
		


	}
	
	
	//
	//set selection
	[outlineView reloadData];
	
	NSMutableIndexSet* indexes = [NSMutableIndexSet indexSet];
	
	
	int hoge;
	for( hoge = 0; hoge < [ movedItems count]; hoge++ )
	{
		[indexes addIndex:[outlineView rowForItem:[movedItems objectAtIndex:hoge] ]];
		
	}
	////NSLog(@"selection %@",[indexes description]);
	[outlineView selectRowIndexes:indexes byExtendingSelection:NO];
	
	//[self outlineViewSelectionDidChange:NULL];
	
	
	
//	[self updateOutlineMemo];
	
	[outlineView reloadData];
	[item release];
	
	[self postOutlineChangedNotification];

	return YES;
	
}






-(NSMutableArray*)childrenItemsOf:(id)item
{
	NSMutableArray* array = [NSMutableArray array];
	
	[array addObject:item];
	
	unsigned hoge;
	
	for( hoge = 0; hoge < [[item children] count]; hoge++ )
	{
		[array addObjectsFromArray:[self childrenItemsOf:[[item children] objectAtIndex:hoge]]  ];
	}
	
	return array;
}

- (BOOL)outlineView:(NSOutlineView *)ov writeItems:(NSArray *)items toPasteboard:(NSPasteboard *)pboard
{
	int hoge;
	for( hoge = 0; hoge < [items count];hoge++ )
	{
		id item = [items objectAtIndex:hoge];
		
		if( ![item canMove] )
			return NO;

	}
	
	[items makeObjectsPerformSelector:@selector(setDragging:) withObject:[NSNumber numberWithBool:YES]];

	
	[pboard declareTypes:OutlineRowTypes owner:self];
	////NSLog(@"writeItems");

	
	[pboard setData:[NSKeyedArchiver archivedDataWithRootObject:items] forType:OutlineRowType];
	//[pboard setPropertyList: indexes  forType:OutlineIndexesType];
	
	
	
	return YES;
}

- (NSDragOperation)outlineView:(NSOutlineView *)ov validateDrop:(id <NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(int)index
{
	
	NSPasteboard *pboard = [info draggingPasteboard];
	NSDragOperation op = [info draggingSourceOperationMask];
	

	
	if( item == nil  ) {
		[tooltip hideToolTip];
		return NSDragOperationGeneric;
	}
	
	if( ![item canDrop] ) {
		[tooltip hideToolTip];
		return NSDragOperationNone;
	}

	
	if( [item isKindOfClass:[TagListItem class]] )
	{
		int modKey = GetCurrentKeyModifiers( );
		////NSLog(@"%d",modKey);
		if(  (modKey | 1024) == (2048 | 1024) ) // option
		{
			NSString* string = [NSString stringWithFormat: NSLocalizedString(@"Remove Tag \"%@\"",@"") ,[item name]];	
			[tooltip showToolTip:string];

			
		}else
		{
			
			
		NSString* string = [NSString stringWithFormat:NSLocalizedString(@"Add Tag \"%@\"\n(Press Option to remove)" ,@""),[item name]];	
		[tooltip showToolTip:string];
		}
		
	}
	
	
	return NSDragOperationGeneric;
}

///
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item
{
	BOOL flag = [item canExpand];
	
	if( flag ) [item setExpanded:YES];
	
	return flag;
	
}


- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item
{
	[item setExpanded:NO];

	
	return YES;
	
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item
{
	return [item canSelect];
	
}


-(NSMutableArray*)expandChildrenFor:(NSMutableArray*)array
{
	NSMutableArray* returnValue = [NSMutableArray array];
	int hoge;
	for( hoge = 0; hoge < [array count]; hoge++ )
	{
		
		NSMutableDictionary* item = [array objectAtIndex:hoge];
		
		[returnValue addObject:item];
		id childrenArray = [item children];
		if( childrenArray != nil  )
		{
			[returnValue addObjectsFromArray:[self expandChildrenFor:childrenArray ]];
		}
		
	}
	
	return returnValue;
}




#pragma mark -

- (void)outlineViewSelectionDidChange:(NSNotification *)aNotification
{
	[[findResult content] removeAllObjects];
	[findResult setFilterPredicate:nil];
	[findResult rearrangeObjects];
	
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"SearchAgentCancelRequestNotification"
														object:nil];

	
	id item = [outlineView itemAtRow:[outlineView selectedRow]];
	
	if( item == nil )
	{
	//	[myDocument setDefault];

	}
	else 
	{
		NSArray* contents = [item contents];
		//NSLog(@"selected item : contents %d",[contents count] );

		[[windowUIObjectController selection] setValue:[NSNumber numberWithBool:YES] forKey:@"isSearching"];

		if( [[NSUserDefaults standardUserDefaults] boolForKey:@"useCache"] )
		{
			if( contents == nil )
			{
				[item searchForArrayController:findResult withEndTarget:self andSelector:@selector(listItemDidFinishSearching:)];
			}
			else
			{
				[findResult addObjects:[item contents] ];
				[self listItemDidFinishSearching:item];
		
			}
		}else
		{
			[item searchForArrayController:findResult withEndTarget:self andSelector:@selector(listItemDidFinishSearching:)];


		}

	}

	NSString* template = [item queryTemplate];
	NSString* scope = [item scope];
	NSString* queryString = [item query];
	NSArray* keywords = [item keywords];
	
	if( template == nil || [template isEqualToString:@""] )
	{
		template = [[NSUserDefaults standardUserDefaults] objectForKey:@"defaultQueryTemplate"];	
	}
	
	[[windowUIObjectController selection] setValue:template forKey:@"queryTemplate"];
	[[windowUIObjectController selection] setValue:scope forKey:@"scope"];
	[[windowUIObjectController selection] setValue:queryString forKey:@"query"];
	[[windowUIObjectController selection] setValue : keywords forKey:@"searchKeywords"];
}



@end
