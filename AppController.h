/* AppController */

#import <Cocoa/Cocoa.h>
#import "TagManager.h"

@interface AppController : NSObject
{
	NSMutableArray* organiserArray;
	

	NSArray* metaListArray;
	NSMutableArray* allKeys;

	TagManager* tagManager;
	
	/// Test
	
	IBOutlet id testDocField;
	IBOutlet id testPluginField;
	IBOutlet id testPreviewBySpotInsideField;
	IBOutlet id testPreviewByUserField;
	IBOutlet id testOtherInfo;

	IBOutlet id tagSample;
}
-(int)OSVersion;

@end
