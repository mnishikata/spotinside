//
//  PointToMilTransformer.h
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 06/07/12.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface KeyedUnarchiveFromDataTransformer : NSValueTransformer {

}
+(Class)transformedValueClass;
+(BOOL)allowsReverseTransformation;
-(id)transformedValue:(id)value;
-(id)reverseTransformedValue:(id)value;

@end
