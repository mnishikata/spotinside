//
//  SearchResultTableView.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/11/20.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "SearchResultTableView.h"
#import "SearchResultTableSizeValueTransformer.h"
#import "SearchResultTableDateCell.h"
#import "SearchResultTableScoreCell.h"
#import "SearchResultTableIconCell.h"
#import "TagBotTagsValueTransformer.h"


//#import "QuickLookPrivateHeader.h"

#define QLPreviewPanel NSClassFromString(@"QLPreviewPanel")



@implementation SearchResultTableView

+ (void)initialize {
    if (self == [SearchResultTableView class]) {   
		

		NSValueTransformer *mbTrans = [[SearchResultTableSizeValueTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"SearchResultTableSizeValueTransformer"];
		[mbTrans release];
		
		
		mbTrans = [[TagBotTagsValueTransformer alloc] init];
		[NSValueTransformer setValueTransformer:mbTrans forName:@"TagBotTagsValueTransformer"];
		[mbTrans release];

		////NSLog(@"SearchResultTableView" );
		
		
		int osversion = [[NSApp delegate] OSVersion];
		if( osversion >=  105 )
		{

			if([[NSBundle bundleWithPath:@"/System/Library/PrivateFrameworks/QuickLookUI.framework"] load])
			{
				//NSLog(@"Quick Look loaded!");
				[[[QLPreviewPanel sharedPreviewPanel] delegate] setDelegate:self];

			}
		}
    }
}




- (void) dealloc {
	[iconCache release]; iconCache = nil;
	[super dealloc];
}

/*
- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)rect {
    // Drawing code here.
}*/

-(void)awakeFromNib{

				iconCache = [[NSMutableDictionary alloc] init];	

//	[self registerForDraggedTypes:[NSArray arrayWithObject:NSStringPboardType]];
}

-(void)addColumnWithIdentifier:(NSString*)identifier title:(NSString*)title bindToObject:(id)object 
/*
 identifier : metadata attribute
 title : if this is nil, it is set automatically
 transformer : can be nil
 
 */
{
	
	if( identifier == nil ) return;
	if( [identifier isEqualToString:@""] ) return;
	
	
	[self setColumnAutoresizingStyle:NSTableViewNoColumnAutoresizing];

	
	
	NSString* keyPath = [NSString stringWithFormat:@"arrangedObjects.%@",identifier];

	
	// object = arrayController, keyPath = @"arrangedObjects.kmditemsomething"

	NSTableColumn* tb = [self tableColumnWithIdentifier:identifier];
	if( tb == nil )
	{
		//set binding
		
		NSNumber* BOOLYES = [NSNumber numberWithBool:YES];
		NSMutableDictionary*  dic = [NSMutableDictionary dictionaryWithObjectsAndKeys: BOOLYES, NSAllowsEditingMultipleValuesSelectionBindingOption,
			BOOLYES, NSCreatesSortDescriptorBindingOption,
			BOOLYES, NSRaisesForNotApplicableKeysBindingOption, nil];
		
		
		NSTableColumn* tb = [[[NSTableColumn alloc] initWithIdentifier:identifier] autorelease];
	
		if( [identifier isEqualToString:kMDQueryResultContentRelevance] )
		{
			title = NSLocalizedString(@"Score",@"");
			[tb setWidth:48];
			[tb setResizingMask:NSTableColumnNoResizing];
			
		}
		
		else if( [identifier isEqualToString:@"IconColumn.SpotInsideInternalAttribute"] )
		{
			title = NSLocalizedString(@"",@"");
			[tb setWidth:16];
			[tb setResizingMask:NSTableColumnNoResizing];
			
		}
		
		
		else if( [identifier isEqualToString:@"TagBotTagColumn.SpotInsideInternalAttribute"] )
			title = NSLocalizedString(@"TagBot Tags",@"");

		if( title == nil )
		{
			////NSLog(@"<1 %@>",identifier);
			////NSLog(@"%@",MDSchemaCopyDisplayNameForAttribute(kMDItemAlbum));
			
			title =  MDSchemaCopyDisplayNameForAttribute(identifier);	

			
			if( title == nil )
			{
				title = identifier;
			}
			else [title autorelease];
			
			

		}
		////NSLog(@"</1 %@>",title);

		[[tb headerCell] setTitle:title];


		//check attribute type
		NSDictionary* metadic = MDSchemaCopyMetaAttributesForAttribute(identifier);
		////NSLog(@"%@",metadic);
		//kMDAttributeType
		//(CFNumberRef, CFTypeId)

		/*
		CFTypeId type = [[dic objectForKey:@"kMDAttributeType"] intValue];
		if( type == CFDataGetTypeID() ) //data
		{
			
		}*/
		
		
		//Customise cell
		
		////NSLog(@"%@",[metadic description]);
	
		if( [[metadic objectForKey:@"kMDAttributeMultiValued"] boolValue] == NO )
		{
			NSTextFieldCell* cell = [[[NSTextFieldCell alloc] init ] autorelease];
			[cell setFont:[NSFont systemFontOfSize:[NSFont systemFontSizeForControlSize:NSSmallControlSize]]];
			[tb setDataCell:cell];
			
		}else //multiple
		{
			NSTokenFieldCell* cell = [[[NSTokenFieldCell alloc] init ] autorelease];
			[cell setControlSize:NSSmallControlSize];
			[cell setFont:[NSFont systemFontOfSize:[NSFont systemFontSizeForControlSize:NSSmallControlSize]]];
			[tb setDataCell:cell];	
		} 
		
		
		CFTypeID type = [[metadic objectForKey:@"kMDAttributeType"] intValue];
		if( type == CFDateGetTypeID() ) //date
		{
			SearchResultTableDateCell* cell = [[[SearchResultTableDateCell alloc] init ] autorelease];
			[cell setFont:[NSFont systemFontOfSize:[NSFont systemFontSizeForControlSize:NSSmallControlSize]]];
			
			[tb setDataCell:cell];
			
		}

			
		if( [identifier isEqualToString: kMDQueryResultContentRelevance ] )
		{
			SearchResultTableScoreCell* cell = [[[SearchResultTableScoreCell alloc] init ] autorelease];
			
			[tb setDataCell:cell];
			
		}
		
		
		if( [identifier isEqualToString: @"IconColumn.SpotInsideInternalAttribute" ] )
		{
			SearchResultTableIconCell* cell = [[[SearchResultTableIconCell alloc] init ] autorelease];
			[cell setOwnerTableView:self andCacheDictionary:iconCache];

			[tb setDataCell:cell];
			
			keyPath = @"arrangedObjects.kMDItemPath";
			
		}
		
		if( [identifier isEqualToString: @"TagBotTagColumn.SpotInsideInternalAttribute" ] )
		{
			
			keyPath = @"arrangedObjects.kMDItemFinderComment";
			
			[dic setObject:@"TagBotTagsValueTransformer" forKey:NSValueTransformerNameBindingOption ];

		}

		////
		

		if( metadic != nil ) CFRelease(metadic);


		
		
		//Customise Transformer
		
		
		if( [identifier isEqualToString: kMDItemFSSize ] ) 
		{
			[dic setObject:@"SearchResultTableSizeValueTransformer" forKey:NSValueTransformerNameBindingOption ];
		}
		
		
		
		
		[tb bind:@"value" toObject:object withKeyPath:keyPath options :dic];

		
	//[tb setSize:NSMakeSize(30,12)];/// #$%
		[self addTableColumn:tb];
		[tb setEditable:YES];

		[[tb dataCell] setLineBreakMode: NSLineBreakByTruncatingTail ];

		
		//set width		
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];		
		NSDictionary* udic = [ud objectForKey:identifier];
		if( udic != nil )
		{
			float width = [[udic objectForKey:@"width"] floatValue];
			[tb setWidth:width];
		}	
		
	}

	
	[self reloadData];
	[self setColumnAutoresizingStyle:NSTableViewUniformColumnAutoresizingStyle];
}
-(void)removeColumnWithIdentifier:(NSString*)identifier
{
	NSTableColumn* tb = [self tableColumnWithIdentifier:identifier];
	
	if( tb != nil )
	{
		[tb unbind:@"value"];
		[self removeTableColumn:tb];
	}
}


-(void)saveTableColumn
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSArray* columns = [self tableColumns];
	int hoge;
	for( hoge = 0; hoge < [self numberOfColumns]; hoge++ )
	{
		NSTableColumn* tb = [columns objectAtIndex:hoge];
		float width = [tb width];
		int idx = [self columnWithIdentifier:[tb identifier]];
	
		NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:width],@"width", [NSNumber numberWithInt:idx],@"index",nil];
		[ud setObject:dic forKey:[tb identifier]];
	}
	
}
/*
-(void)loadTableColumnPosition
{
	[self setAutoresizesAllColumnsToFit:NO];
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSMutableDictionary* sortDic = [NSMutableDictionary dictionary];
	
	NSArray* columns = [self tableColumns];
	int numberOfColumns = [self numberOfColumns];
	int hoge;
	for( hoge = 0; hoge < numberOfColumns; hoge++ )
	{
		NSTableColumn* tb = [columns objectAtIndex:hoge];
		int oldidx = [self columnWithIdentifier:[tb identifier]];

		NSDictionary* dic = [ud objectForKey:[tb identifier]];

		
		float width = [[dic objectForKey:@"width"] floatValue];
		int idx = [[dic objectForKey:@"index"] intValue];
		
		[tb setWidth:width];
		[sortDic setObject:tb forKey:[NSNumber numberWithInt:idx]];
		
		//[self removeTableColumn:tb];
	}	
	//NSLog(@"1");
	
	
	for( hoge = 0; hoge < numberOfColumns; hoge++ )
	{
		NSTableColumn* tb = [columns objectAtIndex:hoge];
		[self removeTableColumn:tb];

	}	
	
	
	//NSLog(@"2");

	
	for( hoge = 0; hoge < numberOfColumns; hoge++ )
	{
		
		NSTableColumn* tb = [sortDic objectForKey:[NSNumber numberWithInt:hoge]];
			
		
		
		[self addTableColumn:tb ];
		//[tb release];
	}
	//NSLog(@"3");

	[self setAutoresizesAllColumnsToFit:YES];
}
*/
- (int)numberOfRows
{
	int num = [super numberOfRows];
	
	if( num == 0 )
	{
		/*
		if( iconCache == nil || [iconCache count] != 0 )
		{
			[iconCache release];
			iconCache = [[NSMutableDictionary alloc] init];	
		}
		
		 */
	}
	return num;
}


- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	NSMenu* menu = [[[NSMenu alloc] initWithTitle:@""] autorelease];
	
	
	NSPoint mouseLoc = [[self window]  mouseLocationOutsideOfEventStream]; 
	mouseLoc = [self convertPoint:mouseLoc fromView:[[self window] contentView]];
	
	////DBLOG(@"c %@  ", NSStringFromPoint(mouseLoc));
	
	NSRange range;
	int columnIndex;
	int rowIndex;
	
	range = [self columnsInRect:NSMakeRect(mouseLoc.x, mouseLoc.y, 1,1)];
	if( range.length == 0 ) columnIndex = -1;
	else
		columnIndex = range.location;
	
	range = [self rowsInRect:NSMakeRect(mouseLoc.x, mouseLoc.y, 1,1)];
	if( range.length == 0 ) rowIndex = -1;
	else
		rowIndex = range.location;
	
	
	
	if( rowIndex != -1 )
	{
		NSDictionary* rep = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithInt:rowIndex],@"row", [NSNumber numberWithInt:columnIndex],@"column",nil];
		
		NSMenuItem* customMenuItem;
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Reveal in Finder",@"")
													action:@selector(reveal:) keyEquivalent:@""];
		[customMenuItem setRepresentedObject:rep];
		[customMenuItem setTarget:self];
		
		[menu addItem:customMenuItem];
		[customMenuItem release];
		//		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Open",@"")
													action:@selector(open:) keyEquivalent:@""];
		[customMenuItem setRepresentedObject:rep];
		[customMenuItem setTarget:self];
		
		[menu addItem:customMenuItem];
		[customMenuItem release];
		//		
		
		
		
		customMenuItem = [[NSMenuItem alloc] initWithTitle: NSLocalizedString(@"Copy Cell",@"")
													action:@selector(copyCell:) keyEquivalent:@""];
		[customMenuItem setRepresentedObject:rep];
		[customMenuItem setTarget:self];

		[menu addItem:customMenuItem];
		[customMenuItem release];

	}
	
	return menu;
	
}

-(void)reloadData
{
	if( [[self delegate] respondsToSelector:@selector(tableViewDidReloadData:)] )
		[[self delegate] tableViewDidReloadData:self];
	//NSLog(@"table view reload data");
	[super reloadData];	
}

-(void)reveal:(id)sender
{
	int row = [[[sender representedObject] objectForKey:@"row" ] intValue];
	
	NSString* path = [[[[[self delegate] findResult] arrangedObjects] objectAtIndex: row] objectForKey:kMDItemPath];
	
	if( path != nil )
		[[NSWorkspace sharedWorkspace] selectFile:path inFileViewerRootedAtPath: path];
	
}

-(void)open:(id)sender
{
	int row = [[[sender representedObject] objectForKey:@"row" ] intValue];
	
	NSString* path = [[[[[self delegate] findResult] arrangedObjects] objectAtIndex: row] objectForKey:kMDItemPath];
	
	if( path != nil )
		[[NSWorkspace sharedWorkspace] openFile:path];
	
}

-(void)copyCell:(id)sender
{
	
	int row = [[[sender representedObject] objectForKey:@"row" ] intValue];
	int column = [[[sender representedObject] objectForKey:@"column" ] intValue];

	
	NSDictionary* dict = [[[[self delegate] findResult] arrangedObjects] objectAtIndex: row];
	NSString* identifier = [[[self tableColumns] objectAtIndex:column] identifier]; 
		
	id value = [dict objectForKey: identifier];

	if( [value isKindOfClass:[NSNumber class]] )
	{
		value = [value stringValue];
		
	}
	
	if( [value isKindOfClass:[NSDate class]] )
	{
		value = [value description];
		
	}
	
	if( [value isKindOfClass:[NSArray class]] )
	{
		if( [value count] > 0 && [[value objectAtIndex:0] isKindOfClass:[NSString class]] )
		{
			
			value = [value componentsJoinedByString:@", "];
		}
	}
	
	
	if( [value isKindOfClass:[NSString class]] )
	{
	
		NSPasteboard* pboard = [NSPasteboard pasteboardWithName:NSGeneralPboard ];
		[pboard declareTypes:[NSArray arrayWithObjects: NSStringPboardType,nil ] owner:self];

		[pboard setString:value forType:NSStringPboardType];

	}else NSBeep();
}


#pragma mark QuickLook

-(void)showQuickLookPanelFromResults:(id)sender
{
	int osversion = [[NSApp delegate] OSVersion];
	if( osversion < 105 )
	{
		//NSBeep();
		return;
	}
	 
	[[[QLPreviewPanel sharedPreviewPanel] delegate] setDelegate:self];
	
	int row = [self selectedRow];
	if( row == -1 ) return;
	
	NSString* path = [[[[[self delegate] findResult] arrangedObjects] objectAtIndex: row] objectForKey:kMDItemPath];
	
	if( path != nil )
		
	{
		[[QLPreviewPanel sharedPreviewPanel] setURLs:[NSArray arrayWithObject:[NSURL fileURLWithPath:path]] currentIndex:0 preservingDisplayState:YES];
		
		[[QLPreviewPanel sharedPreviewPanel] makeKeyAndOrderFrontWithEffect:2];   // 1 = fade in 
	}
	
	
}
-(void)keyDown:(NSEvent*)event
{
	//NSLog(@"%d",[event keyCode]);
	
	if( [event keyCode] == 49 )
	{
		//[[[QLPreviewPanel sharedPreviewPanel] delegate] setDelegate:nil];
		[self showQuickLookPanelFromResults:nil];
	}
	
	else
		[super keyDown:event];
}

- (NSRect)previewPanel:(NSPanel*)panel frameForURL:(NSURL*)URL 
{
	int row = [self selectedRow ];
	if( row == -1 ) return NSZeroRect;
	
	NSRect rowRect = [self rectOfRow: row];
	
	rowRect = NSIntersectionRect(rowRect, [[self enclosingScrollView] documentVisibleRect]);
	
	NSRect winRect = [[[self window] contentView] convertRect:rowRect fromView:self];
	
	winRect.origin.x += [[self window] frame].origin.x;
	winRect.origin.y += [[self window] frame].origin.y;

	
	return winRect;
}



// d&D
/*
- (NSImage *)dragImageForRowsWithIndexes:(NSIndexSet *)dragRows tableColumns:(NSArray *)tableColumns event:(NSEvent*)dragEvent offset:(NSPointPointer)dragImageOffset
{

	NSImage* image  =[super dragImageForRowsWithIndexes:(NSIndexSet *)dragRows tableColumns:(NSArray *)tableColumns event:(NSEvent*)dragEvent offset:(NSPointPointer)dragImageOffset];
	
	NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard ];
	
	
	[pb declareTypes:[NSArray arrayWithObjects: NSStringPboardType, NSRTFPboardType,nil ] owner:self];
	[pb setString:@"sss" forType:NSStringPboardType];
	
	/*
	NSAttributedString* astr = [[[NSAttributedString alloc] initWithString: @"ssss"] autorelease];
	[pb setData:[astr RTFFromRange:NSMakeRange(0,[astr length]) documentAttributes:nil]  forType:  NSRTFPboardType];

	return 	image;
}*/

	/*
#pragma mark
- (void)keyDown:(NSEvent *)event
{

	NSIndexSet *rowIndex = [self selectedRowIndexes];
	
	NSArray* pathArray = [[[[[self delegate] findResult] arrangedObjects] objectsAtIndexes: rowIndex] valueForKey:kMDItemPath];

	NSLog(@"pathArray %@",pathArray);
	NSLog(@"ClassName %@", NSClassFromString(@"QLPreviewPanel") );
	NSLog(@"Class %d",[NSClassFromString(@"QLPreviewPanel")  respondsToSelector:@selector(sharedPreviewPanel)] );
	NSMutableArray* urls = [NSMutableArray array];
	
	int hoge;
	for( hoge=0; hoge < [pathArray count]; hoge++ )
	{
		[urls addObject: [NSURL fileURLWithPath:[pathArray objectAtIndex:hoge]] ];
	}
	
	//Class QLPreview = NSClassFromString(@"QLPreviewPanel");
	[[QLPreviewPanel sharedPreviewPanel] setURLs:urls currentIndex:0 preservingDisplayState:YES];
	
	[[QLPreviewPanel sharedPreviewPanel] makeKeyAndOrderFrontWithEffect:1];   // 1 = fade in 
	
	NSLog(@"keydown");	
	 
}*/
	
#pragma mark Print
	
- (void)drawRect:(NSRect)aRect
{
	if ( ![[NSGraphicsContext currentContext] isDrawingToScreen]  ) // for printing
	{
		NSAffineTransform* transform;
		NSGraphicsContext* context = [NSGraphicsContext currentContext];
		
		//
		id item = [[[self delegate] outlineView] itemAtRow: [[[self delegate] outlineView] selectedRow]];
		NSImage* icon = [item icon]; 
		[icon setFlipped:YES];
		NSString* name = [item name];
	
		NSArray* keywords = [[[[self delegate] valueForKey:@"windowUIObjectController"] selection] valueForKey:@"searchKeywords"];
		
		if( keywords != nil && [keywords count] > 0 )
		{
			name = [NSString stringWithFormat:@"%@ (%@)",name, [keywords componentsJoinedByString:@", "] ];
		}
			
		NSString* countStr = [NSString stringWithFormat:NSLocalizedString(@"%d files",@"") ,[self numberOfRows]];
		
		name = [NSString stringWithFormat:@"%@, %@",name, countStr ];



		[icon drawAtPoint:NSZeroPoint fromRect:NSMakeRect(0,0,16,16) operation:NSCompositeSourceOver fraction:1.0];
		[name drawAtPoint:NSMakePoint(20,0) withAttributes:nil];

		[icon setFlipped:NO];

		//
		[context saveGraphicsState];
		
		 transform = [NSAffineTransform transform];
		[transform translateXBy:0 yBy:20];
		[transform concat];
		
		[[self headerView] drawRect:aRect];
		
		[context restoreGraphicsState];
		
		

		//
		
		[context saveGraphicsState];
			
		 transform = [NSAffineTransform transform];
		[transform translateXBy:0 yBy:[[self headerView] bounds].size.height+20];
		[transform concat];
		
		[super drawRect:aRect];

		[context restoreGraphicsState];


	}
	else
		[super drawRect:aRect];

}
- (void)saveAsPDF:(id)sender
{


	NSArray* objects = [[[self delegate] findResult] arrangedObjects];
	
	int hoge;
	for( hoge = 0; hoge < [objects count]; hoge++ )
	{
		NSString* path = [[objects objectAtIndex: hoge] valueForKey:kMDItemPath];
		

		
		
		NSImage* icon = [[NSWorkspace sharedWorkspace] iconForFile:path];
		[icon setSize:NSMakeSize(16,16)];
		
		@synchronized(iconCache){
			[iconCache setObject:icon forKey:path];
		}
	}
	
	

	
	NSRect drawRect = [self bounds];
	drawRect.size.height += [[self headerView] frame].size.height + 25;
	
	NSData* data = [self dataWithPDFInsideRect:drawRect];
	[data retain];
	NSSavePanel* savePanel = [NSSavePanel savePanel];
	
	[savePanel setRequiredFileType:@"pdf"];
    [savePanel beginSheetForDirectory:nil
                                 file:[[[self delegate] valueForKey:@"field"] stringValue]
                       modalForWindow:[self window]
                        modalDelegate:self
                       didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:)
                          contextInfo:data];
	 
}

- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
    [sheet orderOut:nil];
    [sheet setDelegate:nil];
	
    if (returnCode == NSOKButton){

		
        [contextInfo writeToFile:[sheet filename] atomically:YES];
    }
	[contextInfo release];
}


@end
