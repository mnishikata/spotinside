//
//  C3DTView.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTView.h,v 1.6 2003/06/29 13:58:11 pmanna Exp $
//
// $Log: C3DTView.h,v $
// Revision 1.6  2003/06/29 13:58:11  pmanna
// Added support for mouse up event
//
// Revision 1.5  2003/06/24 22:29:56  pmanna
// Reviewed to support more controller callbacks
//
// Revision 1.4  2003/06/21 19:21:42  pmanna
// Corrected with addition of controller protocol
//
// Revision 1.3  2003/06/15 12:44:32  pmanna
// Removed Quicktime refs: they don't belong here
//
// Revision 1.2  2003/06/14 09:23:24  pmanna
// Added option for orthographic view
//
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import <Cocoa/Cocoa.h>
#import "C3DTScene.h"
#import "C3DTAnimator.h"

@protocol C3DTViewController
- (void)mouseDown: (NSEvent *)anEvent;
- (void)mouseUp: (NSEvent *)anEvent;
- (void)mouseDragged: (NSEvent *)anEvent;
- (void)keyDown: (NSEvent *)anEvent;
@end

@interface C3DTView : NSOpenGLView {
    IBOutlet NSObject <C3DTViewController>	*controller;
    
    C3DTScene								*_scene;
    C3DTCamera								*_camera;
    _C3DTVector								_backColor;

    NSPoint			lastClick;
}

- (C3DTCamera *)camera;

- (C3DTScene *)scene;
- (void)setScene: (C3DTScene *)aScene;

- (_C3DTVector)backColor;
- (void)setBackColor: (_C3DTVector)aColor;

- (void)refresh: (NSNotification *)aNotification;

- (void)registerWith: (id)aSender forNotification: (NSString *)aNotification;
- (void)unregisterWith: (id)aSender forNotification: (NSString *)aNotification;

@end
