// Smultron version 1.2.5, 2005-05-08
// Written by Peter Borg, pgw3@mac.com
// Copyright (C) 2004-2005 Peter Borg
// For the latest version of the code go to http://smultron.sourceforge.net
// Released under GNU General Public License, http://www.gnu.org/copyleft/gpl.html
//
// MNLayoutManager based on SMLLayoutManager

#import "ControlCodeLayoutManager.h"
#import "ColorCheckbox.h"



#define TEMPORARY_HIGHLIGHT_ATTRIBUTENAME @"TEMPORARY_HIGHLIGHT_ATTRIBUTENAME"



//#define CONTROL_CHAR_COLOR [NSColor blueColor]

@implementation ControlCodeLayoutManager

-(id)init
{
	if (self = [super init]) {

		/*
		NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
		
		id obj = [ud objectForKey:@"highlightColor"];
		
		if( obj != nil )
			highlightColor = [[NSUnarchiver unarchiveObjectWithData:obj] retain];
		
		else
		{
			highlightColor = [[NSColor colorWithCalibratedRed:0.2
												   green:0.2
													blue:1.0 alpha:1.0] retain];
			
			[ud setObject:[NSArchiver archivedDataWithRootObject:highlightColor]
				   forKey:@"highlightColor"];
		}
		 */
	}
	return self;
}

-(id)dealloc
{
	//[highlightColor release];
	[super dealloc];
}


- (void)drawGlyphsForGlyphRange:(NSRange)glyphRange atPoint:(NSPoint)containerOrigin
{
	

	

	// token field type highlight
	
	//EMBED_FUGOID_ATTRIBUTENAME
	
	NSTextView* tv = [self firstTextView];
	NSRange longestRange;
	NSRange charRange = [self characterRangeForGlyphRange:glyphRange actualGlyphRange:nil];

	
	
	
	
	
	
	int hoge;
	for( hoge = charRange.location; hoge < NSMaxRange(charRange);     )
	{
		////NSLog(@"%d hoge",hoge);
		
		id embedFugoIdValue  = [self temporaryAttributeForKey:TEMPORARY_HIGHLIGHT_ATTRIBUTENAME 
											 atCharacterIndex:hoge longestEffectiveRange:&longestRange expandTo:0];
		
		if( embedFugoIdValue != nil )
		{
			
			//highlight longestRange
			
			NSRect highlightFrame;
			NSRange longestGlyphRange = [self glyphRangeForCharacterRange:longestRange
													 actualCharacterRange:nil];
			
			highlightFrame = [self boundingRectForGlyphRange:longestGlyphRange
											 inTextContainer:[tv textContainer]];
			
			
			NSArray* rangeArray = [self boundingRectArrayForGlyphRange: longestGlyphRange ];
			
			int piyo;
			for( piyo = 0; piyo < [rangeArray count]; piyo ++ )
			{
				NSRange aRange = NSRangeFromString( [rangeArray objectAtIndex:piyo] );
				NSRect aRect = [self boundingRectForGlyphRange:aRange inTextContainer:[tv textContainer] ];
				
				
				int openEnd = 0;
				
				if( piyo != 0 ) openEnd = 1;
				if( piyo != [rangeArray count]-1 ) openEnd = openEnd + 2;
				
				
				aRect.size.height -= 1;
				aRect.size.width -= 1;
				
				[ColorCheckbox roundedBoxFrame:embedFugoIdValue
										 frame:aRect
										 curve:3.0
									   openEnd:openEnd];
				
				
			}
			
			
		}
		hoge = NSMaxRange(longestRange);
		
	}

	


		[super drawGlyphsForGlyphRange:glyphRange atPoint:containerOrigin];

}








-(NSArray*)boundingRectArrayForGlyphRange:(NSRange)glyphRange
{
	
	NSRange range;
	NSRect frame;
	NSTextView* tv = [self firstTextView];
	NSTextContainer* tc = [tv textContainer];
	
	frame = [self lineFragmentRectForGlyphAtIndex:NSMaxRange(glyphRange)-1
								   effectiveRange:nil];

	
	
	
	NSRect firstGlyphFrame = [self lineFragmentRectForGlyphAtIndex:glyphRange.location
																effectiveRange:nil];


	// one line
	if( NSEqualRects(frame,firstGlyphFrame) ) 
	{
	//	//NSLog(@"equal %@ %@", NSStringFromRect(frame) , NSStringFromRect(firstGlyphFrame) );
		return [NSArray arrayWithObject:NSStringFromRange(glyphRange)];
	}
	
	////NSLog(@"not equal");
	
	//
	NSMutableArray* array = [NSMutableArray array];

	
	int hoge;
	for( hoge = glyphRange.location; hoge < NSMaxRange(glyphRange)+1; hoge ++ )
	{
		NSRange rangeInTheLine;
		NSRect oneRect = [self lineFragmentRectForGlyphAtIndex:hoge effectiveRange:&rangeInTheLine];
		
		
		NSRange intersectionRange =  NSIntersectionRange(rangeInTheLine,glyphRange) ;
		
		if( intersectionRange.length != 0 )
		[array addObject: NSStringFromRange(intersectionRange) ];
		
		hoge = NSMaxRange(rangeInTheLine);
		
	}
	return (NSArray*)array;
}

- (id) temporaryAttributeForKey:(NSString*)key 
			   atCharacterIndex:(unsigned)charIndex longestEffectiveRange:(NSRangePointer)effectiveCharRange expandTo:(int)expandMode
//expandMode = 0 both
//expandMode = 1 only left
//expandMode = 2 only right
// 10 never
{
	
	id value = [[self temporaryAttributesAtCharacterIndex:charIndex effectiveRange:effectiveCharRange] objectForKey:key];
	
	
	
	// expand
	unsigned loc = effectiveCharRange->location;
	if( loc > 0 && expandMode != 2 && expandMode != 10)
	{
		////NSLog(@"%d <- ", loc);

		NSRange preRange;
		
		id preValue;
		
		
		preValue = [[self temporaryAttributesAtCharacterIndex:loc-1 effectiveRange:&preRange] objectForKey:key];


		
		if( [preValue isEqualTo: value] )
		{
			[self temporaryAttributeForKey:key 
							atCharacterIndex: loc-1
					   longestEffectiveRange:&preRange expandTo:1];	
			
			*effectiveCharRange = NSUnionRange( *effectiveCharRange , preRange );
		}
	}
	
	loc = NSMaxRange( *effectiveCharRange );
	NSRange fullRange = NSMakeRange(0, [[[self firstTextView] textStorage] length] );
	if( loc < NSMaxRange( fullRange ) &&  expandMode != 1 && expandMode != 10)
	{
		////NSLog(@"-> %d", loc);

		NSRange sufRange;
		id sufValue;
		
		
		sufValue = [[self temporaryAttributesAtCharacterIndex:loc effectiveRange:&sufRange] objectForKey:key];
		
		
		if( [sufValue isEqualTo: value] )
		{
			[self temporaryAttributeForKey:key 
						  atCharacterIndex: loc
					 longestEffectiveRange:&sufRange  expandTo:2];	
			
			*effectiveCharRange = NSUnionRange( *effectiveCharRange , sufRange );
		}
		

	}
	
	return value;
}

////////
@end
