//
//  C3DTStringTexture.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun Jun 29 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTStringTexture.h,v 1.1 2003/06/29 20:23:58 pmanna Exp $
//
// $Log: C3DTStringTexture.h,v $
// Revision 1.1  2003/06/29 20:23:58  pmanna
// Initial import in CVS
//
//

#import "C3DTTexture.h"


@interface C3DTStringTexture : C3DTTexture {
    NSString	*_fontName;
    float		_fontSize;
    _C3DTVector	_frontColor;
    _C3DTVector	_backColor;
}

+ (C3DTStringTexture *)textureWithString: (NSString *)aText font: (NSString *)fontName size: (float)fontSize;
+ (C3DTStringTexture *)textureWithString: (NSString *)aText
                                    font: (NSString *)fontName
                                    size: (float)fontSize
                              frontColor: (_C3DTVector)fColor
                               backColor: (_C3DTVector)bColor;

- (id)initWithString: (NSString *)aText
                font: (NSString *)fontName
                size: (float)fontSize
          frontColor: (_C3DTVector)fColor
           backColor: (_C3DTVector)bColor;


@end
