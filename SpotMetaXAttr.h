/*
 *  SpotMetaXAttr.h
 *  SpotMetaReadWrite
 *
 *  Created by Ben Summers on 25/09/2005.
 *  Copyright 2005 Ben Summers. All rights reserved.
 *
 */

#ifndef SPOTMETAXATTR__H
#define SPOTMETAXATTR__H

// The prefix to get the SpotMeta namespace.
#define SPOTMETA_XATTR_PREFIX					"org_spotmeta_"

// When encoding a fully built string, how many underscores need skipping?
#define SPOTMETA_XATTR_PREFIX_SKIP_UNDERSCORES	3

// The xattr marker to show that something was written, when no actual xattrs were written.
#define SPOTMETA_XATTR_MARKER					"org_spotmeta_x"
// The single character following the prefix to indicate the marker
#define SPOTMETA_XATTR_MARKER_SINGLE_CHAR		'x'

// When writing multivalues, separate values with this (ASCII "RECORD SEPARATOR", allowed by Unicode)
#define SPOTMETA_MULTIVALUE_SEPARATOR			0x1E
#define SPOTMETA_MULTIVALUE_SEPARATOR_STR		"\x1E"


// Maximum size of an attribute name in bytes, when UTF8 encoded, including the org_spotmeta prefix
#define SPOTMETA_MAX_XATTR_NAME					128

/*
	Returns NULL on error.
*/
#define SPOTMETA_XATTR_READFLAG_RETAIN_PREFIX		1
#define SPOTMETA_XATTR_READFLAG_ENCODED_KEY_NAMES	2
CFDictionaryRef SpotMetaXAttrRead(const char *filename, bool *pHasSpotMetaMarkerOut, int Flags);


/*
	Returns true on success, false on error.
*/
#define SPOTMETA_XATTR_WRITEFLAG_PREFIX_INCLUDED	1
#define SPOTMETA_XATTR_WRITEFLAG_ENCODED_KEY_NAMES	2
bool SpotMetaXAttrWrite(const char *filename, CFDictionaryRef MetaData, int Flags);


CFStringRef SpotMetaEncodeStringForXAttrName(const CFStringRef String, int NumNoEncodeUnderscores);

CFStringRef SpotMetaDecodeStringFromXAttrName(const CFStringRef String, int NumNoDecodeUnderscores);



#endif // SPOTMETAXATTR__H

