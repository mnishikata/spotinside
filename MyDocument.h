//
//  MyDocument.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 06/06/24.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//


#import <Cocoa/Cocoa.h>

@class OASplitView,  RBSplitView, RBSplitSubview,FlowView ;

@interface MyDocument : NSDocument
{
	IBOutlet id splitView;
	IBOutlet id mainSplitView;
	IBOutlet id secondSplitView;
	
	IBOutlet id outlineSplitView;


    IBOutlet id field;	

	IBOutlet id findResult; //arraycontroller

    IBOutlet id table;


	IBOutlet id outlineView;
	IBOutlet id windowUIObjectController;
	
	IBOutlet id slider;
	IBOutlet id sliderSmall;
	IBOutlet id sliderLarge;

	IBOutlet id advancedBox;

	IBOutlet id drawer;
	IBOutlet id knob;
	IBOutlet id tagKnob;
	
	IBOutlet id flowViewTemplate;
	IBOutlet id flowPane;
	FlowView* flowView;

    IBOutlet id textView;
	IBOutlet id pdfView;
	IBOutlet id imageView;
	
	IBOutlet id metaList; //controller;
	IBOutlet id metaListWindow;
 	
	

	IBOutlet id infoPanel;
	
	IBOutlet id window;

	
	NSString* _searchKey;

	

	IBOutlet id shiborikomiField;
	IBOutlet id shiborikomiRegex;
	
	IBOutlet id tagTextView;
	IBOutlet id tagPane;

	
	//ADDITION
	
	IBOutlet id convertWindow;
	IBOutlet id convertBar;
	IBOutlet id convertStatus;


	
}
- (id)init;
- (NSString *)windowNibName;
-(void)close;
-(void)dealloc;
- (void)windowControllerDidLoadNib:(NSWindowController *) aController;
- (void)splitViewDoubleClick:(OASplitView *)sender;
-(void)setDefault;
-(NSString*)queryTitle;
- (NSData *)dataOfType:(NSString *)typeName error:(NSError **)outError;
- (BOOL)readFromData:(NSData *)fromdata ofType:(NSString *)typeName error:(NSError **)outError;
-(NSString*)escape:(NSString*)str;
- (IBAction)launch:(id)sender;
- (IBAction)revealInFinder:(id)sender;
- (IBAction)search:(id)sender;
-(void)searchEnd:(id)sender;
-(void)displayWithFile:(NSString*)path;
-(id)findResult;
-(NSString*)uti:(NSString*)path;
-(NSArray*)utiTree:(NSString*)path;
-(NSAttributedString*)loadTextAt:(NSString*)path;
-(void)highlight:(NSString*)key ;
-(IBAction)slider:(id)sender;
-(IBAction)website:(id)sender;
- (IBAction)actionButtonPressed:(id)sender;
- (IBAction)save:(id)sender;
-(void)setQueryTemplate:(id)value;
-(void)setScope:(id)value;
-(void)setQuery:(id)value;
-(IBAction)showViewOption:(id)sender;
-(void)metaListClosed:(id)sender;
- (void)sheetDidEnd:(NSWindow *)sheet returnCode:(int)returnCode  contextInfo:(void  *)item;;
- (IBAction)metaListFilter:(id)sender;
- (void)tellMeTargetToFindIn:(id)textFinder;
- (void)tableViewSelectionDidChange:(NSNotification *)aNotification;
- (BOOL)tableView:(NSTableView *)aTableView writeRows:(NSArray *)rows toPasteboard:(NSPasteboard *)pboard;
- (void)tableView:(NSTableView *)aTableView setObjectValue:(id)anObject forTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
- (BOOL)tableView:(NSTableView *)aTableView shouldEditTableColumn:(NSTableColumn *)aTableColumn row:(int)rowIndex;
-(NSArray*)wordsFor:(NSString*)string;
-(IBAction)findNext:(id)sender;
-(IBAction)resetSorting:(id)sender;
- (BOOL)tableView:(NSTableView *)aTableView writeRowsWithIndexes:(NSIndexSet *)rowIndexes toPasteboard:(NSPasteboard*)pboard;
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context;
- (NSString *)tableView:(NSTableView *)aTableView toolTipForCell:(NSCell *)aCell rect:(NSRectPointer)rect tableColumn:(NSTableColumn *)aTableColumn row:(int)row mouseLocation:(NSPoint)mouseLocation;
- (void)splitView:(RBSplitView*)sender wasResizedFrom:(float)oldDimension to:(float)newDimension ;
- (unsigned int)splitView:(RBSplitView*)sender dividerForPoint:(NSPoint)point inSubview:(RBSplitSubview*)subview ;
-(BOOL)isDocumentEdited;

-(unsigned)numberOfCovers;
-(NSString*)subtitleForCoverAtIndex:(unsigned)index;
-(NSImage*)imageForCoverAtIndex:(unsigned)index;
-(void)flowView:(FlowView*)aFlowView didSelectCoverAtIndex:(unsigned)index;

@end
