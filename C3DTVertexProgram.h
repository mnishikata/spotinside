//
//  C3DTVertexProgram.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTVertexProgram.h,v 1.2 2003/06/19 20:30:48 pmanna Exp $
//
// $Log: C3DTVertexProgram.h,v $
// Revision 1.2  2003/06/19 20:30:48  pmanna
// Moved OpenGL headers to superclass
//
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import "C3DTStyle.h"
#import	"C3DTMath.h"


@interface C3DTVertexProgram : C3DTStyle {
    GLuint		_programId;
    GLuint		_programSize;		// Program length
    NSString	*_programData;		// The vertex program

    GLuint		_savedProgramId;
    BOOL		_programSaved;
    
    _C3DTVector	*_localParams;
    _C3DTVector	*_savedLocalParams;
    int			_numLocalParams;
    GLint		_maxLocalParams;
    
    _C3DTVector	*_envParams;
    _C3DTVector	*_savedEnvParams;
    int			_numEnvParams;
    GLint		_maxEnvParams;
}

+ (C3DTVertexProgram *)programWithFile: (NSString *)aName;

- (_C3DTVector)localParamAtIndex: (int)anIndex;
- (void)setLocalParam: (_C3DTVector)aParam atIndex: (int)anIndex;

- (_C3DTVector)envParamAtIndex: (int)anIndex;
- (void)setEnvParam: (_C3DTVector)aParam atIndex: (int)anIndex;

@end
