//
//  C3DTInteractiveView.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue Jun 24 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTInteractiveView.h,v 1.1 2003/06/24 22:29:03 pmanna Exp $
//
// $Log: C3DTInteractiveView.h,v $
// Revision 1.1  2003/06/24 22:29:03  pmanna
// Initial release
//
//
#import <Cocoa/Cocoa.h>
#import "C3DTView.h"
#import "C3DTTypes.h"
#import "C3DTEntity.h"
#import "C3DTTexture.h"
#import "MNCFCoverFrame.h"
#import "WindowAnimation.h"
#import "AngleAnimation.h"



@class  TransparentWindow, ScrollerView;
@interface FlowView : C3DTView {

	
	C3DTEntity *title;
	unsigned titleIndex;
	
	double Nmax_plus_1;
	int R;
	double Ns;
	
	double Ns_previous;
	double Ncenter;
	
	NSDictionary* titleStyle;
	NSDictionary* subtitleStyle;
	
	NSDictionary* titleStyle_fullscreen;
	NSDictionary* subtitleStyle_fullscreen;

	
	NSMutableArray* nodes;
	
	
	AngleAnimation* animation;
	
	
	
	id dataSource;
	

	NSMutableDictionary* textureDictionary;


	
	MNCFCoverFrame* coverFrame;
	
	float lx,ly,lz,dx,dy,dz;
	
	BOOL needsCleaning;
	BOOL fullscreen;
	BOOL needsSetup;
	
	
	//UI
	TransparentWindow* fullWindow;
	WindowAnimation* fullscreenAnimation;

	
	NSRect attachedWindowTargetOriginalRect;
	NSRect attachedWindowTargetOriginalRectInScreen;
	NSView* attachedWindowTargetOriginalParentView;

	ScrollerView *scrollerView;

}
-(void)awakeFromNib;
-(NSArray*)nodes;
-(double)Ns_previous;
-(double)Ns;
-(int)R;
-(double)Ncenter;
- (void)animationDidEnd:(AngleAnimation*)animation;
- (void)animationDidStop:(AngleAnimation*)animation;
-(void)updateView;
- (void)mouseDown:(NSEvent *)anEvent;
-(BOOL)isOpaque;
-(void)setNeedsCleaning:(BOOL)flag;
-(void)drawRect:(NSRect)rect;
- (void)mouseDragged:(NSEvent *)anEvent;
- (BOOL)acceptsFirstResponder;
- (void)keyDown:(NSEvent *)e;
- (void)scrollWheel:(NSEvent *)e;
-(void)movePage:(int)dif;
-(void)moveRow:(int)dif;
-(void)setDataSource:(id)obj;
-(void)setup;
-(NSArray*)nodesSortedFromRear;
-(void)setNcenter:(double)value;

-(C3DTTexture*)textureForN:(double)nodeN;


///
@end



