//
//  C3DTCylinder.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Tue May 20 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTCylinder.h,v 1.1.1.1 2003/06/10 18:09:23 pmanna Exp $
//
// $Log: C3DTCylinder.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTGeometry.h"


@interface C3DTCylinder : C3DTGeometry {
    float		_baseRadius;
    float		_topRadius;
    float		_height;
    int			_slices;
    int			_stacks;
}

+ (C3DTCylinder *)cylinderWithBase: (float)baseRadius top: (float)topRadius height: (float)height
                            slices: (int)aSliceNumber stacks: (int)aStackNumber;

- (id)initWithBase: (float)baseRadius top: (float)topRadius height: (float)height
            slices: (int)aSliceNumber stacks: (int)aStackNumber;

- (float)baseRadius;
- (void)setBaseRadius: (float)aRadius;

- (float)topRadius;
- (void)setTopRadius: (float)aRadius;

- (float)height;
- (void)setHeight: (float)aValue;

@end
