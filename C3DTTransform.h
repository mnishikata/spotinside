//
//  C3DTTransform.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTTransform.h,v 1.1.1.1 2003/06/10 18:09:25 pmanna Exp $
//
// $Log: C3DTTransform.h,v $
// Revision 1.1.1.1  2003/06/10 18:09:25  pmanna
// Initial import
//
//

#import "C3DTGroup.h"
#import "C3DTStyle.h"
#import <OpenGL/glu.h>


@interface C3DTTransform : C3DTGroup {
    _C3DTVector	_rotationCenter;
    _C3DTVector	_rotation;
    _C3DTVector	_translation;
    _C3DTVector	_scaling;

    C3DTStyle	*_style;

    BOOL		_hasTransform;
}

- (id)initWithStyle: (C3DTStyle *)aStyle;

- (_C3DTVector)rotationCenter;
- (void)setRotationCenter: (_C3DTVector)aRotationCenter;
- (void)setRotationCenterX: (float)xPos Y: (float)yPos Z: (float)zPos;

- (_C3DTVector)rotation;
- (void)setRotation: (_C3DTVector)aRotation;
- (void)setRotationX: (float)xAngle Y: (float)yAngle Z: (float)zAngle;

- (_C3DTVector)translation;
- (void)setTranslation: (_C3DTVector)aTranslation;
- (void)setTranslationX: (float)xDist Y: (float)yDist Z: (float)zDist;

- (_C3DTVector)scaling;
- (void)setScaling: (_C3DTVector)aScaling;
- (void)setScalingX: (float)xScale Y: (float)yScale Z: (float)zScale;

- (C3DTStyle *)style;
- (void)setStyle: (C3DTStyle *)aStyle;

@end
