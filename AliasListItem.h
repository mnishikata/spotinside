//
//  FolderListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ListItem.h"
#import "NDAlias.h"

@interface AliasListItem : ListItem {

	NDAlias* alias;
}
- (id) init ;
-(void)dealloc;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
- (id)copyWithZone:(NSZone *)zone;
-(void)setAliasWithPath:(NSString*)path;
-(BOOL)isMissing;
-(NSString*)path;
-(NSString*)kind ;
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController;
-(BOOL)_isFolder;
-(NSArray*)scope ;
-(NSArray*)contents;
-(void)search;
-(NSPredicate*)filterPredicate;

@end
