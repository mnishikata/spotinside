//
//  NSMutableArray+ListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/13.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ListItem.h"

@interface NSMutableArray (ListItem)

-(void)undoRemoveListItem:(id)undoDictionary;
-(void)undoInsertListItem:(id)undoDictionary;
-(void) removeObjectIdenticalTo:(id)object usingUndoManager:(NSUndoManager*)undoManager;
- (void)addObject:(id)anObject usingUndoManager:(NSUndoManager*)undoManager;
- (void)insertObject:(id)anObject atIndex:(unsigned)index usingUndoManager:(NSUndoManager*)undoManager;

@end
