//
//  C3DTGroup.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTGroup.h,v 1.2 2003/06/29 13:53:35 pmanna Exp $
//
// $Log: C3DTGroup.h,v $
// Revision 1.2  2003/06/29 13:53:35  pmanna
// Added selection flag
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTNode.h"
#import "C3DTMath.h"


@interface C3DTGroup : C3DTNode {
    int				_visibility;
    _C3DTBounds		_bounds;

    BOOL			_selected;
}

- (_C3DTBounds)bounds;
- (void)setBounds: (_C3DTBounds) newBounds;

- (BOOL)selected;
- (void)setSelected:(BOOL)newSelected;

- (int)spheresForBounds: (_C3DTSpheroid **)spheres;

@end
