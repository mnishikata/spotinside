//
//  DrawLibrary.h
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
//#import "EdgiesLibrary.h"

// IMPORT DUMMY
#define TAB_POSITION int
#define TAB_OPEN_STATUS int

NSColor* convertColorSpace(NSColor* color);

NSAttributedString* MNTruncate(NSAttributedString* ogn, int inWidth);
void drawDragArea(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, TAB_OPEN_STATUS tabOpenStatus);
void _drawDragCorner(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, TAB_OPEN_STATUS tabOpenStatus);
void _drawSides(NSRect frame, NSWindow* window,  TAB_POSITION tabPosition, NSColor* color,  TAB_OPEN_STATUS tabOpenStatus);



void drawButton(NSRect frame, NSWindow* window, TAB_POSITION tabPosition, NSColor* color, 
				BOOL mouseOnTab, NSRect customizeButtonRect, 	NSRect closeButtonRect,
				BOOL disableMenuButton, BOOL disableCloseButton);

void drawRoundedRectangle(NSColor* frameColor, NSColor* fillColor, NSTextView* textView, NSRange range );
NSArray* boundingRectArrayForGlyphRange(NSRange glyphRange, NSTextView* textView);

void drawRoundedBox(NSColor* aColor , NSRect frame , float boxCurve, NSColor* fillColor , int openEnd , float strokeWidth);

void addRoundedRectToPath(CGContextRef context, CGRect rect, 
						  float ovalWidth,float ovalHeight);
void addRoundedRectWithOpenEndToPath(CGContextRef context, CGRect rect, 
									 float ovalWidth,float ovalHeight, int openEnd);

