//
//  ListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ListItem.h"

@interface SearchListItem : ListItem {

	NSString* queryTemplate;
	
	IBOutlet id window;
	IBOutlet id scopeField;
	IBOutlet id queryField;
	IBOutlet id queryTemplateField;


}

- (id) init ;
- (id)initWithCoder:(NSCoder *)coder;
-(NSString*)kind ;
-(NSImage*)icon;
-(void)search;
-(void)searchWithAdditionalQuery:(NSString*)query forArrayController:(NSArrayController*)anArrayController;
-(NSString*)queryTemplate ;
-(void)setQueryTemplate:(NSString*)string;


@end
