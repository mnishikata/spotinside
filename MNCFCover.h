//
//  C3DTPlainSurface.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Thu Jul 03 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTPlainSurface.h,v 1.1 2003/07/04 13:11:25 pmanna Exp $
//
// $Log: C3DTPlainSurface.h,v $
// Revision 1.1  2003/07/04 13:11:25  pmanna
// Initial import in CVS
//
//

#import "C3DTGeometry.h"


@interface MNCFCover : C3DTGeometry {
    float	_width;
    float	_depth;
    int		_meridians;
    int		_parallels;
    BOOL	_repeat;
}

+ (MNCFCover *)planeWithWidth: (float)aWidth
                               depth: (float)aDepth
                           meridians: (int)tessX
                           parallels: (int)tessY
                       textureRepeat: (BOOL)repeat;

- (id)initWithWidth: (float)aWidth
              depth: (float)aDepth
          meridians: (int)tessX
          parallels: (int)tessY
      textureRepeat: (BOOL)repeat;

- (float)width;
- (void)setWidth: (float)aSize;
- (float)depth;
- (void)setDepth: (float)aSize;
- (int)meridians;
- (void)setMeridians:(int)newMeridians;
- (int)parallels;
- (void)setParallels:(int)newParallels;

- (void)buildPlaneWithWidth: (float)aWidth
                      depth: (float)aDepth
                  meridians: (int)tessX
                  parallels: (int)tessY
              textureRepeat: (BOOL)repeat;

@end
