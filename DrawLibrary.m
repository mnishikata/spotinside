//
//  DrawLibrary.m
//  sticktotheedge
//
//  Created by Masatoshi Nishikata on 06/05/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "DrawLibrary.h"

#define TITLE_ATTRIBUTE [NSDictionary dictionaryWithObjectsAndKeys:[NSFont boldSystemFontOfSize:12],NSFontAttributeName , NULL ]





NSColor* convertColorSpace(NSColor* color)
{
	return 	[color colorUsingColorSpaceName:NSCalibratedRGBColorSpace];

}




#pragma mark -
NSAttributedString* MNTruncate(NSAttributedString* ogn, int inWidth)
{
	////NSLog(@"truncate %@ %d",[ogn string],inWidth);

	if( inWidth < 2  ) return [[[NSAttributedString alloc] initWithString:@""] autorelease];
		if ([ogn size].width < inWidth) return ogn;
		
	
	NSAttributedString *result = ogn;

	
	NSMutableAttributedString *newString = [[[NSMutableAttributedString alloc] init] autorelease];
	int curLength = [ogn length] - 1;	// start by chopping off at least one
	
	[newString appendAttributedString:ogn];
	while ([newString size].width > inWidth && [newString length] >1 )
	{
		unichar dots = 0x2026;
		
		NSRange range = NSMakeRange( curLength - 1, 2);	// replace 2 characters with "‚Äö√Ñ√∂‚àö√ë¬¨‚àÇ"
		[newString replaceCharactersInRange:range withString:[NSString stringWithCharacters:&dots  length:1]];
		curLength--;
		
		if( curLength == 0 )
		{
			newString = [[[NSAttributedString alloc] initWithString:@""] autorelease];
			break;
		}
	}
	result = newString;
	
	
	
	
	return result;
	
	
	
}


#pragma mark -
#pragma mark Draw Text View

void drawRoundedRectangle(NSColor* frameColor, NSColor* fillColor, NSTextView* textView, NSRange range )
{
	
	NSLayoutManager* layoutManager = [textView layoutManager];
	
	//highlight longestRange
	
	NSRect highlightFrame;
	NSRange longestGlyphRange = [layoutManager glyphRangeForCharacterRange:range
											 actualCharacterRange:nil];
	
	highlightFrame = [layoutManager boundingRectForGlyphRange:longestGlyphRange
									 inTextContainer:[textView textContainer]];
	
	
	NSArray* rangeArray =  boundingRectArrayForGlyphRange( longestGlyphRange , textView);
	
	int piyo;
	for( piyo = 0; piyo < [rangeArray count]; piyo ++ )
	{
		NSRange aRange = NSRangeFromString( [rangeArray objectAtIndex:piyo] );
		NSRect aRect = [layoutManager boundingRectForGlyphRange:aRange inTextContainer:[textView textContainer] ];
		
		
		int openEnd = 0;
		
		if( piyo != 0 ) openEnd = 1;
		if( piyo != [rangeArray count]-1 ) openEnd = openEnd + 2;
		
		
		aRect.size.height -= 1;
		aRect.size.width -= 1;
		
		
		//NSLog(@"drawing rect %@ openEnd %d",NSStringFromRect(aRect),openEnd);
		drawRoundedBox( frameColor, aRect, 3.0, fillColor, openEnd, 3);
			
	}	
}



NSArray* boundingRectArrayForGlyphRange(NSRange glyphRange, NSTextView* textView)
{
	
	NSRect frame;

//	NSTextContainer* tc = [textView textContainer];
	NSLayoutManager* lm = [textView layoutManager];
	frame = [lm lineFragmentRectForGlyphAtIndex:NSMaxRange(glyphRange)-1
								   effectiveRange:nil];
	
	
	
	
	NSRect firstGlyphFrame = [lm lineFragmentRectForGlyphAtIndex:glyphRange.location
													effectiveRange:nil];
	
	
	// one line
	if( NSEqualRects(frame,firstGlyphFrame) ) 
	{
		//	//NSLog(@"equal %@ %@", NSStringFromRect(frame) , NSStringFromRect(firstGlyphFrame) );
		return [NSArray arrayWithObject:NSStringFromRange(glyphRange)];
	}
	
	////NSLog(@"not equal");
	
	//
	NSMutableArray* array = [NSMutableArray array];
	
	
	int hoge;
	for( hoge = glyphRange.location; hoge < NSMaxRange(glyphRange)+1; hoge ++ )
	{
		NSRange rangeInTheLine;
		NSRect oneRect = [lm lineFragmentRectForGlyphAtIndex:hoge effectiveRange:&rangeInTheLine];
		
		
		NSRange intersectionRange =  NSIntersectionRange(rangeInTheLine,glyphRange) ;
		
		if( intersectionRange.length != 0 )
			[array addObject: NSStringFromRange(intersectionRange) ];
		
		hoge = NSMaxRange(rangeInTheLine);
		
	}
	return (NSArray*)array;
}



void drawRoundedBox(NSColor* aColor , NSRect frame , float boxCurve, NSColor* fillColor , int openEnd, float strokeWidth)
	//openEnd = 0  no
	//openEnd = 1 left open
	//openEnd = 2 right open
	//openEnd = 3 both open

{
	aColor =		convertColorSpace(aColor);
	fillColor = convertColorSpace(fillColor);
	
	//	float boxSize = 30;
	//	float boxCurve = 5;
	
	//
	
	float red, green, blue;
	float redF, greenF, blueF;
	
	float hue, sat, brt, alpha;
	[aColor getHue:&hue saturation:&sat brightness:&brt alpha:&alpha];
	[aColor getRed:&red green:&green blue:&blue alpha:&alpha];
	
	
	
	[fillColor getRed:&redF green:&greenF blue:&blueF alpha:&alpha];
	
	
	//draw
	CGContextRef context = [[NSGraphicsContext currentContext] graphicsPort];
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetRGBFillColor(context, redF, greenF, blueF, alpha);
	
	
	CGContextSetLineWidth ( context, 0.1 );
	
	//Fill 
	CGContextBeginPath(context);
	addRoundedRectWithOpenEndToPath(context, CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height),boxCurve, boxCurve, openEnd);	
	
	CGContextFillPath(context);
	
	
	
	//draw
	CGContextSetRGBStrokeColor(context, red, green, blue, alpha);
	
	CGContextSetLineWidth ( context, strokeWidth );
	
	//stroke 
	CGContextBeginPath(context);
	addRoundedRectWithOpenEndToPath(context, CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, frame.size.height),boxCurve, boxCurve, openEnd);	
	
	CGContextStrokePath(context);
	
}




void addRoundedRectToPath(CGContextRef context, CGRect rect, 
						  float ovalWidth,float ovalHeight)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
		// 1
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
	// 2
    CGContextTranslateCTM (context, CGRectGetMinX(rect),
						   // 3
						   CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
	// 4
    fw = CGRectGetWidth (rect) / ovalWidth;
	// 5
    fh = CGRectGetHeight (rect) / ovalHeight;
	// 6
    CGContextMoveToPoint(context, fw, fh/2); 
	// 7
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
	// 8
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
	// 9
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
	// 10
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); 
	// 11
    CGContextClosePath(context);
	// 12
    CGContextRestoreGState(context);
	// 13
}



void addRoundedRectWithOpenEndToPath(CGContextRef context, CGRect rect, 
									 float ovalWidth,float ovalHeight, int openEnd)
{
    float fw, fh;
    if (ovalWidth == 0 || ovalHeight == 0) {
		// 1
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
	// 2
    CGContextTranslateCTM (context, CGRectGetMinX(rect),
						   // 3
						   CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
	
	
	// 4
    fw = CGRectGetWidth (rect) / ovalWidth;
	// 5
    fh = CGRectGetHeight (rect) / ovalHeight;
	
	
	
	
	////NSLog(@"%d",openEnd);
	
	if( openEnd == 0 )
	{
		// 6
		CGContextMoveToPoint(context, fw, fh/2); 
		// 7
		CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
		// 8
		CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
		// 9
		CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
		// 10
		CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); 
		// 11
		CGContextClosePath(context);
		
	}
	
	else if( openEnd == 1 )
	{
		
		// 6
		CGContextMoveToPoint(context, 0, 0); 
		CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1); 
		
		// 7
		CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
		CGContextAddLineToPoint(context, 0, fh); 
		
		
	}else if( openEnd == 2 )
	{
		
		CGContextMoveToPoint(context, fw, fh); 
		// 7
		// 8
		CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
		// 9
		CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
		// 10
		CGContextAddLineToPoint(context, fw, 0);
		
		
	}else if( openEnd == 3 )
	{
		CGContextMoveToPoint(context, fw, fh); 
		CGContextAddLineToPoint(context, 0, fh);
		
		CGContextMoveToPoint(context, fw, 0); 
		CGContextAddLineToPoint(context, 0, 0);
		
	}
	
	
	// 12
    CGContextRestoreGState(context);
	// 13
	
}




