//
//  FolderListItem.m
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import "SpotMetaTagListItem.h"
#import "NSFileManager+NNExtensions.h"
#import "AliasListItem.h"
#import "SpotMetaXAttr.h"

@implementation SpotMetaTagListItem



- (id) init {
	self = [super init];
	if (self != nil) {


		name = [@"XAttr Tag" retain];
		canDelete = YES;
		canMove = YES;
		canDrop = YES;
		canSelect = YES;
		
		icon = nil;
	}
	return self;
}


- (id)initWithCoder:(NSCoder *)coder
{
	self = [super initWithCoder: coder];
	if (self != nil) {
		
		canDelete = YES;
		canMove = YES;
		canDrop = YES;
		canSelect = YES;
		
		
	}
	return self;
	
	
}




-(NSString*)kind {return @"spotmetatagger";}


-(NSString* )setQuery:(id)value
{
}

-(NSString*)query  {
	
	if( name == nil || [name isEqualToString:@""] ) return @"";
		
		
	NSString* format = @"((kMDItemFinderComment = '&%@ *')   || (kMDItemFinderComment = '* &%@ *') || (kMDItemFinderComment = '* &%@')   || (kMDItemFinderComment = '&%@') || (kMDItemKeywords = %@ && kMDItemContentType == com.apple.mail.emlx))";

 format = [NSString stringWithFormat:   format,
	 name,name,name,name,name];

	
	return format;
}

-(void)dropItem:(AliasListItem*)object
{
	NSString* path = [object path];
	if( path == nil ) {
		NSBeep(); return;
	}

	NSString* comment = nil;
	
	/*
	 slow....
	comment = [[NSFileManager defaultManager]  commentForURL:[NSURL fileURLWithPath:path]];
	*/
	
	MDItemRef mditem =  MDItemCreate (nil,path);
	
	if( mditem != nil ) 
	{
		comment = MDItemCopyAttribute(mditem, kMDItemFinderComment );
		CFRelease(mditem);
		
		if( comment != nil ) [comment autorelease];
	}
	
	if( comment == nil ) comment = @"";
	
	//NSLog(@"changing comment %@",comment);
	
	NSString* tagString = [@"&" stringByAppendingString:[self name]];
	
	if( [comment rangeOfString:tagString].length != 0 ) //alread exist
	{
		// do nothing
	}else
	{
		comment = [comment stringByAppendingString: @" "];
		comment = [comment stringByAppendingString: tagString];

	}
	
	[[NSFileManager defaultManager] setComment:comment forURL:[NSURL fileURLWithPath:path]];

}


-(void)setName:(NSString*)aName
{
	NSMutableString* string = [[NSMutableString alloc] initWithString:aName];
	[string replaceOccurrencesOfString:@"&" withString:@"+" options:0 range:NSMakeRange(0,[string length])];
		[string replaceOccurrencesOfString:@" " withString:@"_" options:0 range:NSMakeRange(0,[string length])];
	
	[name release];
	name = [string retain];
}

-(NSImage*)icon
{
	if( icon == nil )
	icon = [[NSImage imageNamed:@"tagbot.png"] retain];
	
	return [super icon];

}


@end
