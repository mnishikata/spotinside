//
//  C3DTCamera.h
//  Cocoa3DTutorial
//
//  Created by Paolo Manna on Sun May 18 2003.
//  Copyright (c) 2003. All rights reserved.
//
//  Terms of use:
//  - Short: OPEN SOURCE under Artistic License -- credit fairly, use freely, alter carefully.
//  -  Full: <http://www.opensource.org/licenses/artistic-license.html>
//
// $Id: C3DTCamera.h,v 1.4 2003/06/29 13:57:14 pmanna Exp $
//
// $Log: C3DTCamera.h,v $
// Revision 1.4  2003/06/29 13:57:14  pmanna
// Added support for object picking
//
// Revision 1.3  2003/06/24 22:30:50  pmanna
// Moved position to spherical coordinates to avoid gimbal lock
//
// Revision 1.2  2003/06/14 09:23:24  pmanna
// Added option for orthographic view
//
// Revision 1.1.1.1  2003/06/10 18:09:23  pmanna
// Initial import
//
//

#import "C3DTObject.h"
#import "C3DTMath.h"

@interface C3DTCamera : C3DTObject {
    _C3DTVector		_pos;			// Camera position
    _C3DTVector		_lookAt;		// Looking at this point
    _C3DTVector		_frustum;		// height,width & depth of the view
    float			_fov;			// Angle of the view
    BOOL			_useOrtho;		// Use orthographic view instead of perspective
    
    BOOL			_selectionMode;	// Is the camera operating in selection mode?
    NSPoint			_selectionPoint;
    NSSize			_selectionSize;
}

- (id)initWithBounds: (NSRect)boundsRect;

- (_C3DTVector)pos;
- (void)setPos: (_C3DTVector)newPos;
- (void)setPosX: (float)x Y: (float)y Z: (float)z;

- (void)rotateToAzimuth: (float)theta elevation: (float)phi distance: (float)d;
- (void)rotateByDegreesX: (float)theta Y: (float)phi;

- (float)distance;
- (void)setDistance: (float)d;

- (_C3DTVector)lookAt;
- (void)setLookAt: (_C3DTVector)newPos;
- (void)setLookAtX: (float)x Y: (float)y Z: (float)z;

- (_C3DTVector)frustum;
- (void)setFrustum: (_C3DTVector)newPos;
- (void)setFrustumWidth: (float)w height: (float)h depth: (float)d;

- (float)fov;
- (void)setFov: (float)newFov;

- (BOOL)useOrtho;
- (void)setUseOrtho: (BOOL)ortho;

- (BOOL)selectionMode;
- (void)setSelectionMode: (BOOL)newSelectionMode;

- (NSPoint)selectionPoint;
- (void)setSelectionPoint:(NSPoint)newSelectionPoint;

- (NSSize)selectionSize;
- (void)setSelectionSize:(NSSize)newSelectionSize;

- (void)setBounds: (NSRect)boundsRect;

- (void)useWith: (id)anObject;

@end
