//
//  FolderListItem.h
//  SpotInside
//
//  Created by Masatoshi Nishikata on 07/10/10.
//  Copyright 2007 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ListItem.h"

@interface FolderListItem : ListItem {

	NSMutableArray* children;
	unsigned countForFinishedChildrenSearching;
	NSMutableArray* childrenInSearching;

}
- (id) init ;
-(void)dealloc;
- (void)encodeWithCoder:(NSCoder *)encoder;
- (id)initWithCoder:(NSCoder *)coder;
- (id)copyWithZone:(NSZone *)zone;
-(NSMutableArray*)children;
-(NSString*)kind ;
-(NSImage*)icon;
-(void )setQuery:(id)value;
-(void )setChildren:(id)value;
-(NSString*)query ;
-(int)numberOfChildren;
-(BOOL)canExpand;
-(void)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController;
-(void)search;
-(void)searchListener:(NSNotification*)notification;
-(void)finish:(id)result;
-(void)removeItemFromChildren:(ListItem*)item;
-(BOOL)hasChild:(ListItem*)item;
-(NSPredicate*)filterPredicate;
-(NSString*)searchWithAdditionalQuery:(NSString*)aQuery forArrayController:(NSArrayController*)anArrayController endTarget:(id)aTarget andSelector:(SEL)aSelector;




@end
