/* ModifierKeyWell */

#import <Cocoa/Cocoa.h>

@interface FileLocationWell : NSButton
{
}
- (void) awakeFromNib;
- (void)_pushed:(NSEvent *)theEvent;
-(IBAction)playSound:(id)sender;

@end
